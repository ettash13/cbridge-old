﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace CBridge
{
    public class Kanvas3D : Kanvas2D
    {
        double[,] m_Points3D; // [i,7]: X1,Y1,Z1, x1,y1,z1, pen#
        double[,] m_Lines3D; // [i,13]: X1,Y1,Z1,X2,Y2,Z2 (actual coord), x1,y1,z1,x2,y2,z2 (rotated coord), pen#
        double[][] m_PolyLines3D; // [iset][6*npt+1]:  X1,Y1,Z1, x1,y1,z1 for each point, pen#
        double[][] m_Polygons3D; // [iset][6*npt+2]:  X1,Y1,Z1, x1,y1,z1 for each point, pen#, brush#
        double[,] m_SymPoints3D; // [i,5]: X1,Y1, x1,y1, Symbol#
        double[] m_ViewOrigin = new double[3]; // Origin of rotations for coord transformation
        double[,] m_T; // Transformatiom Matrix
        double[][][] m_Limits3D; // [2][3][2]: [2]=0=global,1=viewport,[3]=x,y,z,[2]=min, max

        // Mouse Control
        bool m_RotateOn = false;
        bool m_PanON = false;
        Point m_MouseClickLoc;
        int m_RotateFlag = 1; // 1=update, 0=do not update

        public Kanvas3D()
        {
            m_Pens = new Pen[1];
            m_Pens[0] = new Pen(Color.Black, 2);
            m_Points3D = new double[0, 7];
            m_Lines3D = new double[0, 13];
            m_PolyLines3D = new double[0][];
            m_Polygons3D = new double[0][];
            m_Symbols = new int[0][,];
            m_Zoomfactor = 1.0;
            m_ViewOrigin = new double[3];
            m_T = new double[3, 3]; for (int i = 0; i < 3; i++) m_T[i, i] = 1.0;
            m_Limits3D = new double[2][][];
            for (int i = 0; i < 2; i++) { m_Limits3D[i] = new double[3][]; for (int j = 0; j < 3; j++) m_Limits3D[i][j] = new double[2]; }
            for (int i = 0; i < 2; i++) { for (int k = 0; k < 3; k++) m_Limits3D[i][k][0] = 0; }
            for (int i = 0; i < 2; i++) { for (int k = 0; k < 3; k++) m_Limits3D[i][k][1] = 1; }
        }
        public bool SetViewAngle(int StartDir, double XAngle, double YAngle, double ZAngle)
        {
            m_T = new double[3, 3];
            if (StartDir == 0) // View plane is XY, Z is up (No rotation)
            {
                for (int i = 0; i < 3; i++) m_T[i, i] = 1.0;
            }
            else if (StartDir == 1) // View Plane is XZ, Y is down (90 rotation of X)
            {
                m_T[0, 0] = 1;
                m_T[1, 2] = -1;
                m_T[2, 1] = 1;
            }
            else if (StartDir == -1) // View Plane is XZ, Y is down (-90 rotation of X)
            {
                m_T[0, 0] = 1;
                m_T[1, 2] = 1;
                m_T[2, 1] = -1;
            }
            else if (StartDir == 2) // View Plane is YZ, X is down (90 rotation of Y)
            {
                m_T[0, 2] = 1;
                m_T[1, 1] = 1;
                m_T[2, 0] = -1;
            }
            else if (StartDir == -2) // View Plane is YZ, X is down (-90 rotation of Y)
            {
                m_T[0, 2] = -1;
                m_T[1, 1] = 1;
                m_T[2, 0] = 1;
            }
            else if (StartDir == 3) // View Plane is XY, Z is up, Y is to right (90 rotation of Z)
            {
                m_T[0, 1] = 1;
                m_T[1, 0] = -1;
                m_T[2, 2] = 1;
            }
            else if (StartDir == 3) // View Plane is XY, Z is up, Y is to left (-90 rotation of Z)
            {
                m_T[0, 1] = -1;
                m_T[1, 0] = 1;
                m_T[2, 2] = 1;
            }
            else
            {
                return false;
            }
            Rotate(XAngle, YAngle, ZAngle);
            return true;
        }

        public bool AddPoint(double[] xyz, int PenNum)
        {
            bool RetCode = false;
            if (xyz.Length == 3)
            {
                try
                {
                    if (PenNum >= m_Pens.Length) PenNum = 0;
                    int Npt = m_Points3D.GetLength(0);
                    double[,] Points = new double[Npt + 1, 7];
                    for (int i = 0; i < Npt; i++)
                    {
                        for (int j = 0; j < 7; j++)
                        {
                            Points[i, j] = m_Points3D[i, j];
                        }
                    }
                    for (int j = 0; j < 3; j++) Points[Npt, j] = xyz[j];
                    double[] xyzLocal = ViewXYZ(xyz);
                    for (int j = 0; j < 3; j++) Points[Npt, j + 3] = xyzLocal[j];
                    Points[Npt, 6] = PenNum;
                    m_Points3D = Points;
                    for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][0] > xyz[k]) m_Limits3D[0][k][0] = xyz[k] - Math.Abs(xyz[k]) / 10.0;
                    for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][1] < xyz[k]) m_Limits3D[0][k][1] = xyz[k] + Math.Abs(xyz[k]) / 10.0;
                    RetCode = true;
                }
                catch
                {
                    RetCode = false;
                }
            }
            return RetCode;
        }
        public bool AddLine(double[] xyz, int PenNum)
        {
            bool RetCode = false;
            if (xyz.Length == 6)
            {
                try
                {
                    if (PenNum >= m_Pens.Length) PenNum = 0;
                    int NLn = m_Lines3D.GetLength(0);
                    double[,] Lines = new double[NLn + 1, 13];
                    for (int i = 0; i < NLn; i++)
                    {
                        for (int j = 0; j < 13; j++)
                        {
                            Lines[i, j] = m_Lines3D[i, j];
                        }
                    }
                    for (int j = 0; j < 6; j++) Lines[NLn, j] = xyz[j];
                    double[] XYZPt = new double[3];
                    for (int iPT = 0; iPT < 2; iPT++)
                    {
                        for (int i = 0; i < 3; i++) XYZPt[i] = xyz[i];
                        double[] xyzLocal = ViewXYZ(XYZPt);
                        for (int j = 0; j < 3; j++) Lines[NLn, iPT * 3 + j + 6] = xyzLocal[j];
                    }
                    Lines[NLn, 12] = PenNum;
                    m_Lines3D = Lines;
                    for (int iPT = 0; iPT < 2; iPT++)
                    {
                        for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][0] > xyz[iPT * 3 + k]) m_Limits3D[0][k][0] = xyz[iPT * 3 + k] - Math.Abs(xyz[iPT * 3 + k]) / 10.0;
                        for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][1] < xyz[iPT * 3 + k]) m_Limits3D[0][k][1] = xyz[iPT * 3 + k] + Math.Abs(xyz[iPT * 3 + k]) / 10.0;
                    }
                    RetCode = true;
                }
                catch
                {
                    RetCode = false;
                }
            }
            return RetCode;
        }
        /// <summary>
        /// Save a polyline for drawing
        /// </summary>
        /// <param name="XYs">[npt,3]X,Y,Z of points of polyline</param>
        /// <param name="PenNum">reference to a pen number for the lines, invalid pen->use pen #0</param>
        /// <returns>true=Ok, false=failed</returns>
        new public bool AddPolyLine(double[,] xyz, int PenNum)
        {
            bool RetCode = false;
            if (xyz.GetLength(1) == 2)  // take care of 2D poilyline that has same function signature
            {
                Kanvas2D k = (Kanvas2D)this;
                RetCode = k.AddPolyLine(xyz, PenNum);
                return RetCode;
            }
            if (xyz.GetLength(1) == 3)
            {
                try
                {
                    if (PenNum >= m_Pens.Length) PenNum = 0;
                    int NPL = m_PolyLines3D.GetLength(0);
                    double[][] PLines = new double[NPL + 1][];
                    for (int i = 0; i < NPL; i++)
                    {
                        PLines[i] = m_PolyLines3D[i];
                    }
                    int nPt = xyz.GetLength(0); ;
                    PLines[NPL] = new double[6 * nPt + 1];
                    for (int j = 0; j < nPt; j++)
                    {
                        PLines[NPL][6 * j] = xyz[j, 0];
                        PLines[NPL][6 * j + 1] = xyz[j, 1];
                        PLines[NPL][6 * j + 2] = xyz[j, 2];
                    }
                    PLines[NPL][6 * nPt] = PenNum;
                    m_PolyLines3D = PLines;
                    for (int iPT = 0; iPT < nPt; iPT++)
                    {
                        for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][0] > xyz[iPT, k]) m_Limits3D[0][k][0] = xyz[iPT, k] - Math.Abs(xyz[iPT, k]) / 10.0;
                        for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][1] < xyz[iPT, k]) m_Limits3D[0][k][1] = xyz[iPT, k] + Math.Abs(xyz[iPT, k]) / 10.0;
                    }
                    RetCode = true;
                }
                catch
                {
                    RetCode = false;
                }
                return RetCode;
            }
            return false;
        }
        /// <summary>
        /// Save a polygon for drawing
        /// </summary>
        /// <param name="XYs">[npt,3]X,Y,Z of points of polygon</param>
        /// <param name="PenNum">reference to a pen number for the lines, invalid pen->use pen #0</param>
        /// <param name="BrushNum">reference to a brush number for the fill, invalid brush->use brush #0</param>
        /// <returns>true=Ok, false=failed</returns>
        new public bool AddPolygon(double[,] xyz, int PenNum, int BrushNum)
        {
            bool RetCode = false;
            if (xyz.GetLength(1) == 2)  // take care of 2D poilyline that has same function signature
            {
                Kanvas2D k = (Kanvas2D)this;
                RetCode = k.AddPolygon(xyz, PenNum, BrushNum);
                return RetCode;
            }
            if (xyz.GetLength(1) == 3)
            {
                try
                {
                    if (PenNum >= m_Pens.Length) PenNum = 0;
                    if (BrushNum >= m_Brushes.Length) BrushNum = 0;
                    int NPL = m_Polygons3D.Length;
                    double[][] PGs = new double[NPL + 1][];
                    for (int i = 0; i < NPL; i++)
                    {
                        PGs[i] = m_Polygons3D[i];
                    }
                    int nPt = xyz.GetLength(0); ;
                    PGs[NPL] = new double[6 * nPt + 2];
                    for (int j = 0; j < nPt; j++)
                    {
                        PGs[NPL][6 * j] = xyz[j, 0];
                        PGs[NPL][6 * j + 1] = xyz[j, 1];
                        PGs[NPL][6 * j + 2] = xyz[j, 2];
                    }
                    PGs[NPL][6 * nPt] = PenNum;
                    PGs[NPL][6 * nPt + 1] = BrushNum;
                    m_Polygons3D = PGs;
                    for (int iPT = 0; iPT < nPt; iPT++)
                    {
                        for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][0] > xyz[iPT, k]) m_Limits3D[0][k][0] = xyz[iPT, k] - Math.Abs(xyz[iPT, k]) / 10.0;
                        for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][1] < xyz[iPT, k]) m_Limits3D[0][k][1] = xyz[iPT, k] + Math.Abs(xyz[iPT, k]) / 10.0;
                    }
                    RetCode = true;
                }
                catch
                {
                    RetCode = false;
                }
                return RetCode;
            }
            return false;
        }
        private double[] ViewXYZ(double[] XYZ)
        {
            double[] xyz = new double[3];
            try
            {
                for (int i = 0; i < 3; i++) xyz[i] = XYZ[i] - m_ViewOrigin[i];
                double[] xyz2 = MMult(m_T, xyz);
                xyz = xyz2;
            }
            catch
            {
                xyz = null;
            }
            return xyz;
        }

        public void RotAngle(int StartDir, double XAngle, double YAngle, double ZAngle)
        {
            SetViewAngle(StartDir, XAngle, YAngle, ZAngle);
            ResetK2D();
        }
        public void Rotate(double XAngle, double YAngle, double ZAngle, int UpdateFlag = 1)
        {
            double[,] TX = new double[3, 3];
            TX[0, 0] = 1.0;
            TX[1, 1] = Math.Cos(XAngle);
            TX[1, 2] = Math.Sin(XAngle);
            TX[2, 1] = -TX[1, 2];
            TX[2, 2] = TX[1, 1];
            double[,] TY = new double[3, 3];
            TY[1, 1] = 1.0;
            TY[0, 0] = Math.Cos(YAngle);
            TY[0, 2] = -Math.Sin(YAngle);
            TY[2, 0] = -TY[0, 2];
            TY[2, 2] = TY[0, 0];
            double[,] TZ = new double[3, 3];
            TZ[2, 2] = 1.0;
            TZ[0, 0] = Math.Cos(ZAngle);
            TZ[0, 1] = Math.Sin(ZAngle);
            TZ[1, 0] = -TZ[0, 1];
            TZ[1, 1] = TZ[0, 0];
            double[,] T = MMult(TZ, MMult(TY, TX));
            double[,] saveT = Duplicate(m_T); 
            m_T = MMult(m_T, T);
            ResetK2D();
            if (UpdateFlag == 0) m_T = saveT; // if no update, then the new transformation is not saved
        }
        public void Move(double[] dxyzL)
        {
            double[,] TT = MTranspose(m_T);
            double[] dXYZ = MMult(TT, dxyzL);
            for (int i = 0; i < 3; i++) m_ViewOrigin[i] += dXYZ[i];
        }
        public void UpdatePoint(int Pt)
        {
            double[] xyz = new double[3];
            for (int i = 0; i < 3; i++) xyz[i] = m_Points3D[Pt, i] - m_ViewOrigin[i];
            xyz = MMult(MTranspose(m_T), xyz);
            for (int i = 0; i < 3; i++) m_Points3D[Pt, i + 3] = xyz[i];
        }
        public void UpdateLine(int Ln)
        {
            for (int Pt = 0; Pt < 2; Pt++)
            {
                double[] xyz = new double[3];
                for (int i = 0; i < 3; i++) xyz[i] = m_Lines3D[Ln, Pt * 3 + i] - m_ViewOrigin[i];
                xyz = MMult(MTranspose(m_T), xyz);
                for (int i = 0; i < 3; i++) m_Lines3D[Ln, Pt * 3 + i + 6] = xyz[i];
            }
        }
        public void UpdatePolyLine(int Ln)
        {
            int npt = (m_PolyLines3D[Ln].Length - 1) / 6;
            for (int Pt = 0; Pt < npt; Pt++)
            {
                double[] xyz = new double[3];
                for (int i = 0; i < 3; i++) xyz[i] = m_PolyLines3D[Ln][Pt * 6 + i] - m_ViewOrigin[i];
                xyz = MMult(MTranspose(m_T), xyz);
                for (int i = 0; i < 3; i++) m_PolyLines3D[Ln][Pt * 6 + i + 3] = xyz[i];
            }
        }
        public void UpdatePolygon(int Ln)
        {
            int npt = (m_Polygons3D[Ln].Length - 2) / 6;
            for (int Pt = 0; Pt < npt; Pt++)
            {
                double[] xyz = new double[3];
                for (int i = 0; i < 3; i++) xyz[i] = m_Polygons3D[Ln][Pt * 6 + i] - m_ViewOrigin[i];
                xyz = MMult(MTranspose(m_T), xyz);
                for (int i = 0; i < 3; i++) m_Polygons3D[Ln][Pt * 6 + i + 3] = xyz[i];
            }
        }
        public void UpdateTransformation()
        {
            for (int i = 0; i < m_Points3D.GetLength(0); i++) UpdatePoint(i);
            for (int i = 0; i < m_Lines3D.GetLength(0); i++) UpdateLine(i);
            for (int i = 0; i < m_PolyLines3D.GetLength(0); i++) UpdatePolyLine(i);
            for (int i = 0; i < m_Polygons3D.GetLength(0); i++) UpdatePolygon(i);
        }
        public void GetPic(Graphics g, int WidthPixels, int HeightPixels)
        {
            for (int i = 0; i < m_Points3D.GetLength(0); i++)
            {
                double[] XY = new double[2];
                XY[0] = m_Points3D[i, 3];
                XY[1] = m_Points3D[i, 5];
                int PenNum = Convert.ToInt16(m_Points3D[i, 6]);
                int[] XYPixle = XYWin(XY, WidthPixels, HeightPixels, m_Limits3D[1][0][0], m_Limits3D[1][0][1], m_Limits3D[1][2][0], m_Limits3D[1][2][1]);
                g.DrawRectangle(m_Pens[PenNum], XYPixle[0] - 1, XYPixle[1] - 1, 2, 2);
            }
            for (int i = 0; i < m_Lines3D.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Lines3D[i, 12]);
                int[][] XYPixle = new int[2][];
                for (int Pt = 0; Pt < 2; Pt++)
                {
                    double[] XY = new double[2];
                    XY[0] = m_Lines3D[i, Pt * 3 + 6];
                    XY[1] = m_Lines3D[i, Pt * 3 + 8];
                    XYPixle[Pt] = XYWin(XY, WidthPixels, HeightPixels, m_Limits3D[1][0][0], m_Limits3D[1][0][1], m_Limits3D[1][2][0], m_Limits3D[1][2][1]);
                }
                g.DrawLine(m_Pens[PenNum], XYPixle[0][0], XYPixle[0][1], XYPixle[1][0], XYPixle[1][1]);
            }
            for (int i = 0; i < m_PolyLines3D.GetLength(0); i++)
            {
                int npt = (m_PolyLines3D.GetLength(1) - 1) / 6;
                int PenNum = Convert.ToInt16(m_PolyLines3D[i][6 * npt]);
                int[][] XYPixle = new int[npt][];
                for (int Pt = 0; Pt < npt; Pt++)
                {
                    double[] XY = new double[2];
                    XY[0] = m_PolyLines3D[i][Pt * 3 + 1];
                    XY[1] = m_PolyLines3D[i][Pt * 3 + 3];
                    XYPixle[Pt] = XYWin(XY, WidthPixels, HeightPixels, m_Limits3D[1][0][0], m_Limits3D[1][0][1], m_Limits3D[1][2][0], m_Limits3D[1][2][1]);
                }
                for (int j = 1; j < npt; j++)
                {
                    g.DrawLine(m_Pens[PenNum], XYPixle[j - 1][0], XYPixle[j - 1][1], XYPixle[j][0], XYPixle[j][1]);
                }
            }
            for (int i = 0; i < m_Polygons3D.Length; i++)
            {
                int npt = (m_Polygons3D.GetLength(1) - 2) / 6;
                int PenNum = Convert.ToInt16(m_Polygons3D[i][6 * npt]);
                int[][] XYPixle = new int[npt][];
                for (int Pt = 0; Pt < npt; Pt++)
                {
                    double[] XY = new double[2];
                    XY[0] = m_Polygons3D[i][Pt * 3 + 1];
                    XY[1] = m_Polygons3D[i][Pt * 3 + 3];
                    XYPixle[Pt] = XYWin(XY, WidthPixels, HeightPixels, m_Limits3D[1][0][0], m_Limits3D[1][0][1], m_Limits3D[1][2][0], m_Limits3D[1][2][1]);
                }
                for (int j = 1; j < npt; j++)
                {
                    g.DrawLine(m_Pens[PenNum], XYPixle[j - 1][0], XYPixle[j - 1][1], XYPixle[j][0], XYPixle[j][1]);
                }
            }
        }
        public void ResetK2D()
        {
            SetViewOrigin(0, 0);
            UpdateTransformation();
            m_Points = new double[0, 5];
            for (int i = 0; i < m_Points3D.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Points3D[i, 6]);
                AddPoint(m_Points3D[i, 3], m_Points3D[i, 4], PenNum);
            }
            m_Lines = new double[0, 9];
            for (int i = 0; i < m_Lines3D.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Lines3D[i, 12]);
                AddLine(m_Lines3D[i, 6], m_Lines3D[i, 7], m_Lines3D[i, 9], m_Lines3D[i, 10], PenNum);
            }
            m_PolyLines = new double[0][];
            for (int i = 0; i < m_PolyLines3D.Length; i++)
            {
                int npt = (m_PolyLines3D[i].Length - 1) / 6;
                int PenNum = Convert.ToInt16(m_PolyLines3D[i][6 * npt]);
                double[,] XYs = new double[npt, 2];
                for (int j = 0; j < npt; j++)
                {
                    for (int k = 0; k < 2; k++) XYs[j, k] = m_PolyLines3D[i][6 * j + k + 3];
                }
                Kanvas2D k2D = (Kanvas2D)this;
                k2D.AddPolyLine(XYs, PenNum);
            }
            m_Polygons = new double[0][];
            for (int i = 0; i < m_Polygons3D.Length; i++)
            {
                int npt = (m_Polygons3D[i].Length - 2) / 6;
                int PenNum = Convert.ToInt16(m_Polygons3D[i][6 * npt]);
                int BrushNum = Convert.ToInt16(m_Polygons3D[i][6 * npt+1]);
                double[,] XYs = new double[npt, 2];
                for (int j = 0; j < npt; j++)
                {
                    for (int k = 0; k < 2; k++) XYs[j, k] = m_Polygons3D[i][6 * j + k + 3];
                }
                Kanvas2D k2D = (Kanvas2D)this;
                k2D.AddPolygon(XYs, PenNum, BrushNum);
            }
            ResetPicture();
        }
        private void SetViewOrigin(double Xoffset, double Yoffset)
        {
            // Find center of all points, offset is from -0.5 to +0.5
            double[,] MinMaxXYZ = new double[2, 3];
            for (int i = 0; i < 3; i++)
            {
                MinMaxXYZ[0, i] = 1.0e+6; // min values
                MinMaxXYZ[1, i] = -1.0e+6; //Max values
            }
            for (int i = 0; i < m_Points3D.GetLength(0); i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (m_Points3D[i, j] < MinMaxXYZ[0, j]) MinMaxXYZ[0, j] = m_Points3D[i, j];
                    if (m_Points3D[i, j] > MinMaxXYZ[1, j]) MinMaxXYZ[1, j] = m_Points3D[i, j];
                }
            }
            double dx = MinMaxXYZ[1, 0] - MinMaxXYZ[0, 0];
            double x0 = 0.5 * (MinMaxXYZ[1, 0] + MinMaxXYZ[0, 0]);
            double offset = Math.Min(Math.Max(Xoffset, -0.5), 0.5);
            m_ViewOrigin[0] = x0 + offset * dx;
            double dy = MinMaxXYZ[1, 1] - MinMaxXYZ[0, 1];
            double y0 = 0.5 * (MinMaxXYZ[1, 1] + MinMaxXYZ[0, 1]);
            offset = Math.Min(Math.Max(Yoffset, -0.5), 0.5);
            m_ViewOrigin[1] = y0 + offset * dy;
            m_ViewOrigin[2] = 0.5 * (MinMaxXYZ[1, 2] + MinMaxXYZ[0, 2]);
        }
        public int[] XYWin(double[] XY, int WinX, int WinY, double MinX, double MaxX, double MinY, double MaxY)
        {
            int[] XYWin = new int[2];
            XYWin[0] = Convert.ToInt16((XY[0] - MinX) * (WinX / (MaxX - MinX)));
            XYWin[1] = Convert.ToInt16(WinY - (XY[1] - MinY) * (WinY / (MaxY - MinY)));
            return XYWin;
        }

        public int GetLine(double X, double Y)
        {
            int Line = -1;
            double[] LRatio = new double[0];
            int[] Lines = ((Kanvas2D)this).GetLines(X, Y, ref LRatio);
            double Zi = -1.0e12;
            for (int i = 0; i < Lines.Length; i++)
            {
                double z1 = m_Lines3D[Lines[i], 8];
                double z2 = m_Lines3D[Lines[i], 11];
                double r = LRatio[i];
                double z = z1 + r * (z2 - z1);
                if (z > Zi) { Zi = z; Line = Lines[i]; }
            }
            return Line;
        }
        public void SetLinePen(int LineNumber, int PenNumber)
        {
            if (LineNumber >= m_Lines3D.GetLength(0)) return;
            m_Lines3D[LineNumber, 12] = PenNumber;
            m_Lines[LineNumber, 8] = PenNumber;
        }

        // -----------------------------------------------------------------------------
        // Mouse Control Functions
        // -----------------------------------------------------------------------------
        public void MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                m_PanON = true;
            }
            else if (e.Button == MouseButtons.Right)
            {
                m_RotateOn = true;
                m_RotateFlag = 0;
            }
            MouseEventArgs me = (MouseEventArgs)e;
            m_MouseClickLoc = me.Location;
        }
        public bool MouseMove(object sender, MouseEventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            Point coordinates = me.Location;
            Point Pt2 = me.Location;
            double dx = Convert.ToDouble(Pt2.X - m_MouseClickLoc.X);
            double dy = Convert.ToDouble(Pt2.Y - m_MouseClickLoc.Y);
            if (m_RotateOn)
            {
                Rotate(dy * Math.PI / WinSize()[1], dx * Math.PI / WinSize()[0], 0, m_RotateFlag);
                return true;
            }
            else if (m_PanON)
            {
                pan(-dx, dy, 0);
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool MouseUp(object sender, MouseEventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            Point Pt2 = me.Location;
            double dx = Convert.ToDouble(Pt2.X - m_MouseClickLoc.X);
            double dy = Convert.ToDouble(Pt2.Y - m_MouseClickLoc.Y);
            if (e.Button == MouseButtons.Middle)
            {
                if (m_PanON)
                {
                    m_PanON = false;
                    pan(-dx, dy, 1);
                    return true;
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (m_RotateOn)
                {
                    m_RotateOn = false;
                    m_RotateFlag = 1;
                    Rotate(dy * Math.PI / WinSize()[1], dx * Math.PI / WinSize()[1], 0, m_RotateFlag);
                    return true;
                }
            }
            else
            {
                return false;
            }
            return false;
        }
        public bool MouseWheel(object sender, MouseEventArgs e)
        {
            // If the mouse wheel delta is positive, zoonm in.
            if (e.Delta > 0)
            {
                Zoom(0.95);
                return true;
            }

            // If the mouse wheel delta is negative, zoom out.
            if (e.Delta < 0)
            {
                Zoom(1.05);
                return true;
            }
            else
            {
                return false;
            }
        }

        // -----------------------------------------------------------------------------
        // Matrix operation Functions
        // -----------------------------------------------------------------------------

        /// <summary>
        /// Return a copy of the array
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static double[,] Duplicate(double[,] A)
        {
            double[,] B = null;
            if (A != null)
            {
                B = new double[A.GetLength(0), A.GetLength(1)];
                for (int i = 0; i < A.GetLength(0); i++)
                {
                    for (int j = 0; j < A.GetLength(1); j++)
                    {
                        B[i, j] = A[i, j];
                    }
                }
            }
            return B;
        }

        /// <summary>
        /// Transpose of a 2D matrix
        /// </summary>
        /// <param name="A">matrix to be transposed</param>
        /// <returns>Transpose of A</returns>
        public static double[,] MTranspose(double[,] A)
        {
            int NRow = A.GetLength(0);
            int nCol = A.GetLength(1);
            double[,] B = new double[nCol, NRow];
            for (int i = 0; i < NRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    B[j, i] = A[i, j];
                }
            }
            return B;
        }

        /// <summary>
        /// Multiply Matrices: C = A*B
        /// </summary>
        /// <param name="A">First matrix</param>
        /// <param name="B">Second Matrix</param>
        /// <param name="RetCode">Note use as input: true=success, false=failed</param>
        /// <returns>The resulting matrix</returns>
        public static double[,] MMult(double[,] A, double[,] B, bool RetCode = true)
        {
            int M = A.GetLength(0);
            int N = A.GetLength(1);
            if (N != B.GetLength(0)) // Cannot Multiply
            {
                RetCode = false;
                return null;
            }
            int P = B.GetLength(1);
            double[,] C = new double[M, P];
            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < P; j++)
                {
                    C[i, j] = 0.0;
                    for (int k = 0; k < N; k++)
                    {
                        C[i, j] += A[i, k] * B[k, j];
                    }
                }
            }
            return C;
        }
        public static double[] MMult(double[,] A, double[] B, bool RetCode = true)
        {

            int M = A.GetLength(0);
            int N = A.GetLength(1);
            if (N != B.GetLength(0)) // Cannot Multiply
            {
                RetCode = false;
                return null;
            }
            double[] C = new double[M];
            for (int i = 0; i < M; i++)
            {
                C[i] = 0.0;
                for (int k = 0; k < N; k++)
                {
                    C[i] += A[i, k] * B[k];
                }
            }
            return C;
        }

    }
}




/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace CBridge
{
    public class Kanvas3D : Kanvas2D
    {
        double[,] m_Lines3D; // [i,13]: X1,Y1,Z1,X2,Y2,Z2, x1,y1,z1,x2,y2,z2, pen#
        double[,] m_Points3D; // [i,7]: X1,Y1,Z1, x1,y1,z1, pen#
        double[,] m_SymPoints3D; // [i,5]: X1,Y1, x1,y1, Symbol#
        double[] m_ViewOrigin = new double[3]; // Origin of rotations for coord transformation
        double[,] m_T; // Transformatiom Matrix
        double[][][] m_Limits3D; // [2][3][2]: [2]=0=global,1=viewport,[3]=x,y,z,[2]=min, max


        public Kanvas3D()
        {
            m_Pens = new Pen[1];
            m_Pens[0] = new Pen(Color.Black, 2);
            m_Lines3D = new double[0, 13];
            m_Points3D = new double[0, 7];
            m_Symbols = new int[0][,];
            m_Zoomfactor = 1.0;
            m_ViewOrigin = new double[3];
            m_T = new double[3, 3]; for (int i = 0; i < 3; i++) m_T[i, i] = 1.0;
            m_Limits3D = new double[2][][];
            for (int i = 0; i < 2; i++) { m_Limits3D[i] = new double[3][]; for (int j = 0; j < 3; j++) m_Limits3D[i][j] = new double[2]; }
            for (int i = 0; i < 2; i++) { for (int k = 0; k < 3; k++) m_Limits3D[i][k][0] = 0; }
            for (int i = 0; i < 2; i++) { for (int k = 0; k < 3; k++) m_Limits3D[i][k][1] = 1; }
        }
        public bool Initialize(int StartDir, double XAngle, double YAngle, double ZAngle)
        {
            m_T = new double[3, 3];
            if (StartDir == 0) // View plane is XY, Z is up (No rotation)
            {
                for (int i = 0; i < 3; i++) m_T[i, i] = 1.0;
            }
            else if (StartDir == 1) // View Plane is XZ, Y is down (90 rotation of X)
            {
                m_T[0, 0] = 1;
                m_T[1, 2] = -1;
                m_T[2, 1] = 1;
            }
            else if (StartDir == -1) // View Plane is XZ, Y is down (-90 rotation of X)
            {
                m_T[0, 0] = 1;
                m_T[1, 2] = 1;
                m_T[2, 1] = -1;
            }
            else if (StartDir == 2) // View Plane is YZ, X is down (90 rotation of Y)
            {
                m_T[0, 2] = 1;
                m_T[1, 1] = 1;
                m_T[2, 0] = -1;
            }
            else if (StartDir == -2) // View Plane is YZ, X is down (-90 rotation of Y)
            {
                m_T[0, 2] = -1;
                m_T[1, 1] = 1;
                m_T[2, 0] = 1;
            }
            else if (StartDir == 3) // View Plane is XY, Z is up, Y is to right (90 rotation of Z)
            {
                m_T[0, 1] = 1;
                m_T[1, 0] = -1;
                m_T[2, 2] = 1;
            }
            else if (StartDir == 3) // View Plane is XY, Z is up, Y is to left (-90 rotation of Z)
            {
                m_T[0, 1] = -1;
                m_T[1, 0] = 1;
                m_T[2, 2] = 1;
            }
            else
            {
                return false;
            }
            Rotate(XAngle, YAngle, ZAngle);
            return true;
        }

        public bool AddPoint(double[] xyz, int PenNum)
        {
            bool RetCode = false;
            if (xyz.Length == 3)
            {
                try
                {
                    if (PenNum >= m_Pens.Length) PenNum = 0;
                    int Npt = m_Points3D.GetLength(0);
                    double[,] Points = new double[Npt + 1, 7];
                    for (int i = 0; i < Npt; i++)
                    {
                        for (int j = 0; j < 7; j++)
                        {
                            Points[i, j] = m_Points3D[i, j];
                        }
                    }
                    for (int j = 0; j < 3; j++) Points[Npt, j] = xyz[j];
                    double[] xyzLocal = ViewXYZ(xyz);
                    for (int j = 0; j < 3; j++) Points[Npt, j + 3] = xyzLocal[j];
                    Points[Npt, 6] = PenNum;
                    m_Points3D = Points;
                    for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][0] > xyz[k]) m_Limits3D[0][k][0] = xyz[k] - Math.Abs(xyz[k]) / 10.0;
                    for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][1] < xyz[k]) m_Limits3D[0][k][1] = xyz[k] + Math.Abs(xyz[k]) / 10.0;
                    RetCode = true;
                }
                catch
                {
                    RetCode = false;
                }
            }
            return RetCode;
        }
        public bool AddLine(double[] xyz, int PenNum)
        {
            bool RetCode = false;
            if (xyz.Length == 6)
            {
                try
                {
                    if (PenNum >= m_Pens.Length) PenNum = 0;
                    int NLn = m_Lines3D.GetLength(0);
                    double[,] Lines = new double[NLn + 1, 13];
                    for (int i = 0; i < NLn; i++)
                    {
                        for (int j = 0; j < 13; j++)
                        {
                            Lines[i, j] = m_Lines3D[i, j];
                        }
                    }
                    for (int j = 0; j < 6; j++) Lines[NLn, j] = xyz[j];
                    double[] XYZPt = new double[3];
                    for (int iPT = 0; iPT < 2; iPT++)
                    {
                        for (int i = 0; i < 3; i++) XYZPt[i] = xyz[i];
                        double[] xyzLocal = ViewXYZ(XYZPt);
                        for (int j = 0; j < 3; j++) Lines[NLn, iPT * 3 + j + 6] = xyzLocal[j];
                    }
                    Lines[NLn, 12] = PenNum;
                    m_Lines3D = Lines;
                    for (int iPT = 0; iPT < 2; iPT++)
                    {
                        for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][0] > xyz[iPT * 3 + k]) m_Limits3D[0][k][0] = xyz[iPT * 3 + k] - Math.Abs(xyz[iPT * 3 + k]) / 10.0;
                        for (int k = 0; k < 3; k++) if (m_Limits3D[0][k][1] < xyz[iPT * 3 + k]) m_Limits3D[0][k][1] = xyz[iPT * 3 + k] + Math.Abs(xyz[iPT * 3 + k]) / 10.0;
                    }
                    RetCode = true;
                }
                catch
                {
                    RetCode = false;
                }
            }
            return RetCode;
        }
        private double[] ViewXYZ(double[] XYZ)
        {
            double[] xyz = new double[3];
            try
            {
                for (int i = 0; i < 3; i++) xyz[i] = XYZ[i] - m_ViewOrigin[i];
                double[] xyz2 = CTMath.MMult(m_T, xyz);
                xyz = xyz2;
            }
            catch
            {
                xyz = null;
            }
            return xyz;
        }

        public void RotAngle(int StartDir, double XAngle, double YAngle, double ZAngle)
        {
            Initialize(StartDir, XAngle, YAngle, ZAngle);
            ResetK2D();
        }
        public void Rotate(double XAngle, double YAngle, double ZAngle, int UpdateFlag = 1)
        {
            double[,] TX = new double[3, 3];
            TX[0, 0] = 1.0;
            TX[1, 1] = Math.Cos(XAngle);
            TX[1, 2] = Math.Sin(XAngle);
            TX[2, 1] = -TX[1, 2];
            TX[2, 2] = TX[1, 1];
            double[,] TY = new double[3, 3];
            TY[1, 1] = 1.0;
            TY[0, 0] = Math.Cos(YAngle);
            TY[0, 2] = -Math.Sin(YAngle);
            TY[2, 0] = -TY[0, 2];
            TY[2, 2] = TY[0, 0];
            double[,] TZ = new double[3, 3];
            TZ[2, 2] = 1.0;
            TZ[0, 0] = Math.Cos(ZAngle);
            TZ[0, 1] = Math.Sin(ZAngle);
            TZ[1, 0] = -TZ[0, 1];
            TZ[1, 1] = TZ[0, 0];
            double[,] T = CTMath.MMult(TZ, CTMath.MMult(TY, TX));
            double[,] saveT = CTMath.Duplicate(m_T); 
            m_T = CTMath.MMult(m_T, T);
            ResetK2D();
            if (UpdateFlag == 0) m_T = saveT; // if no update, then the new transformation is not saved
        }
        public void Move(double[] dxyzL)
        {
            double[,] TT = CTMath.MTranspose(m_T);
            double[] dXYZ = CTMath.MMult(TT, dxyzL);
            for (int i = 0; i < 3; i++) m_ViewOrigin[i] += dXYZ[i];
        }
        public void UpdatePoint(int Pt)
        {
            double[] xyz = new double[3];
            for (int i = 0; i < 3; i++) xyz[i] = m_Points3D[Pt, i] - m_ViewOrigin[i];
            xyz = CTMath.MMult(CTMath.MTranspose(m_T), xyz);
            for (int i = 0; i < 3; i++) m_Points3D[Pt, i + 3] = xyz[i];
        }
        public void UpdateLine(int Ln)
        {
            for (int Pt = 0; Pt < 2; Pt++)
            {
                double[] xyz = new double[3];
                for (int i = 0; i < 3; i++) xyz[i] = m_Lines3D[Ln, Pt*3  + i] - m_ViewOrigin[i];
                xyz = CTMath.MMult(CTMath.MTranspose(m_T), xyz);
                for (int i = 0; i < 3; i++) m_Lines3D[Ln, Pt * 3 + i + 6] = xyz[i];
            }
        }
        public void UpdateTransformation()
        {
            for (int i = 0; i < m_Points3D.GetLength(0); i++) UpdatePoint(i);
            for (int i = 0; i < m_Lines3D.GetLength(0); i++) UpdateLine(i);
        }
        public void GetPic(Graphics g, int WidthPixels, int HeightPixels)
        {
            for (int i = 0; i < m_Points3D.GetLength(0); i++)
            {
                double[] XY = new double[2];
                XY[0] = m_Points3D[i, 3];
                XY[1] = m_Points3D[i, 5];
                int PenNum = Convert.ToInt16(m_Points3D[i, 6]);
                int[] XYPixle = XYWin(XY, WidthPixels, HeightPixels, m_Limits3D[1][0][0], m_Limits3D[1][0][1], m_Limits3D[1][2][0], m_Limits3D[1][2][1]);
                g.DrawRectangle(m_Pens[PenNum], XYPixle[0] - 1, XYPixle[1] - 1, 2, 2);
            }
            for (int i = 0; i < m_Lines3D.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Lines3D[i, 12]);
                int[][] XYPixle = new int[2][];
                for (int Pt = 0; Pt < 2; Pt++)
                {
                    double[] XY = new double[2];
                    XY[0] = m_Lines3D[i, Pt * 3 + 6];
                    XY[1] = m_Points3D[i, Pt * 3 + 8];
                    XYPixle[Pt] = XYWin(XY, WidthPixels, HeightPixels, m_Limits3D[1][0][0], m_Limits3D[1][0][1], m_Limits3D[1][2][0], m_Limits3D[1][2][1]);
                }
                g.DrawLine(m_Pens[PenNum], XYPixle[0][0], XYPixle[0][1], XYPixle[1][0], XYPixle[1][1]);
            }
        }
        public void ResetK2D()
        {
            SetViewOrigin(0, 0);
            UpdateTransformation();
            m_Points = new double[0, 5];
            for (int i = 0; i < m_Points3D.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Points3D[i, 6]);
                AddPoint(m_Points3D[i, 3], m_Points3D[i, 4], PenNum);
            }
            m_Lines = new double[0, 9];
            for (int i = 0; i < m_Lines3D.GetLength(0); i++)
            {
                int PenNum = Convert.ToInt16(m_Lines3D[i, 12]);
                AddLine(m_Lines3D[i, 6], m_Lines3D[i, 7], m_Lines3D[i, 9], m_Lines3D[i, 10], PenNum);
            }
            ResetPicture();
        }
        private void SetViewOrigin(double Xoffset, double Yoffset)
        {
            // Find center of all points, offset is from -0.5 to +0.5
            double[,] MinMaxXYZ = new double[2, 3];
            for (int i = 0; i < 3; i++)
            {
                MinMaxXYZ[0, i] = 1.0e+6; // min values
                MinMaxXYZ[1, i] = -1.0e+6; //Max values
            }
            for (int i = 0; i < m_Points3D.GetLength(0); i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (m_Points3D[i, j] < MinMaxXYZ[0, j]) MinMaxXYZ[0, j] = m_Points3D[i, j];
                    if (m_Points3D[i, j] > MinMaxXYZ[1, j]) MinMaxXYZ[1, j] = m_Points3D[i, j];
                }
            }
            double dx = MinMaxXYZ[1, 0] - MinMaxXYZ[0, 0];
            double x0 = 0.5 * (MinMaxXYZ[1, 0] + MinMaxXYZ[0, 0]);
            double offset = Math.Min(Math.Max(Xoffset, -0.5), 0.5);
            m_ViewOrigin[0] = x0 + offset * dx;
            double dy = MinMaxXYZ[1, 1] - MinMaxXYZ[0, 1];
            double y0 = 0.5 * (MinMaxXYZ[1, 1] + MinMaxXYZ[0, 1]);
            offset = Math.Min(Math.Max(Yoffset, -0.5), 0.5);
            m_ViewOrigin[1] = y0 + offset * dy;
            m_ViewOrigin[2] = 0.5 * (MinMaxXYZ[1, 2] + MinMaxXYZ[0, 2]);
        }
        public int[] XYWin(double[] XY, int WinX, int WinY, double MinX, double MaxX, double MinY, double MaxY)
        {
            int[] XYWin = new int[2];
            XYWin[0] = Convert.ToInt16((XY[0] - MinX) * (WinX / (MaxX - MinX)));
            XYWin[1] = Convert.ToInt16(WinY - (XY[1] - MinY) * (WinY / (MaxY - MinY)));
            return XYWin;
        }
    }
}
*/