﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBridge
{
    /// <summary>
    ///  This class defines the abstract Base Section 
    ///  
    ///  
    ///  Depends on: 
    /// </summary>
    public abstract class Section
    {
        bool m_InUse;       // true if this section is in use

        string m_Name;
        string m_Type;
        string m_Shape;

        string m_GirderName;

        int m_NumberOfGirders;
        double m_GirderSpacing;
        double[] m_GirderDistances;

        double m_LeftCurb;
        double m_RightCurb;

        double m_SlabThickness;
        double m_SlabWidth;

        /// <summary>
        /// get/Set for parameters
        /// </summary>
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string SectionType
        {
            get { return m_Type; }
            set { m_Type = value; }
        }

        public string SectionShape
        {
            get { return m_Shape; }
            set { m_Shape = value; }
        }

        public string GirderName
        {
            get { return m_GirderName; }
            set { m_GirderName = value; }
        }

        public bool InUse
        {
            get { return m_InUse; }
            set { m_InUse = value; }
        }

        public double[] Prop
        {
            get
            {
                double[] Prop1 = new double[6];
                Prop1[0] = m_NumberOfGirders;
                Prop1[1] = m_GirderSpacing;

                Prop1[2] = m_LeftCurb;
                Prop1[3] = m_RightCurb;

                Prop1[4] = m_SlabThickness;
                Prop1[5] = m_SlabWidth;
      
                return Prop1;
            }

            set
            {
                m_NumberOfGirders = Convert.ToInt32(value[0]);
                m_GirderSpacing = value[1];

                m_LeftCurb = value[2];
                m_RightCurb = value[3];

                m_SlabThickness = value[4];
                m_SlabWidth = value[5];
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Section()
        {
            m_Name = "";
            m_Type = "";
            m_Shape = "";

            InUse = false;

            m_NumberOfGirders = 0;
            m_GirderSpacing = 0;

            m_LeftCurb = 0;
            m_RightCurb = 0;

            m_SlabThickness = 0;
            m_SlabWidth = 0;

        }

        /// <summary>
        /// Cosntructor with Section rpoperties
        /// </summary>
        /// <param name="iName">name of Section</param>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        public Section(string iName, string SectionType, string SectionShape, string Girdername, double[] iProp)
        {
            Name = iName;
            InUse = false;
            SetProp(iProp);
        }

        /// <summary>
        /// Cosntructor with Section rpoperties
        /// </summary>
        /// <param name="iName">name of Section</param>
        /// <param name="iProp">E, nu(poisson ratio), gamma (unit weight)</param>
        public Section(string iName)
        {
            Name = iName;
            InUse = false;
        }

        /// <summary>
        /// sets the Section properties
        /// </summary>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        /// <returns>false=failed, true=successful</returns>
        public void SetProp(double[] iProp)
        {

            m_NumberOfGirders = Convert.ToInt32(iProp[0]);
            m_GirderSpacing = iProp[1];

            m_LeftCurb = iProp[2];
            m_RightCurb = iProp[3];

            m_SlabThickness = iProp[4];
            m_SlabWidth = iProp[5];

        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    ///  This class defines Slab on Grid Bridge Secection 
    ///  
    ///  Depends on: 
    /// </summary>
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public class SectionSlabOnGirder : Section
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SectionSlabOnGirder() : base()
        {

        }

        public SectionSlabOnGirder(string iName) : base( iName)
        {

        }

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    ///  This class defines Slab on Grid Bridge Secection 
    ///  
    ///  Depends on: 
    /// </summary>
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public class SectionBoxGirder : Section
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SectionBoxGirder() : base()
        {

        }

        public SectionBoxGirder(string iName) : base( iName)
        {

        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    ///  This class defines Slab on Grid Bridge Secection 
    ///  
    ///  Depends on: 
    /// </summary>
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public class SectionSolidSlab : Section
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SectionSolidSlab() : base()
        {

        }


        public SectionSolidSlab(string iName) : base( iName)
        {

        }


    }


}
