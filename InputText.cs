﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CBridge
{
    public class InputText
    {
        string m_Data;
        StructureModel m_Structure;

        public string[] m_Commands = null;
        public string[] m_MessageLog;

        public InputText(string FData)
        {
            m_Data = FData;
            m_Structure = new StructureModel();
        }
        /// <summary>
        /// The Read an input file and create a structure
        /// </summary>
        /// 
        public StructureModel GetStructure()
        {
            m_Structure = new StructureModel();

            string[] iCommands = m_Data.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < iCommands.Length; i++)
            {
                if (iCommands[i].Substring(0, 1) == "*") continue; ;  // Comment lines start with "*"
                DoOneCommand(iCommands[i]);
            }

            return m_Structure;
        }

        /// <summary>
        /// Read the comamnd line and hand it off for processing
        /// </summary>
        /// <param name="iCommand">Command keyword</param>
        /// 
        private void DoOneCommand(string iCommand)
        {
            if (iCommand.Substring(0, 1) == "*") return;  // Comment lines start with "*"

            string[] delim = { " ", ",", ";" };
            string[] Words;
            
            string iCmd = iCommand.Substring(0, 2).ToUpper();  // Minimum two letter match
            
            switch (iCmd)
            {
                case "IN":  // Input File
                    Words = iCommand.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                    try
                    {
                        string FName = Words[1];
                        string FData;
                        using (StreamReader sr = new StreamReader(FName))
                        {
                            FData = sr.ReadToEnd();
                            string[] iCommands = FData.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                            for (int i = 0; i < iCommands.Length; i++)
                            {
                                DoOneCommand(iCommands[i]);
                            }
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Input file " + Words[1] + " could not be openned.");
                    }
                    break;

                case "SE":  //   Settings Command. 
                    if (!DoSettingsCommand(iCommand)) MessageBox.Show("Settings Command failed");
                    break;
            
                case "MA":  //   Material Command. 
                    if (!DoMaterialCommand(iCommand)) MessageBox.Show("Material Command failed");
                    break;

                case "AL":  //   Alignment Command. 
                    if (!DoAlignmentCommand(iCommand)) MessageBox.Show("Alignment Command failed");
                    break;

                default:
                    //MessageBox.Show("Invalid Command '" + iCommand + "'.");
                    return;
            }
        }
        /// <summary>
        /// Processes a Settings Command in the form of 
        /// "Setting Gravity i j k" Gravity direction multipliers to be multiplied by the weight (NOT gravity in units of lengtyh/time^2)
        /// </summary>
        /// <param name="iCommand">The Command</param>
        /// <returns></returns>
        /// 
        private bool DoSettingsCommand(string iCommand)
        {
            bool RetCode = false;  // Not successful
            string[] delim = { " ", ",", ";" };
            string[] Words;
            try
            {
                int iw = 1;
                Words = iCommand.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                string Word2 = Words[iw].Substring(0, 1).ToUpper(); iw += 1;
                if (Word2 == "G")  // Set Gravity
                {
                    double[] gFactor = new double[3];
                    gFactor[0] = Convert.ToDouble(Words[iw]); iw += 1;
                    gFactor[1] = Convert.ToDouble(Words[iw]); iw += 1;
                    gFactor[2] = Convert.ToDouble(Words[iw]); iw += 1;
                    if (m_Structure.SetGravity(gFactor))
                    {
                     // FEUtil.Insert(ref m_Commands, iCommand);
                        RetCode = true;
                    }
                }
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }

        /// <summary>
        /// Processes a Material Command in the form of 
        /// "Material Add name E, pr, alpha": adds the node or reises it if same name exists
        /// "Material Remove Name": removes the material with name "Name" if found
        /// </summary>
        /// <param name="iCommand">Material Command</param>
        /// <returns></returns>
        /// 
        private bool DoMaterialCommand(string iCommand)
        {
            bool RetCode = false;  // Not successful
            string[] delim = { " ", ",", ";" };
            string[] Words;
            try
            {
                int iw = 1;
                Words = iCommand.Split(delim, StringSplitOptions.RemoveEmptyEntries);

                string Word2 = Words[iw].Substring(0, 1).ToUpper(); iw += 1;
                
                if (Word2 == "A")  // Add or replace the node
                {
                    double[] Mprop = new double[4];

                    string Name = Words[iw]; iw += 1;

                    Mprop[0] = Convert.ToDouble(Words[iw]); iw += 1;
                    Mprop[1] = Convert.ToDouble(Words[iw]); iw += 1;
                    Mprop[2] = Convert.ToDouble(Words[iw]); iw += 1;
                    Mprop[3] = Convert.ToDouble(Words[iw]); iw += 1;

                    if (m_Structure.MaterialAddRevise("Concrete", Name, Mprop))
                    {
                        //FEUtil.Insert(ref m_Commands, iCommand);
                        RetCode = true;
                    }
                }
                else if (Word2 == "R") // Remove the node
                {
                    string Name = Words[iw]; iw += 1;

                    RetCode = m_Structure.MaterialRemove("Concrete", Name);
                }
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }

        
        /// <summary>
        /// Processes a Material Command in the form of 
        /// "Material Add name E, pr, alpha": adds the node or reises it if same name exists
        /// "Material Remove Name": removes the material with name "Name" if found
        /// </summary>
        /// <param name="iCommand">Material Command</param>
        /// <returns></returns>
        /// 
        private bool DoAlignmentCommand(string iCommand)
        {
            bool RetCode = false;  // Not successful
            string[] delim = { " ", ",", ";" };
            string[] Words;
            try
            {
                int iw = 1;
                Words = iCommand.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                string Word2 = Words[iw].Substring(0, 1).ToUpper(); iw += 1;

                if (Word2 == "S")  // Set Gravity
                {
                    string curveName; 
                    double[] startXYZ = new double[3];

                    double[] crvHorizontal = new double[3];
                    double azimuth = 5;
                                    
                    curveName   = (Words[iw]); iw += 1;
                    
                    startXYZ[0] = Convert.ToDouble(Words[iw]); iw += 1;
                    startXYZ[1] = Convert.ToDouble(Words[iw]); iw += 1;
                    startXYZ[2] = Convert.ToDouble(Words[iw]); iw += 1;

                    if (m_Structure.AlignmentAdd(curveName, startXYZ, azimuth, startXYZ, startXYZ));
                    {
                        // FEUtil.Insert(ref m_Commands, iCommand);
                        RetCode = true;
                    }
                }
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;

        }
    }
}
