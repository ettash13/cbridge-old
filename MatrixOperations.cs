using System;
using System.Collections.Generic;
using System.Text;
//using CSML;
using System.Windows.Forms;

namespace CBridge
{
    class MatrixOperations
    {
        static double[][] MatrixCreate(int rows, int cols)
        {
            // creates a matrix initialized to all 0.0s
            // do error checking here?
            double[][] result = new double[rows][];
            for (int i = 0; i < rows; ++i)
                result[i] = new double[cols]; // auto init to 0.0
            return result;
        }
        /**************************************************************************************
         * this function loops thru the 3rd dimension to see if it is blank for specified dim 1 and 2
         * *************************************************************************************/
        public static bool RowIsBlank(string[, ,] array, int d1, int d2)
        {
            for (int i = 0; i < array.GetLength(2); i++)
            {
                if (array[d1, d2, i] == null || array[d1, d2, i] == "")
                { }
                else { return false; }//row isn't blank
            }
            return true;//row is blank
        }//end function
        /**************************************************************************
         * ************************************************************************/
        public static double CvtstTodbl(string s)
        {
            double d = 0;
            try
            {
                if (s == "") d = 0;
                else if (s == null) d = 0;
                else d = Double.Parse(s);
            }
            catch { }
            return d;
        }//end function
        /**************************************************************************
         * ************************************************************************/
        public static int CvtstToint(string s)
        {
            int i;
            if (s == "") i = 0;
            else if (s == null) i = 0;
            else i = int.Parse(s);
            return i;
        }//end function
        /**************************************************************************
         * ************************************************************************/
        internal static double[][] MatrixProduct(double[][] matrixA, double[][] matrixB)
        {
            int aRows = matrixA.Length; int aCols = matrixA[0].Length;
            int bRows = matrixB.Length; int bCols = matrixB[0].Length;
            double[][] result = MatrixCreate(aRows, bCols);
            if (aCols != bRows)
            {
                throw new Exception("aCols != bRows");
            }

            if (aRows != bCols)
            {
                //double[][] result = MatrixCreate(aRows, bCols);
                for (int i = 0; i < aRows; ++i) // each row of A
                    for (int j = 0; j < bCols; ++j) // each col of B
                        for (int k = 0; k < aCols; ++k)
                            result[i][j] += matrixA[i][k] * matrixB[k][j];
            }
            else
            {
                //double[][] result = MatrixCreate(aRows, bCols);
                for (int i = 0; i < aRows; ++i) // each row of A
                    for (int j = 0; j < bCols; ++j) // each col of B
                        for (int k = 0; k < aCols; ++k)
                            result[i][j] += matrixA[i][k] * matrixB[k][j];
            }
            return result;
        }

        /// <summary>
        /// matrix multiplication A*B
        /// </summary>
        /// <param name="A">First Matrix</param>
        /// <param name="B">Second Matrix</param>
        /// <returns>C = A*B</returns>
        public static double[] MMult(double[,] A, double[] B)
        {
            if (A.GetLength(1) != B.GetLength(0)) return null;
            int NR1 = A.GetLength(0);
            int NC1 = A.GetLength(1);
            double[] C = new double[NR1];
            for (int i = 0; i < NR1; i++)
            {
                C[i] = 0.0;
                for (int k = 0; k < NC1; k++)
                {
                    C[i] += A[i, k] * B[k];
                }
            }
            return C;
        }

        public static double[,] MMult(double[,] A, double[,] B)
        {
            if (A.GetLength(1) != B.GetLength(0)) return null;
            int NR1 = A.GetLength(0);
            int NC1 = A.GetLength(1);
            int NC2 = B.GetLength(1);
            double[,] C = new double[NR1, NC2];
            for (int i = 0; i < NR1; i++)
            {
                for (int j = 0; j < NC2; j++)
                {
                    C[i, j] = 0.0;
                    for (int k = 0; k < NC1; k++)
                    {
                        C[i, j] += A[i, k] * B[k, j];
                    }
                }
            }
            return C;
        }

        internal static double[,] Transpose(double[,] m)
        {
            double[,] t = new double[m.GetLength(1), m.GetLength(0)];
            for (int i = 0; i < m.GetLength(0); i++)
                for (int j = 0; j < m.GetLength(1); j++)
                    t[j, i] = m[i, j];

            return t;
        }
        /************************************************************************
         * *******************************************************************/
        public static string[,] ConvertSysArrayToStringArray(System.Array ary)
        {
            string[,] newary = new string[ary.GetLength(0), ary.GetLength(1)];

            for (int i = 0; i < ary.GetLength(0); i++)
            {
                for (int j = 0; j < ary.GetLength(1); j++)
                {

                    if (ary.GetValue(i + 1, j + 1) == null) newary[i, j] = "";
                    else
                    {
                        newary[i, j] = (string)ary.GetValue(i + 1, j + 1).ToString();
                    }
                }
            }
            newary = deleteRemarkRows(newary, 0);//delete remarks (0th column)
            return newary;
        }//end function
        /**********************************************************************
         * **********************************************************************/
        public static string[,] deleteRemarkRows(string[,] ary, int col)
        {
            for (int i = 0; i < ary.GetLength(0); i++)
            {
                int j = col;
                if (ary[i, j] != null)
                {
                    if (ary[i, j].ToString().ToUpper() == "REM")//skip entire row if there is a remark line
                    {
                        DeleteRow(ref ary, i);
                        i--;
                    }
                }
            }
            return ary;
        }//end function
        /*******************************************************************************************
* 
* *****************************************************************************************/
        public static void addObject(double d, ref double[] Array, int locInArray)
        {
            //this function adds a beam into a Beams Array at a given location within the array
            //copy array so you can resize it
            double[] ArrayCopy = new double[Array.Length];
            for (int i = 0; i < Array.Length; i++)
            {
                ArrayCopy[i] = Array[i];
            }
            Array = new double[ArrayCopy.Length + 1];
            //add everything that used to be in the array up to the locInArray
            for (int i = 0; i < locInArray; i++)
            {
                Array[i] = ArrayCopy[i];
            }
            double saveDb = d;
            for (int i = locInArray; i < Array.Length; i++)
            {
                double nxtDb;
                if (ArrayCopy.Length == i) { nxtDb = 0; }
                else { nxtDb = ArrayCopy[i]; }
                Array[i] = 0;
                Array[i] = saveDb;
                saveDb = nxtDb;
                if (nxtDb == 0)
                {
                    break;
                }
            }
        }//end function
        /****************************************************************************
         * ****************************************************************************/
        public static void addObject(int d, ref int[] Array, int locInArray)
        {
            //this function adds a beam into a Beams Array at a given location within the array
            //copy array so you can resize it
            int[] ArrayCopy = new int[Array.Length];
            for (int i = 0; i < Array.Length; i++)
            {
                ArrayCopy[i] = Array[i];
            }
            Array = new int[ArrayCopy.Length + 1];
            //add everything that used to be in the array up to the locInArray
            for (int i = 0; i < locInArray; i++)
            {
                Array[i] = ArrayCopy[i];
            }
            int saveDb = d;
            for (int i = locInArray; i < Array.Length; i++)
            {
                int nxtDb;
                if (ArrayCopy.Length == i) { nxtDb = 0; }
                else { nxtDb = ArrayCopy[i]; }
                Array[i] = 0;
                Array[i] = saveDb;
                saveDb = nxtDb;
                if (nxtDb == 0)
                {
                    break;
                }
            }
        }//end function
        /****************************************************************************
         * ****************************************************************************/

        public static void addObject(string s, ref string[] Array, int locInArray)
        {
            //this function adds a beam into a Beams Array at a given location within the array
            //copy array so you can resize it
            string[] ArrayCopy = new string[Array.Length];
            for (int i = 0; i < Array.Length; i++)
            {
                ArrayCopy[i] = Array[i];
            }
            Array = new string[ArrayCopy.Length + 1];
            //add everything that used to be in the array up to the locInArray
            for (int i = 0; i < locInArray; i++)
            {
                Array[i] = ArrayCopy[i];
            }
            string save = s;
            for (int i = locInArray; i < Array.Length; i++)
            {
                string nxtDb;
                if (ArrayCopy.Length == i) { nxtDb = ""; }
                else { nxtDb = ArrayCopy[i]; }
                Array[i] = "";
                Array[i] = save;
                save = nxtDb;
                if (nxtDb == "")
                {
                    break;
                }
            }
        }//end function
        /****************************************************************************
         * ****************************************************************************/
        /****************************************************************************
 * ****************************************************************************/
        /// <summary>
        /// Adds an object to the last line of an array
        /// </summary>
        /// <param name="s">object to add</param>
        /// <param name="Array">array to add s to</param>
        public static void addObject(string s, ref string[] Array)
        {
            if (Array == null) Array = new string[0];
            int locInArray = Array.Length;
            //this function adds a beam into a Beams Array at a given location within the array
            //copy array so you can resize it
            string[] ArrayCopy = new string[Array.Length];
            for (int i = 0; i < Array.Length; i++)
            {
                ArrayCopy[i] = Array[i];
            }
            Array = new string[ArrayCopy.Length + 1];
            //add everything that used to be in the array up to the locInArray
            for (int i = 0; i < locInArray; i++)
            {
                Array[i] = ArrayCopy[i];
            }
            string save = s;
            for (int i = locInArray; i < Array.Length; i++)
            {
                string nxtDb;
                if (ArrayCopy.Length == i) { nxtDb = ""; }
                else { nxtDb = ArrayCopy[i]; }
                Array[i] = "";
                Array[i] = save;
                save = nxtDb;
                if (nxtDb == "")
                {
                    break;
                }
            }
        }//end function
        /****************************************************************************
         * ****************************************************************************/
        public static void addObject(char s, ref char[] Array, int locInArray)
        {
            //this function adds a beam into a Beams Array at a given location within the array
            //copy array so you can resize it
            char[] ArrayCopy = new char[Array.Length];
            for (int i = 0; i < Array.Length; i++)
            {
                ArrayCopy[i] = Array[i];
            }
            Array = new char[ArrayCopy.Length + 1];
            //add everything that used to be in the array up to the locInArray
            for (int i = 0; i < locInArray; i++)
            {
                Array[i] = ArrayCopy[i];
            }
            char save = s;
            for (int i = locInArray; i < Array.Length; i++)
            {
                char nxtDb;
                if (ArrayCopy.Length == i) { nxtDb = Char.Parse(""); }
                else { nxtDb = ArrayCopy[i]; }
                Array[i] = Char.Parse("");
                Array[i] = save;
                save = nxtDb;
                if (nxtDb == Char.Parse(""))
                {
                    break;
                }
            }
        }//end function
        /****************************************************************************
         * ****************************************************************************/
        public static string[,] addRow(string[] row, ref string[,] A, int rowInArray)
        {
            if (row.Length > A.GetLength(1))//if row contains more columns than A
            {
                string[,] nullcols = new string[0, row.Length - A.GetLength(1)];
                addColumnsToArray2D(ref A, nullcols, A.GetLength(1));
            }
            string[,] B = Duplicate(A);
            A = new string[A.GetLength(0) + 1, A.GetLength(1)];
            //populate A up to rowInArray
            for (int i = 0; i < rowInArray; i++)
            {
                for (int j = 0; j < B.GetLength(1); j++)
                {
                    A[i, j] = B[i, j];
                }
            }
            //insert row rowInArray
            for (int j = 0; j < row.Length; j++) A[rowInArray, j] = row[j];
            //populate A after rowInArray
            for (int i = rowInArray; i < B.GetLength(0); i++)
            {
                for (int j = 0; j < B.GetLength(1); j++)
                {
                    A[i + 1, j] = B[i, j];
                }
            }
            return A;
        }//end function
        public static string[,] addRows(string[,] rows, ref string[,] A, int rowInArray)
        {
            if (rows != null)
            {
                for (int i = 0; i < rows.GetLength(0); i++)
                {
                    string[] row = new string[A.GetLength(1)];
                    for (int c = 0; c < row.Length; c++) row[c] = rows[i, c];
                    addRow(row, ref A, rowInArray + i);
                }
            }
            return A;
        }//end function
        public static double[,] addRows(double[,] rows, ref double[,] A, int rowInArray)
        {
            for (int i = 0; i < A.GetLength(0); i++)
            {
                double[] row = new double[A.GetLength(1)];
                for (int c = 0; c < row.Length; c++) row[c] = rows[i, c];
                addRow(row, ref A, rowInArray + i);
            }
            return A;
        }//end function
        public static string[] addRows(string[] rows, ref string[] A, int rowInArray)
        {
            for (int i = 0; i < rows.Length; i++)
            {
                addObject(rows[i], ref A, rowInArray + i);
            }
            return A;
        }//end function
        /****************************************************************************
         * ****************************************************************************/
        public static double[,] addRow(double[] row, ref double[,] A, int rowInArray)
        {
            double[,] B = Duplicate(A);
            A = new double[A.GetLength(0) + 1, A.GetLength(1)];
            //populate A up to rowInArray
            for (int i = 0; i < rowInArray; i++)
            {
                for (int j = 0; j < B.GetLength(1); j++)
                {
                    A[i, j] = B[i, j];
                }
            }
            //insert row rowInArray
            for (int j = 0; j < row.Length; j++) A[rowInArray, j] = row[j];
            //populate A after rowInArray
            for (int i = rowInArray; i < B.GetLength(0); i++)
            {
                for (int j = 0; j < B.GetLength(1); j++)
                {
                    A[i + 1, j] = B[i, j];
                }
            }
            return A;
        }//end function

        /****************************************************************************
         * ****************************************************************************/
        //public static Span[,] addRow(Span[] row, ref Span[,] A, int rowInArray)























            

        /****************************************************************************
         * ****************************************************************************/
        public static double[][,] addArray(double[,] a, ref double[][,] aa, int locationInAA)
        {
            double[][,] bb = Duplicate(aa);
            aa = new double[aa.GetLength(0) + 1][,];
            //for (int i = 0; i < aa.GetLength(0); i++) aa[i] = new double[bb[i].GetLength(0), bb[i].GetLength(1)];

            //populate aa up to location
            for (int i = 0; i < locationInAA; i++)
            {
                aa[i] = bb[i];
            }
            //insert array at locinAA
            aa[locationInAA] = a;
            //populate aa after locationinaa
            for (int i = locationInAA; i < bb.GetLength(0); i++)
            {
                aa[i + 1] = bb[i];
            }
            trimNullArrays(ref aa);
            return aa;
        }//end function
        /****************************************************************************
         * ****************************************************************************/
        public static double[][,] addArray(double[][,] a, ref double[][,] aa, int locationInAA)
        {
            double[][,] bb = Duplicate(aa);
            aa = new double[aa.GetLength(0) + a.Length][,];

            //populate aa up to location
            for (int i = 0; i < locationInAA; i++)
            {
                aa[i] = bb[i];
            }
            //insert arrays at locinAA
            for (int i = 0; i < a.Length; i++)
            {
                aa[i + locationInAA] = a[i];
            }
            //populate aa after locationinaa
            for (int i = locationInAA + a.Length; i < bb.GetLength(0); i++)
            {
                aa[i + 1] = bb[i];
            }
            trimNullArrays(ref aa);
            return aa;
        }//end function
        /****************************************************************************
         * ****************************************************************************/
        public static string[][,] addArray(string[,] a, ref string[][,] aa, int locationInAA)
        {
            string[][,] bb = Duplicate(aa);
            aa = new string[aa.GetLength(0) + 1][,];
            //for (int i = 0; i < aa.GetLength(0); i++) aa[i] = new double[bb[i].GetLength(0), bb[i].GetLength(1)];

            //populate aa up to location
            for (int i = 0; i < locationInAA; i++)
            {
                aa[i] = bb[i];
            }
            //insert array at locinAA
            aa[locationInAA] = a;
            //populate aa after locationinaa
            for (int i = locationInAA; i < bb.GetLength(0); i++)
            {
                aa[i + 1] = bb[i];
            }
            trimNullArrays(ref aa);
            return aa;
        }//end function
        /****************************************************************************
         * ****************************************************************************/
        public static double[][,] trimNullArrays(ref double[][,] aa)
        {
            for (int i = 0; i < aa.Length; i++)
            {
                if (aa[i] == null)
                {
                    double[][,] aacopy = aa;
                    aa = new double[i][,];
                    for (int j = 0; j < aa.Length; j++)
                    {
                        aa[j] = aacopy[j];
                    }
                }
            }
            return aa;
        }
        /****************************************************************************
         * ****************************************************************************/
        public static string[][,] trimNullArrays(ref string[][,] aa)
        {
            for (int i = 0; i < aa.Length; i++)
            {
                if (aa[i] == null)
                {
                    string[][,] aacopy = aa;
                    aa = new string[i][,];
                    for (int j = 0; j < aa.Length; j++)
                    {
                        aa[j] = aacopy[j];
                    }
                }
            }
            return aa;
        }
        /****************************************************************************
         * ****************************************************************************/
        internal static double[] Add(double[] M1, double[] M2)
        {
            int nRows1 = M1.GetLength(0);
            int nRows2 = M2.GetLength(0);

            if (nRows1 != nRows2)
                throw new Exception("Rows not similar");

            double[] result = new double[nRows1];

            for (int i = 0; i < nRows1; i++)
            {
                result[i] = M1[i] + M2[i];
            }

            return result;
        }//end function
        /****************************************************************************
         * ****************************************************************************/
        internal static double[,] Add(double[,] M1, double[,] M2)
        {
            int nRows1 = M1.GetLength(0);
            int nCols1 = M1.GetLength(1);
            int nRows2 = M2.GetLength(0);
            int nCols2 = M2.GetLength(1);

            if (nRows1 != nRows2 || nCols1 != nCols2)
                throw new Exception("Rows and Columns not similar");

            double[,] result = new double[nRows1, nCols1];

            for (int i = 0; i < nRows1; i++)
            {
                for (int j = 0; j < nCols1; j++)
                {
                    result[i, j] = M1[i, j] + M2[i, j];
                }
            }

            return result;
        }//end function
        /****************************************************************************
 * ****************************************************************************/
        internal static double[, ,] Add(double[, ,] M1, double[, ,] M2)
        {
            int n1Dim1 = M1.GetLength(0);
            int n2Dim1 = M1.GetLength(1);
            int n3Dim1 = M1.GetLength(2);
            int n1Dim2 = M2.GetLength(0);
            int n2Dim2 = M2.GetLength(1);
            int n3Dim2 = M2.GetLength(2);

            if (n1Dim1 != n1Dim2 || n2Dim1 != n2Dim2 || n3Dim1 != n3Dim2)
                throw new Exception("Rows and Columns not similar");

            double[, ,] result = new double[n1Dim1, n2Dim1, n3Dim1];

            for (int i = 0; i < n1Dim1; i++)
            {
                for (int j = 0; j < n2Dim1; j++)
                {
                    for (int k = 0; k < n3Dim1; k++)
                    {
                        result[i, j, k] = M1[i, j, k] + M2[i, j, k];
                    }
                }
            }
            return result;
        }//end function
        /****************************************************************************
 * ****************************************************************************/
        internal static double[][,] Add(double[][,] M1, double[][,] M2)
        {
            double[][,] result = new double[M1.Length][,];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new double[M1[i].GetLength(0), M1[i].GetLength(1)];
                for (int j = 0; j < result[i].GetLength(0); j++)
                {
                    for (int k = 0; k < result[i].GetLength(1); k++)
                    {
                        result[i][j, k] = M1[i][j, k] + M2[i][j, k];
                    }
                }
            }
            return result;
        }//end function
        /****************************************************************************
* ****************************************************************************/
        internal static double[][,,] Add(double[][,,] M1, double[][,,] M2)
        {
            double[][,,] result = new double[M1.Length][,,];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new double[M1[i].GetLength(0), M1[i].GetLength(1), M1[i].GetLength(2)];
                for (int j = 0; j < result[i].GetLength(0); j++)
                {
                    for (int k = 0; k < result[i].GetLength(1); k++)
                    {
                        for (int m = 0; m < result[i].GetLength(2); m++)
                        {
                            result[i][j, k, m] = M1[i][j, k, m] + M2[i][j, k, m];
                        }
                    }
                }
            }
            return result;
        }//end function
        /****************************************************************************
 * ****************************************************************************/
        internal static double[, , ,] Add(double[, , ,] M1, double[, , ,] M2)
        {
            int n1Dim1 = M1.GetLength(0);
            int n2Dim1 = M1.GetLength(1);
            int n3Dim1 = M1.GetLength(2);
            int n4Dim1 = M1.GetLength(3);
            int n1Dim2 = M2.GetLength(0);
            int n2Dim2 = M2.GetLength(1);
            int n3Dim2 = M2.GetLength(2);
            int n4Dim2 = M2.GetLength(3);

            if (n1Dim1 != n1Dim2 || n2Dim1 != n2Dim2 || n3Dim1 != n3Dim2 || n4Dim1 != n4Dim2)
                throw new Exception("Rows and Columns not similar");

            double[, , ,] result = new double[n1Dim1, n2Dim1, n3Dim1, n4Dim1];

            for (int i = 0; i < n1Dim1; i++)
            {
                for (int j = 0; j < n2Dim1; j++)
                {
                    for (int k = 0; k < n3Dim1; k++)
                    {
                        for (int m = 0; m < n4Dim1; m++)
                        {
                            result[i, j, k, m] = M1[i, j, k, m] + M2[i, j, k, m];
                        }
                    }
                }
            }
            return result;
        }//end function
        /****************************************************************************
* ****************************************************************************/
        internal static double[, , , ,] Add(double[, , , ,] M1, double[, , , ,] M2)
        {
            int n1Dim1 = M1.GetLength(0);
            int n2Dim1 = M1.GetLength(1);
            int n3Dim1 = M1.GetLength(2);
            int n4Dim1 = M1.GetLength(3);
            int n5Dim1 = M1.GetLength(4);
            int n1Dim2 = M2.GetLength(0);
            int n2Dim2 = M2.GetLength(1);
            int n3Dim2 = M2.GetLength(2);
            int n4Dim2 = M2.GetLength(3);
            int n5Dim2 = M2.GetLength(4);

            if (n1Dim1 != n1Dim2 || n2Dim1 != n2Dim2 || n3Dim1 != n3Dim2 || n4Dim1 != n4Dim2 || n5Dim1 != n5Dim2)
                throw new Exception("Rows and Columns not similar");

            double[, , , ,] result = new double[n1Dim1, n2Dim1, n3Dim1, n4Dim1, n5Dim2];

            for (int i = 0; i < n1Dim1; i++)
            {
                for (int j = 0; j < n2Dim1; j++)
                {
                    for (int k = 0; k < n3Dim1; k++)
                    {
                        for (int m = 0; m < n4Dim1; m++)
                        {
                            for (int n = 0; n < n5Dim2; n++)
                            {
                                result[i, j, k, m, n] = M1[i, j, k, m, n] + M2[i, j, k, m, n];
                            }
                        }
                    }
                }
            }
            return result;
        }//end function

        /******************************************************************************
         * ***********************************************************************/
        internal static double[][] MatrixDecompose(double[][] matrix,
  out int[] perm, out int toggle)
        {
            // Doolittle LUP decomposition.
            // assumes matrix is square.
            int n = matrix.Length; // convenience
            double[][] result = MatrixDuplicate(matrix);
            perm = new int[n];
            for (int i = 0; i < n; ++i) { perm[i] = i; }
            toggle = 1;
            for (int j = 0; j < n - 1; ++j) // each column
            {
                double colMax = Math.Abs(result[j][j]); // largest val in col j
                int pRow = j;
                for (int i = j + 1; i < n; ++i)
                {
                    if (result[i][j] > colMax)
                    {
                        colMax = result[i][j];
                        pRow = i;
                    }
                }
                if (pRow != j) // swap rows
                {
                    double[] rowPtr = result[pRow];
                    result[pRow] = result[j];
                    result[j] = rowPtr;
                    int tmp = perm[pRow]; // and swap perm info
                    perm[pRow] = perm[j];
                    perm[j] = tmp;
                    toggle = -toggle; // row-swap toggle
                }
                if (Math.Abs(result[j][j]) < 1.0E-20)
                    return null; // consider a throw
                for (int i = j + 1; i < n; ++i)
                {
                    result[i][j] /= result[j][j];
                    for (int k = j + 1; k < n; ++k)
                        result[i][k] -= result[i][j] * result[j][k];
                }
            } // main j column loop
            return result;
        }




        internal static double[][] MatrixDuplicate(double[][] matrix)
        {
            // assumes matrix is not null.
            double[][] result = MatrixCreate(matrix.Length, matrix[0].Length);
            for (int i = 0; i < matrix.Length; ++i) // copy the values
                for (int j = 0; j < matrix[i].Length; ++j)
                    result[i][j] = matrix[i][j];
            return result;
        }




        internal double[][] MatrixInverse(double[][] matrix)
        {
            int n = matrix.Length;
            double[][] result = MatrixDuplicate(matrix);
            int[] perm;
            int toggle;
            double[][] lum = MatrixDecompose(matrix, out perm, out toggle);
            if (lum == null)
                throw new Exception("Unable to compute inverse");
            double[] b = new double[n];
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (i == perm[j])
                        b[j] = 1.0;
                    else
                        b[j] = 0.0;
                }
                double[] x = HelperSolve(lum, b);
                for (int j = 0; j < n; ++j)
                    result[j][i] = x[j];
            }
            return result;
        }



        internal static double[] HelperSolve(double[][] luMatrix,
                        double[] b)
        {
            // solve luMatrix * x = b
            int n = luMatrix.Length;
            double[] x = new double[n];
            b.CopyTo(x, 0);
            for (int i = 1; i < n; ++i)
            {
                double sum = x[i];
                for (int j = 0; j < i; ++j)
                    sum -= luMatrix[i][j] * x[j];
                x[i] = sum;
            }
            x[n - 1] /= luMatrix[n - 1][n - 1];
            for (int i = n - 2; i >= 0; --i)
            {
                double sum = x[i];
                for (int j = i + 1; j < n; ++j)
                    sum -= luMatrix[i][j] * x[j];
                x[i] = sum / luMatrix[i][i];
            }
            return x;
        }
        // ---------------------------------------------------------------------------------------------------
        // Static functions below are for matrix operations on double arrays
        // TZ - Jan 2013
        static public double[] Mult(double[,] A, double[] B)
        {
            // matrix multiplication for 2D into 1D matrix
            int ARow = A.GetLength(0);
            int ACol = A.GetLength(1);
            double[,] B2 = new double[ACol, 1];
            for (int i = 0; i < B2.Length; i++)
            {
                B2[i, 0] = B[i];
            }
            double[,] C2 = Mult(A, B2);
            double[] C = new double[ARow];
            for (int i = 0; i < ARow; i++)
            {
                C[i] = C2[i, 0];
            }
            return C;
        }
        static public double[,] Mult(double A, double[,] B)
        {
            // matrix multiplication
            double[,] C = new double[B.GetLength(0), B.GetLength(1)];
            for (int i = 0; i < B.GetLength(0); i++)
            {
                for (int j = 0; j < B.GetLength(1); j++)
                {
                    C[i, j] = B[i, j] * A;
                }
            }
            return C;
        } // End of Function
        static public double[, ,] Mult(double A, double[, ,] B)
        {
            // matrix multiplication
            double[, ,] C = new double[B.GetLength(0), B.GetLength(1), B.GetLength(2)];
            for (int i = 0; i < B.GetLength(0); i++)
            {
                for (int j = 0; j < B.GetLength(1); j++)
                {
                    for (int k = 0; k < B.GetLength(2); k++)
                    {
                        C[i, j, k] = B[i, j, k] * A;
                    }
                }
            }
            return C;
        } // End of Function
        static public double[][,] Mult(double A, double[][,] B)
        {
            // matrix multiplication
            double[][,] C = new double[B.GetLength(0)][,];
            for (int i = 0; i < B.GetLength(0); i++)
            {
                C[i] = new double[B[i].GetLength(0), B[i].GetLength(1)];
                for (int j = 0; j < B[i].GetLength(0); j++)
                {
                    for (int k = 0; k < B[i].GetLength(1); k++)
                    {
                        C[i][j, k] = B[i][j, k] * A;
                    }
                }
            }
            return C;
        } // End of Function
        static public double[][,,] Mult(double A, double[][,,] B)
        {
            // matrix multiplication
            double[][,,] C = new double[B.GetLength(0)][,,];
            for (int i = 0; i < B.GetLength(0); i++)
            {
                C[i] = new double[B[i].GetLength(0), B[i].GetLength(1), B[i].GetLength(2)];
                for (int j = 0; j < B[i].GetLength(0); j++)
                {
                    for (int k = 0; k < B[i].GetLength(1); k++)
                    {
                        for (int m = 0; m < B[i].GetLength(1); m++)
                        {
                            C[i][j, k, m] = B[i][j, k, m] * A;
                        }
                    }
                }
            }
            return C;
        } // End of Function

        static public double[] Mult(double A, double[] B)
        {
            // matrix multiplication
            double[] C = new double[B.GetLength(0)];
            for (int i = 0; i < B.GetLength(0); i++)
            {
                C[i] = B[i] * A;
            }
            return C;
        } // End of Function
        static public double[,] Mult(double[,] A, double[,] B)
        {
            // matrix multiplication
            int ARow = A.GetLength(0);
            int ACol = A.GetLength(1);
            int BCol = B.GetLength(1);
            double[,] C = new double[ARow, BCol];
            for (int i = 0; i < ARow; i++)
            {
                for (int j = 0; j < BCol; j++)
                {
                    C[i, j] = 0;
                    for (int k = 0; k < ACol; k++)
                    {
                        C[i, j] += A[i, k] * B[k, j];
                    }
                }
            }
            return C;
        }
        /// <summary>
        /// Cross multiply two 3D vectors
        /// </summary>
        /// <param name="A">vector of 3</param>
        /// <param name="B">vector of 3</param>
        /// <returns>vector of 3 as cross product</returns>
        static public double[] CrossProduct(double[] A, double[] B)
        {
            // vector cross product
            int ARow = A.GetLength(0);
            int BRow = B.GetLength(0);
            if (ARow != 3 || BRow != 3) throw new Exception("Cross product only valid on 3d vectors.");
            double[] C = new double[3];
            C[0] = A[1] * B[2] - A[2] * B[1];
            C[1] = A[0] * B[2] - A[2] * B[0];
            C[2] = A[0] * B[1] - A[1] * B[0];
            return C;
        } // End of Function
        static public double[,] B1TAB2(double[,] B1, double[,] A, double[,] B2)
        {
            // Rreturn B1T*(A*B2): B1 is BRowxBcol, A is BRowxBRow, B2 is BRowxBCol, C is BColxBCol
            int BCol = B1.GetLength(1);
            double[,] C = new double[BCol, BCol];
            double[,] B1T;
            B1T = Transpose(B1);
            double[,] AB2;
            AB2 = Mult(A, B2);
            C = Mult(B1T, AB2);
            return C;
        } // End of Function
        static public void Merge(double[,] A, int[] map, double[,] B)
        {
            int size = A.GetLength(0);
            // Merges matrix A into B according to the map vector, A is square matrix
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    B[map[i], map[j]] += A[i, j];
                }
            }
        } // End of Function

        static public void Init(double[,] A, double diagval)
        {
            // Initialize the matrix 
            int Rows = A.GetLength(0);
            int Columns = A.GetLength(1);
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    A[i, j] = 0;
                    if (i == j)
                    {
                        A[i, j] = diagval;
                    }
                }
            }
        } // End of Function
        static public double[,] Identity(int Size)
        {
            // Initialize the matrix  SizexSize and return it
            double[,] A = new double[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    A[i, j] = 0;
                }
                A[i, i] = 1.0;
            }
            return A;
        } // End of Function
        /********************************************************************
         * ******************************************************************/

       
        /********************************************************************
 * ******************************************************************/

        static public double[,] Duplicate(double[,] A)
        {
            if (A == null) return null;
            int Rows = A.GetLength(0);
            int Cols = A.GetLength(1);
            double[,] B = new double[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    B[i, j] = A[i, j];
                }
            }
            return B;
        } //End of Function
        /********************************************************************************
                 * *******************************************************************************/
        static public double[, ,] Duplicate(double[, ,] A)
        {
            int Dim0 = A.GetLength(0);
            int Dim1 = A.GetLength(1);
            int Dim2 = A.GetLength(2);
            double[, ,] B = new double[Dim0, Dim1, Dim2];
            for (int i = 0; i < Dim0; i++)
            {
                for (int j = 0; j < Dim1; j++)
                {
                    for (int k = 0; k < Dim2; k++)
                    {
                        B[i, j, k] = A[i, j, k];
                    }
                }
            }
            return B;
        } //End of Function
        /***************************************************************
         * ************************************************************/
        static public string[][,] Duplicate(string[][,] A)
        {
            string[][,] B = new string[0][,];
            try
            {
                int Dim0 = A.GetLength(0);
                int Dim1 = A[0].GetLength(0);
                int Dim2 = A[0].GetLength(1);
                B = new string[Dim0][,];
                for (int i = 0; i < Dim0; i++) B[i] = Duplicate(A[i]);
            }
            catch { B = A; }//if it doesn't work, A.Length = 0
            return B;
        } //End of Function
        /***************************************************************
 * ************************************************************/
        static public double[][,,] Duplicate(double[][,,] A)
        {
            double[][,,] B = new double[0][,,];
            try
            {
                int Dim0 = A.GetLength(0);
                int Dim1 = A[0].GetLength(0);
                int Dim2 = A[0].GetLength(1);
                int Dim3 = A[0].GetLength(2);
                B = new double[Dim0][,,];
                for (int i = 0; i < Dim0; i++) { B[i] = new double[Dim1, Dim2, Dim3]; }

                for (int i = 0; i < Dim0; i++)
                {
                    for (int j = 0; j < Dim1; j++)
                    {
                        for (int k = 0; k < Dim2; k++)
                        {
                            for (int m = 0; m < Dim3; m++)
                            {
                                B[i][j, k, m] = A[i][j, k, m];
                            }
                        }
                    }
                }
            }
            catch { B = A; }//if it doesn't work, A.Length = 0
            return B;
        } //End of Function
        /***************************************************************
         * ************************************************************/
        static public double[][,] Duplicate(double[][,] A)
        {
            double[][,] B = new double[0][,];
            try
            {
                int Dim0 = A.GetLength(0);
                int Dim1 = A.GetLength(1);
                int Dim2 = A.GetLength(2);
                B = new double[Dim0][,];
                for (int i = 0; i < Dim0; i++) { B[i] = new double[Dim1, Dim2]; }

                for (int i = 0; i < Dim0; i++)
                {
                    for (int j = 0; j < Dim1; j++)
                    {
                        for (int k = 0; k < Dim2; k++)
                        {
                            B[i][j, k] = A[i][j, k];
                        }
                    }
                }
            }
            catch { B = A; }//if it doesn't work, A.Length = 0
            return B;
        } //End of Function
        /***************************************************************
         * ************************************************************/
      

        /***************************************************************
         * ************************************************************/
    

        static public double[, , ,] Duplicate(double[, , ,] A)
        {
            int Dim0 = A.GetLength(0);
            int Dim1 = A.GetLength(1);
            int Dim2 = A.GetLength(2);
            int Dim3 = A.GetLength(3);
            double[, , ,] B = new double[Dim0, Dim1, Dim2, Dim3];
            for (int i = 0; i < Dim0; i++)
            {
                for (int j = 0; j < Dim1; j++)
                {
                    for (int k = 0; k < Dim2; k++)
                    {
                        for (int l = 0; l < Dim3; l++)
                        {
                            B[i, j, k, l] = A[i, j, k, l];
                        }
                    }
                }
            }
            return B;
        } //End of Function

        static public double[][][ , ,] Duplicate(double[][][ , ,] A)
        {
            int Dim0 = A.GetLength(0);
            double[][][, ,] B = new double[Dim0][][,,];
            for (int i = 0; i < Dim0; i++)
            {
                int Dim1 = A[i].GetLength(0);
                B[i] = new double[Dim1][, ,];
                for (int j = 0; j < Dim1; j++)
                {
                    int Dim2 = A[i][j].GetLength(0);
                    int Dim3 = A[i][j].GetLength(1);
                    int Dim4 = A[i][j].GetLength(2);
                    B[i][j] = new double[Dim2, Dim3, Dim4];
                    for (int k = 0; k < Dim2; k++)
                    {
                        for (int l = 0; l < Dim3; l++)
                        {
                            for (int m = 0; m < Dim4; m++)
                            {
                                B[i][j][k, l, m] = A[i][ j][ k, l, m];
                            }
                        }
                    }
                }
            }
            return B;
        } //End of Function
        static public double[, , , ,] Duplicate(double[, , , ,] A)
        {
            int Dim0 = A.GetLength(0);
            int Dim1 = A.GetLength(1);
            int Dim2 = A.GetLength(2);
            int Dim3 = A.GetLength(3);
            int Dim4 = A.GetLength(4);
            double[, , , ,] B = new double[Dim0, Dim1, Dim2, Dim3, Dim4];
            for (int i = 0; i < Dim0; i++)
            {
                for (int j = 0; j < Dim1; j++)
                {
                    for (int k = 0; k < Dim2; k++)
                    {
                        for (int l = 0; l < Dim3; l++)
                        {
                            for (int m = 0; m < Dim4; m++)
                            {
                                B[i, j, k, l, m] = A[i, j, k, l, m];
                            }
                        }
                    }
                }
            }
            return B;
        } //End of Function
        static public string[,] Duplicate(string[,] A)
        {
            int Rows = A.GetLength(0);
            int Cols = A.GetLength(1);
            string[,] B = new string[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    B[i, j] = A[i, j];
                }
            }
            return B;
        } //End of Function
        static public string[, ,] Duplicate(string[, ,] A)
        {
            int Dim0 = A.GetLength(0);
            int Dim1 = A.GetLength(1);
            int Dim2 = A.GetLength(2);
            string[, ,] B = new string[Dim0, Dim1, Dim2];
            for (int i = 0; i < Dim0; i++)
            {
                for (int j = 0; j < Dim1; j++)
                {
                    for (int k = 0; k < Dim2; k++)
                    {
                        B[i, j, k] = A[i, j, k];
                    }
                }
            }
            return B;
        } //End of Function
        static public string[, , ,] Duplicate(string[, , ,] A)
        {
            int Dim0 = A.GetLength(0);
            int Dim1 = A.GetLength(1);
            int Dim2 = A.GetLength(2);
            int Dim3 = A.GetLength(3);
            string[, , ,] B = new string[Dim0, Dim1, Dim2, Dim3];
            for (int i = 0; i < Dim0; i++)
            {
                for (int j = 0; j < Dim1; j++)
                {
                    for (int k = 0; k < Dim2; k++)
                    {
                        for (int l = 0; l < Dim3; l++)
                        {
                            B[i, j, k, l] = A[i, j, k, l];
                        }
                    }
                }
            }
            return B;
        } //End of Function

        static public string[, , , ,] Duplicate(string[, , , ,] A)
        {
            int Dim0 = A.GetLength(0);
            int Dim1 = A.GetLength(1);
            int Dim2 = A.GetLength(2);
            int Dim3 = A.GetLength(3);
            int Dim4 = A.GetLength(4);
            string[, , , ,] B = new string[Dim0, Dim1, Dim2, Dim3, Dim4];
            for (int i = 0; i < Dim0; i++)
            {
                for (int j = 0; j < Dim1; j++)
                {
                    for (int k = 0; k < Dim2; k++)
                    {
                        for (int l = 0; l < Dim3; l++)
                        {
                            for (int m = 0; m < Dim4; m++)
                            {
                                B[i, j, k, l, m] = A[i, j, k, l, m];
                            }
                        }
                    }
                }
            }
            return B;
        } //End of Function

        static public int[,] Duplicate(int[,] A)
        {
            int Rows = A.GetLength(0);
            int Cols = A.GetLength(1);
            int[,] B = new int[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    B[i, j] = A[i, j];
                }
            }
            return B;
        } //End of Function        

        static public int[] Duplicate(int[] A)
        {
            if (A == null) return null;
            int Rows = A.GetLength(0);
            int[] B = new int[Rows];
            for (int i = 0; i < Rows; i++)
            {
                B[i] = A[i];
            }
            return B;
        } //End of Function

        static public double[] Duplicate(double[] A)
        {
            if (A == null) return null;
            int Rows = A.GetLength(0);
            double[] B = new double[Rows];
            for (int i = 0; i < Rows; i++)
            {
                B[i] = A[i];
            }
            return B;
        } //End of Function

        static public string[] Duplicate(string[] A)
        {
            if (A == null) return null;
            int Rows = A.GetLength(0);
            string[] B = new string[Rows];
            for (int i = 0; i < Rows; i++)
            {
                B[i] = A[i];
            }
            return B;
        } //End of Function

        static public TreeNode Duplicate(TreeNode A)
        {
            TreeNode[] TNs = new TreeNode[A.Nodes.Count];
            for (int tns = 0; tns < A.Nodes.Count; tns++) TNs[tns] = (TreeNode)A.Nodes[tns].Clone();
            TreeNode B = new TreeNode(A.Text, TNs);
            return B;
        } //End of Function








        

        /**********************************************************************
        ******************************************************************/
        static public string[,] Convert3Dto2D(string[][,] Array)
        {
            string[,] NewArray = new string[0, 0];
            for (int i = 0; i < Array.Length; i++)
            {
                //add columns to new array
                addColumnsToArray2D(ref NewArray, Array[i], NewArray.GetLength(1));
            }
            return NewArray;
        }//end function




        /**********************************************************************
        ******************************************************************/
        static public void DeleteRow(ref string[,] A, int loc)
        {
            string[,] B = new string[A.GetLength(0) - 1, A.GetLength(1)];

            //populate array up to loc
            for (int r = 0; r < loc; r++)
            {
                for (int c = 0; c < A.GetLength(1); c++)
                {
                    B[r, c] = A[r, c];
                }
            }
            //populate array after loc
            for (int r = loc; r < B.GetLength(0); r++)
            {
                for (int c = 0; c < B.GetLength(1); c++)
                {
                    B[r, c] = A[r + 1, c];
                }
            }
            A = B;
        }//end function

        /**********************************************************************
        ******************************************************************/
        static public void DeleteRow(ref double[,] A, int loc)
        {
            double[,] B = new double[A.GetLength(0) - 1, A.GetLength(1)];

            //populate array up to loc
            for (int r = 0; r < loc; r++)
            {
                for (int c = 0; c < A.GetLength(1); c++)
                {
                    B[r, c] = A[r, c];
                }
            }
            //populate array after loc
            for (int r = loc; r < B.GetLength(0); r++)
            {
                for (int c = 0; c < B.GetLength(1); c++)
                {
                    B[r, c] = A[r + 1, c];
                }
            }
            A = B;
        }//end function
        /**********************************************************************
        ******************************************************************/
        static public void DeleteRow(ref double[] A, int loc)
        {
            double[] B = new double[A.GetLength(0) - 1];

            //populate array up to loc
            for (int r = 0; r < loc; r++)
            {
                B[r] = A[r];
            }
            //populate array after loc
            for (int r = loc; r < B.GetLength(0); r++)
            {
                B[r] = A[r + 1];
            }
            A = B;
        }//end function

        /**********************************************************************
        ******************************************************************/
        static public void DeleteRow(ref string[] A, int loc)
        {
            string[] B = new string[A.GetLength(0) - 1];

            //populate array up to loc
            for (int r = 0; r < loc; r++)
            {
                B[r] = A[r];
            }
            //populate array after loc
            for (int r = loc; r < B.GetLength(0); r++)
            {
                B[r] = A[r + 1];
            }
            A = B;
        }//end function
        /**********************************************************************
        ******************************************************************/
        static public string[,] Convert3Dto2D(string[, ,] arry)
        {
            string[][,] ary = Cnvt3DtoArrayofArrays(arry);
            string[,] NewArray = Convert3Dto2D(ary);
            return NewArray;
        }//end function
        /****************************************************************************
         * **************************************************************************/
        static public double[][,] CnvtAoAo2DAtoAo2DA(double[][][,] arry)
        {
            double[][,] NewAry = new double[0][,];
            for (int i = 0; i < arry.Length; i++)
            {
                double[][,] a = arry[i];
                if (a != null) addArray(a, ref NewAry, NewAry.Length);
            }
            return NewAry;
        }
        /****************************************************************************
         * **************************************************************************/
        static public string[][,] Cnvt3DtoArrayofArrays(string[, ,] arry)
        {
            string[][,] NewArray = new string[arry.GetLength(0)][,];
            for (int i = 0; i < arry.GetLength(0); i++)
            {
                NewArray[i] = new string[arry.GetLength(1), arry.GetLength(2)];
                for (int j = 0; j < arry.GetLength(1); j++)
                {
                    for (int k = 0; k < arry.GetLength(2); k++)
                    {
                        NewArray[i][j, k] = arry[i, j, k];
                    }
                }
            }
            return NewArray;
        }//end function
        /**********************************************************************
******************************************************************/
        static public string[,] Convert3Dto2D(double[][,] Arry)
        {
            string[,] NewArray = new string[0, 0];
            for (int i = 0; i < Arry.Length; i++)
            {
                string[,] ary = CnvtDoubleAryToStringAry(Arry[i], "");
                //add columns to new array
                addColumnsToArray2D(ref NewArray, ary, NewArray.GetLength(1));
            }
            return NewArray;
        }//end function
        /***************************************************************************************
         * ***********************************************************************************/
        static public string[, ,] CnvtDoubleAryToStringAry(double[, ,] A, string opformat)
        {
            string[, ,] B = new string[A.GetLength(0), A.GetLength(1), A.GetLength(2)];
            if (opformat == null) opformat = "";
            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    for (int k = 0; k < A.GetLength(2); k++)
                    {
                        B[i, j, k] = A[i, j, k].ToString(opformat);
                    }
                }
            }
            return B;
        }//end function
        /***************************************************************************************
         * ***********************************************************************************/
        static public string[][,] CnvtDoubleAryToStringAry(double[][,] A, string opformat)
        {
            string[][,] B = new string[A.GetLength(0)][,];
            if (opformat == null) opformat = "";
            for (int i = 0; i < A.GetLength(0); i++)
            {
                B[i] = new string[A[i].GetLength(0), A[i].GetLength(1)];
                for (int j = 0; j < A[i].GetLength(0); j++)
                {
                    for (int k = 0; k < A[i].GetLength(1); k++)
                    {
                        B[i][j, k] = A[i][j, k].ToString(opformat);
                    }
                }
            }
            return B;
        }//end function
        /***************************************************************************************
         * ***********************************************************************************/
        static public string[,] CnvtDoubleAryToStringAry(double[,] A, string opformat)
        {
            string[,] B = new string[A.GetLength(0), A.GetLength(1)];
            if (opformat == null) opformat = "";
            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    B[i, j] = A[i, j].ToString(opformat);
                }
            }
            return B;
        }//end function
        /***************************************************************************************
         * ***********************************************************************************/
        static public string[] CnvtDoubleAryToStringAry(double[] A, string opformat)
        {
            string[] B = new string[A.GetLength(0)];
            if (opformat == null) opformat = "";
            for (int i = 0; i < A.GetLength(0); i++)
            {
                B[i] = A[i].ToString(opformat);
            }
            return B;
        }//end function
        /***************************************************************************************
         * ***********************************************************************************/
        static public double[] CnvtStringAryToDoubleAry(string[] A)
        {
            double[] B = new double[A.GetLength(0)];
            for (int i = 0; i < A.GetLength(0); i++)
            {
                B[i] = CvtstTodbl(A[i]);
            }
            return B;
        }//end function
        /***************************************************************************************
 * ***********************************************************************************/
        static public double[,] CnvtStringAryToDoubleAry(string[,] A)
        {
            double[,] B = new double[A.GetLength(0), A.GetLength(1)];
            for (int i = 0; i < A.GetLength(0); i++)
            {
                for (int j = 0; j < A.GetLength(1); j++)
                {
                    B[i, j] = Double.Parse(A[i, j]);
                }
            }
            return B;
        }//end function
        /**********************************************************************
         * This function adds new columns to an array.  If there are more rows in the new array than in the old one,
         * there will be zeros (or null values) at the bottom of the old array.  If the opposite is the case, 
         * zeros (or null values) will exist in the unoccupied rows below these columns.
        ******************************************************************/
        static public void addColumnsToArray2D(ref string[,] A, string[,] toAdd, int colLoc)
        {
            string[,] Acopy = Duplicate(A);

            //get new dimensions
            int d1 = Math.Max(A.GetLength(0), toAdd.GetLength(0));
            int d2 = A.GetLength(1) + toAdd.GetLength(1);
            A = new string[d1, d2];

            //populate array with old values up to colLoc
            for (int rw = 0; rw < Acopy.GetLength(0); rw++)
            {
                for (int cl = 0; cl < colLoc; cl++)
                {
                    A[rw, cl] = Acopy[rw, cl];
                }
            }
            //place new columns in array
            for (int rw = 0; rw < toAdd.GetLength(0); rw++)
            {
                for (int cl = colLoc; cl < colLoc + toAdd.GetLength(1); cl++)
                {
                    A[rw, cl] = toAdd[rw, cl - colLoc];
                }
            }
            //add remaining old values from Acopy
            for (int rw = 0; rw < Acopy.GetLength(0); rw++)
            {
                for (int cl = colLoc + toAdd.GetLength(1); cl < d2; cl++)
                {
                    A[rw, cl] = Acopy[rw, cl - 1];
                }
            }
        }//end function        
        /*******************************************************************************************
 * *****************************************************************************************/
        public static void addDoubleToCol(double d, ref double[,] Array, int locInArray, int row)
        {
            //this function adds a column into a 2D Array at a given column location within the array
            //first check for trailing zeroes
            int L = 0;
            for (int i = Array.GetLength(1) - 1; i > -1; i--)
            {
                if (Array[row, i] != 0)
                { L = i + 1; break; }
            }
            if (L < Array.GetLength(1))//if we have trailing zeroes, we dont need to add a dimension to the array
            {
                double[] Ary1D = new double[L + 1];
                for (int i = 0; i < locInArray; i++) Ary1D[i] = Array[row, i];//add data up to locInArray
                Ary1D[locInArray] = d;//add new value
                for (int i = locInArray; i < L; i++) Ary1D[i + 1] = Array[row, i];//add data after locinarray
                for (int i = 0; i < L + 1; i++) Array[row, i] = Ary1D[i];
            }
            else
            {
                //copy array so you can resize it
                double[,] ArrayCopy = MatrixOperations.Duplicate(Array);

                //make new array
                Array = new double[ArrayCopy.GetLength(0), ArrayCopy.GetLength(1) + 1];
                //make 1D array to add a column to
                double[] Array1D = new double[ArrayCopy.GetLength(1) + 1];

                //add everything that used to be in the array up to the locInArray
                for (int i = 0; i < locInArray; i++) { Array1D[i] = ArrayCopy[row, i]; }
                //add d to the loc in array
                Array1D[locInArray] = d;
                //add everything that used to be in the array after locInArray
                for (int i = locInArray; i < ArrayCopy.GetLength(1); i++) { Array1D[i + 1] = ArrayCopy[row, i]; }

                //add everything back into the array
                for (int i = 0; i < ArrayCopy.GetLength(0); i++)
                {
                    for (int j = 0; j < ArrayCopy.GetLength(1); j++)
                    {
                        if (i != row) Array[i, j] = ArrayCopy[i, j];
                    }
                }
                //add row that was added to
                for (int i = 0; i < Array1D.Length; i++)
                {
                    Array[row, i] = Array1D[i];
                }
            }
        }//end function
        /*******************************************************************************************
 * *****************************************************************************************/
        public static void addDoubleToLastDim(double d, ref double[, ,] Array, int locInArray, int d1, int d2)
        {
            //this function adds an entry in the 3rd dimension into a 3D Array at a 1st and 2nd dimension within the array
            //first check for trailing zeroes
            int L = 0;
            for (int i = Array.GetLength(2) - 1; i > -1; i--)
            {
                if (Array[d1, d2, i] != 0)
                { L = i + 1; break; }
            }
            if (L < Array.GetLength(2))//if we have trailing zeroes, we dont need to add a dimension to the array
            {
                double[] Ary1D = new double[L + 1];
                for (int i = 0; i < locInArray; i++) Ary1D[i] = Array[d1, d2, i];//add data up to locInArray
                Ary1D[locInArray] = d;//add new value
                for (int i = locInArray; i < L; i++) Ary1D[i + 1] = Array[d1, d2, i];//add data after locinarray
                for (int i = 0; i < L + 1; i++) Array[d1, d2, i] = Ary1D[i];
            }
            else
            {
                //copy array so you can resize it
                double[, ,] ArrayCopy = MatrixOperations.Duplicate(Array);

                //make new array
                Array = new double[ArrayCopy.GetLength(0), ArrayCopy.GetLength(1), ArrayCopy.GetLength(2) + 1];
                //make 1D array to add a column to
                double[] Array1D = new double[ArrayCopy.GetLength(2) + 1];

                //add everything that used to be in the array up to the locInArray
                for (int i = 0; i < locInArray; i++) { Array1D[i] = ArrayCopy[d1, d2, i]; }
                //add d to the loc in array
                Array1D[locInArray] = d;
                //add everything that used to be in the array after locInArray
                for (int i = locInArray; i < ArrayCopy.GetLength(2); i++) { Array1D[i + 1] = ArrayCopy[d1, d2, i]; }

                //add everything back into the array
                for (int i = 0; i < ArrayCopy.GetLength(0); i++)
                {
                    for (int j = 0; j < ArrayCopy.GetLength(1); j++)
                    {
                        for (int k = 0; k < ArrayCopy.GetLength(2); k++)
                        {
                            if (i == d1 && j == d2) { }
                            else Array[i, j, k] = ArrayCopy[i, j, k];
                        }
                    }
                }

                //add row that was added to
                for (int i = 0; i < Array1D.Length; i++)
                {
                    Array[d1, d2, i] = Array1D[i];
                }
            }
        }//end function

        /**********************************************************************
        ******************************************************************/
        static public void addColumnsToArray2D(ref double[,] A, double[,] toAdd, int colLoc)
        {
            double[,] Acopy = Duplicate(A);

            //get new dimensions
            int d1 = Math.Max(A.GetLength(0), toAdd.GetLength(0));
            int d2 = A.GetLength(1) + toAdd.GetLength(1);
            A = new double[d1, d2];

            //populate array with old values up to colLoc
            for (int rw = 0; rw < Acopy.GetLength(0); rw++)
            {
                for (int cl = 0; cl < colLoc; cl++)
                {
                    A[rw, cl] = Acopy[rw, cl];
                }
            }
            //place new columns in array
            for (int rw = 0; rw < toAdd.GetLength(0); rw++)
            {
                for (int cl = colLoc; cl < colLoc + toAdd.GetLength(1); cl++)
                {
                    A[rw, cl] = toAdd[rw, cl - colLoc];
                }
            }
            //add remaining old values from Acopy
            for (int rw = 0; rw < Acopy.GetLength(0); rw++)
            {
                for (int cl = colLoc + toAdd.GetLength(1); cl < d2; cl++)
                {
                    A[rw, cl] = Acopy[rw, cl - 1];
                }
            }
        }//end function


       



        /**********************************************************************
        ******************************************************************/
        static public double[,] Inverse(double[,] AA)
        {
            int ARow = AA.GetLength(0);
            double[,] A; double[,] AInv;
            A = Duplicate(AA);
            AInv = Identity(ARow);
            // return the inverse of a matrix
            for (int i = 0; i < ARow; i++) // Matrix reduction
            {
                double ri = A[i, i];
                for (int k = 0; k < ARow; k++)
                {
                    A[i, k] = A[i, k] / ri;
                    AInv[i, k] = AInv[i, k] / ri;
                }
                for (int j = i + 1; j < ARow; j++)
                {
                    double r = -A[j, i];
                    for (int k = 0; k < ARow; k++)
                    {
                        A[j, k] += r * A[i, k];
                        AInv[j, k] += r * AInv[i, k];
                    }
                }
            }
            for (int i = ARow - 1; i > 0; i--) // Matrix back substituion
            {
                for (int j = i - 1; j >= 0; j--)
                {
                    double r = -A[j, i];
                    for (int k = 0; k < ARow; k++)
                    {
                        A[j, k] += r * A[i, k];
                        AInv[j, k] += r * AInv[i, k];
                    }
                }
            }
            return AInv;
        } // End of Function
        //------------------------------------------------------------------------------------------------------------
        static public double[,] BanMult(double[,] AA, double[,] X)
        {
            // Multiplication of banded stiffness and displacements
            int N = AA.GetLength(0);  // Size of the matrix
            int NB = AA.GetLength(1); // Columns of A, or bandwidth
            int NLC = X.GetLength(1); // Number of solution cases
            double[,] F = new double[N, NLC];
            int jb = 0; int ib = 0;
            for (int i = 0; i < N; i++)
            {
                for (int k = 0; k < NLC; k++)    //each load case
                {
                    F[i, k] = 0;
                    for (int j = 0; j < N; j++)
                    {
                        jb = j - i;
                        ib = i - j;
                        if (jb < 0 && jb > -NB)
                        {
                            F[i, k] += AA[j, ib] * X[j, k];
                        }
                        else if (jb >= 0 && jb < NB)
                        {
                            F[i, k] += AA[i, jb] * X[j, k];
                        }
                    }
                }
            }
            return F;
        } // End of Function
        //------------------------------------------------------------------------------------------------------------
        static public double[,] BanSolve(double[,] AA, double[,] RHS)
        {
            // Simultaneous linear equation solution of a symmetric matrix stored in upper traingle banded form, 
            // with correction for 0 equations (0 row and column)
            int N = AA.GetLength(0);  // Size of the matrix
            int NB = AA.GetLength(1); // Columns of A, or bandwidth
            int NLC = RHS.GetLength(1); // Number of solution cases
            int NB2 = 2 * NB - 1;
            int NB1 = NB - 1;
            double[,] KB; double[,] X;
            KB = new double[N, NB2];   // Note: In banded form k[i,j] is stored in k[i,jofi], jofi = j-i+NB1
            X = Duplicate(RHS);
            for (int i = 0; i < N; i++) // create the full banded from the symmetric banded matrix
            {
                for (int j = 0; j < NB; j++)
                {
                    int jofi = j + NB1;
                    KB[i, jofi] = AA[i, j];
                }
                for (int j = i - NB1; j < i; j++)
                {
                    if (j >= 0)
                    {
                        int jofi = j - i + NB1;
                        int iofjs = i - j;
                        int iofj = i - j + NB1;
                        KB[i, jofi] = KB[j, iofj];
                    }
                }
            } // KB is the full banded matrix
            for (int i = 0; i < N; i++) // Matrix reduction
            {
                double KBii = KB[i, NB1];   // kii in banded matrix
                if (KBii != 0)
                {
                    for (int k = i - NB1; k < i + NB; k++)  // Divide Row i by kii, results in kii=1
                    {
                        int kofi = k - i + NB1;
                        KB[i, kofi] = KB[i, kofi] / KBii;
                    }
                    for (int k = 0; k < NLC; k++)
                    {
                        X[i, k] = X[i, k] / KBii;
                    }                                     // Done: Divide Row i by kii, results in kii=1
                    for (int j = i + 1; j < i + NB; j++)     // Row j = Row j - kji * Row i
                    {
                        int iofj = i - j + NB1;
                        if (j < N)
                        {
                            double r = -KB[j, iofj];
                            if (r != 0)
                            {
                                for (int k = i; k < i + NB; k++)
                                {
                                    int kofj = k - j + NB1;
                                    int kofi = k - i + NB1;
                                    KB[j, kofj] += r * KB[i, kofi];
                                }
                                for (int k = 0; k < NLC; k++)
                                {
                                    X[j, k] += r * X[i, k];
                                }
                            }
                        }
                    }
                }
            }
            for (int i = N - 2; i >= 0; i--)   // Back Substituion Xi = Fi - Kij*Xj For j = i+1 to N
            {
                int jmax = (i + NB); if (jmax > N) { jmax = N; }
                for (int j = i + 1; j < jmax; j++)
                {
                    int jofi = j - i + NB1;
                    for (int k = 0; k < NLC; k++)
                    {
                        X[i, k] -= KB[i, jofi] * X[j, k];
                    }
                }
            }
            return X;
        } // End of Function

        static public double[,] Solver(double[,] AA, double[,] RHS)
        {
            int ARow = AA.GetLength(0);
            int NLC = RHS.GetLength(1);
            double[,] A; double[,] XX;
            A = Duplicate(AA);
            XX = Duplicate(RHS);
            // return the solution to AA*XX=RHS of a matrix
            for (int i = 0; i < ARow; i++) // Matrix reduction
            {
                double ri = A[i, i];
                if (ri != 0)
                {
                    for (int k = 0; k < ARow; k++)
                    {
                        A[i, k] = A[i, k] / ri;
                    }
                    for (int k = 0; k < NLC; k++)
                    {
                        XX[i, k] = XX[i, k] / ri;
                    }
                    for (int j = i + 1; j < ARow; j++)
                    {
                        double r = -A[j, i];
                        if (r != 0)
                        {
                            for (int k = 0; k < ARow; k++)
                            {
                                A[j, k] += r * A[i, k];
                            }
                            for (int k = 0; k < NLC; k++)
                            {
                                XX[j, k] += r * XX[i, k];
                            }
                        }
                    }
                }
            }
            for (int i = ARow - 1; i > 0; i--) // Matrix back substituion
            {
                for (int j = i - 1; j >= 0; j--)
                {
                    double r = -A[j, i];
                    if (r != 0)
                    {
                        for (int k = 0; k < ARow; k++)
                        {
                            A[j, k] += r * A[i, k];
                        }
                        for (int k = 0; k < NLC; k++)
                        {
                            XX[j, k] += r * XX[i, k];
                        }
                    }
                }
            }
            return XX;
        } // End of Function
        /// <summary>
        /// Returns Y value from a multi-linear curve
        /// </summary>
        /// <param name="AA">[i,2] X,Y pairs</param>
        /// <param name="X"> given X to get Y</param>
        /// <param name="OutofRangeFlag"> greater than 0: return last value, =0: return 0, less than 0: extrapolate </param>
        /// <returns></returns>
        static public double[] YofCurve(double[,] AA, double X, int OutofRangeFlag)
        {
            // AA is [Arow,ACol]. Values in column 0 are expected to be in ascending order. 
            // Interpolates values in Column 1 to find other columns based on given X
            // If X is out of range: Flag>0 -> returns the last value, Flag=0 -> returns 0, Flag<0 -> extrapolate
            int ARow = AA.GetLength(0);
            int ACol = AA.GetLength(1);

            double r = 0.0;
            double[] Y = new double[ACol - 1];
            if (X < AA[0, 0])
            {
                if (OutofRangeFlag > 0)
                {
                    for (int j = 1; j < ACol; j++)
                    {
                        Y[j - 1] = AA[0, j];
                    }
                }
                else if (OutofRangeFlag == 0)
                {
                    for (int j = 1; j < ACol; j++)
                    {
                        Y[j - 1] = 0;
                    }
                }
                else
                {
                    if ((AA[1, 0] - AA[0, 0]) == 0)
                    {
                        r = 0.5D;
                    }
                    else
                    {
                        r = (X - AA[0, 0]) / (AA[1, 0] - AA[0, 0]);
                    }
                    for (int j = 1; j < ACol; j++)
                    {
                        Y[j - 1] = AA[0, j] + r * (AA[1, j] - AA[0, j]);
                    }
                }
                return Y;
            }
            if (X > AA[ARow - 1, 0])
            {
                if (OutofRangeFlag > 0)
                {
                    for (int j = 1; j < ACol; j++)
                    {
                        Y[j - 1] = AA[ARow - 1, j];
                    }
                }
                else if (OutofRangeFlag == 0)
                {
                    for (int j = 1; j < ACol; j++)
                    {
                        Y[j - 1] = 0;
                    }
                }
                else
                {
                    if ((AA[ARow - 2, 0] - AA[ARow - 1, 0]) == 0)
                    {
                        r = 0.5D;
                    }
                    else
                    {
                        r = (X - AA[ARow - 2, 0]) / (AA[ARow - 1, 0] - AA[ARow - 2, 0]);
                    }
                    for (int j = 1; j < ACol; j++)
                    {
                        Y[j - 1] = AA[ARow - 2, j] + r * (AA[ARow - 1, j] - AA[ARow - 2, j]);
                    }
                }
                return Y;
            }
            for (int ir = 1; ir < ARow; ir++)
            {
                if (X <= AA[ir, 0])
                {
                    if ((AA[ir, 0] - AA[ir - 1, 0]) == 0)
                    {
                        r = 0.5D;
                    }
                    else
                    {
                        r = (X - AA[ir - 1, 0]) / (AA[ir, 0] - AA[ir - 1, 0]);
                    }
                    for (int j = 1; j < ACol; j++)
                    {
                        Y[j - 1] = AA[ir - 1, j] + r * (AA[ir, j] - AA[ir - 1, j]);
                    }
                    break;
                }
            }
            return Y;
        } // End of Function

        /// <summary>
        /// Returns the indeces of max or min peaks
        /// </summary>
        /// <param name="AA">[n,2] x,y values</param>
        /// <param name="minmax">+ve=max, -ve=min</param>
        /// <returns>indeces of peaks of curve</returns>
        static public int[] PeaksofCurve(double[,] AA, int minmax)
        {
            // AA is nPTx2
            // returns an array of the positive peak locations for minmax>0 & negative peak locations for minmax<0
            int nPt = AA.GetLength(0);
            if (nPt < 1) return null;
            double ILmax = 0; double Tolerance = 1.0E-6;
            for (int i = 0; i < nPt; i++)
            {
                if (minmax * AA[i, 1] > ILmax) ILmax = AA[i, 1];
            }
            Tolerance = Tolerance * ILmax;  // Tolerance is set to max(IL)/10^6
            if (Tolerance < 1.0E-12) Tolerance = 1.0E-12;  // Just in case all values are 0
            int dir = 0; // Ascending=1, decsending=-1
            // if (AA[1, 1] > AA[0, 1]) dir = 1; else dir = -1;
            double val = AA[1, 1];  // start with second point
            int[] tempPeak = new int[nPt];
            int nPeak = 0;
            for (int i = 1; i < nPt; i++)
            {
                if ((AA[i, 1] < (AA[i - 1, 1] - Tolerance)) && dir != -1)
                {
                    // decreasing starts or at last point: found a maximum
                    dir = -1;
                    if (minmax > 0)
                    {
                        // record the maximum
                        tempPeak[nPeak] = i - 1;
                        nPeak++;
                    }
                }
                else if ((AA[i, 1] > (AA[i - 1, 1] + Tolerance)) && dir != 1)
                {
                    // increasing after decrease: found a minimum
                    dir = 1;
                    if (minmax < 0)
                    {
                        // record the minimum
                        tempPeak[nPeak] = i - 1;
                        nPeak++;
                    }
                }
                if (i == nPt - 1)
                {
                    if (AA[i, 1] > (AA[i - 1, 1] + Tolerance))
                    {
                        if (minmax > 0)
                        {
                            // at the end of curve & ascending: save as maximum
                            tempPeak[nPeak] = i;
                            nPeak++;
                        }
                    }
                    if (AA[i, 1] < (AA[i - 1, 1] - Tolerance))
                    {
                        if (minmax < 0)
                        {
                            // at end and decreasing: save as minimum
                            tempPeak[nPeak] = i;
                            nPeak++;
                        }
                    }
                }
            }
            // If found no peaks, Add the last point
            if (nPeak == 0)
            {
                nPeak = 1;
                tempPeak[0] = nPt - 1;
            }
            int[] Peaks = new int[nPeak];
            for (int i = 0; i < nPeak; i++)
            { // Create an array of peaks
                Peaks[i] = tempPeak[i];
            }
            return Peaks;
        } // End of Function

        static public double[,] MaxofCurve(double[,] AA)
        {
            int n0 = AA.GetLength(0);
            int n1 = AA.GetLength(1);
            double[,] maxval = new double[2, n1 - 1];
            for (int i = 0; i < n1 - 1; i++) maxval[1, i] = -1.0e15;
            for (int i0 = 0; i0 < n0; i0++)
            {
                for (int i1 = 1; i1 < n1; i1++)
                {
                    if (AA[i0, i1] > maxval[1, i1 - 1]) { maxval[0, i1 - 1] = AA[i0, 0]; maxval[1, i1 - 1] = AA[i0, i1]; }
                }
            }
            return maxval;
        } // End of Function
        static public double[,] MaxofCurve(double[,] AA, double Start, double End)
        {
            int n0 = AA.GetLength(0);
            int n1 = AA.GetLength(1);
            if (End < AA[0, 0]) return null; // Range ends before the curve
            if (Start > AA[n0 - 1, 0]) return null;  // Range starts after end of curve
            double[,] maxval = new double[2, n1 - 1];
            // Set to StartVal
            if (Start > AA[0, 0])
            {
                double[] tmpValS = YofCurve(AA, Start, 0);
                for (int i = 0; i < n1 - 1; i++)
                {
                    maxval[0, i] = Start;
                    maxval[1, i] = tmpValS[i];
                }
            }
            for (int i = 0; i < n1 - 1; i++) maxval[1, i] = -1.0e15;
            for (int i0 = 0; i0 < n0; i0++)
            {
                if (AA[i0, 0] >= Start && AA[i0, 0] <= End)
                {
                    for (int i1 = 1; i1 < n1; i1++)
                    {
                        if (AA[i0, i1] > maxval[1, i1 - 1]) { maxval[0, i1 - 1] = AA[i0, 0]; maxval[1, i1 - 1] = AA[i0, i1]; }
                    }
                }
            }
            // Check the end point
            if (End < AA[n0 - 1, 0])
            {
                double[] tmpValE = YofCurve(AA, End, 0);
                for (int i = 0; i < n1 - 1; i++)
                {
                    if (maxval[1, i] < tmpValE[i])
                    {
                        maxval[0, i] = End;
                        maxval[1, i] = tmpValE[i];
                    }
                }
            }
            return maxval;
        }//End of Function

        static public double[] sortDoubleArray(double[] A)
        {
            //this function sorts from least to greatest leaving trailing zeroes at the end
            //check for trailing zeroes
            int aryLength = A.Length;
            for (int l = aryLength - 1; l > -1; l--)
            {
                //if ending values are zeroes, we leave them at the end by adjusting the array length
                if (A[l] != 0) { aryLength = l + 1; break; }
            }

            //trimmed array
            double[] B = new double[aryLength];
            for (int i = 0; i < aryLength; i++) B[i] = A[i];
            int[] SortIndex = SortID(B, 0);
            for (int i = 0; i < aryLength; i++)
            {
                A[i] = B[i];
            }
            return A;
        }//end function

        /**********************************************************************************
         * *****************************************************************************/
        /// <summary>
        /// This function sorts a node array from smallest to largest along a 1-dimensional line starting with LowNode
        /// </summary>
        /// <param name="A">Nodes to sort</param>
        /// <param name="LowNode">lowest/first node on line</param>
        /// <returns>sorted nodes array</returns>

        

        /*************************************************************************************************
         * ******************************************************************************************/
        static public void sortDoubleArrayLastDim(double[, ,] A)
        {
            //this function sorts from least to greatest leaving trailing zeroes at the end
            int nDim = A.Rank;
            //check for trailing zeroes
            for (int d0 = 0; d0 < A.GetLength(0); d0++)
            {
                for (int d1 = 0; d1 < A.GetLength(1); d1++)
                {
                    int aryLength = A.GetLength(nDim - 1);//get length of last dimension
                    for (int l = aryLength - 1; l > -1; l--)
                    {
                        //if ending values are zeroes, we leave them at the end by adjusting the array length
                        if (A[d0, d1, l] != 0) { aryLength = l + 1; break; }
                    }

                    //trimmed array
                    double[] B = new double[aryLength];
                    for (int i = 0; i < aryLength; i++) B[i] = A[d0, d1, i];
                    int[] SortIndex = SortID(B, 0);
                    for (int i = 0; i < aryLength; i++)
                    {
                        A[d0, d1, i] = B[i];
                    }
                }
            }
        }//end function

        public static void sortDoubleArrayLastDim(double[,] A)
        {
            //this function sorts from least to greatest leaving trailing zeroes at the end
            int nDim = A.Rank;
            //check for trailing zeroes
            for (int d0 = 0; d0 < A.GetLength(0); d0++)
            {
                int aryLength = A.GetLength(nDim - 1);//get length of last dimension
                for (int l = aryLength - 1; l > -1; l--)
                {
                    //if ending values are zeroes, we leave them at the end by adjusting the array length
                    if (A[d0, l] != 0) { aryLength = l + 1; break; }
                }

                //trimmed array
                double[] B = new double[aryLength];
                for (int i = 0; i < aryLength; i++) B[i] = A[d0, i];
                int[] SortIndex = SortID(B, 0);
                for (int i = 0; i < aryLength; i++)
                {
                    A[d0, i] = B[i];
                }
            }
        }//end function
        static public double[] TrimTrailingZeros(double[] A)
        {
            int L = 0;
            double[] Acopy = Duplicate(A);
            for (int l = A.Length - 1; l > -1; l--)
            {
                if (A[l] != 0)
                { L = l + 1; break; }
            }
            A = new double[L];
            for (int l = 0; l < L; l++) A[l] = Acopy[l];
            return A;
        }//end function
        /**********************************************************************************
         * ********************************************************************************/
        static public int[] SortID(double[] A, int flag)
        {//flag <0 means sort in descending order
            int n = A.Length;
            int[] SortIndex = new int[n];
            for (int i = 0; i < n; i++)
            {
                SortIndex[i] = i;
            }
            Array.Sort(A, SortIndex);
            if (flag < 0)
            { // Change the SortIndex to Beam3D in decreasing order
                int[] S2 = new int[n];
                for (int i = 0; i < n; i++)
                {
                    S2[i] = SortIndex[n - i - 1];
                }
                for (int i = 0; i < n; i++)
                {
                    SortIndex[i] = S2[i];
                }
            }
            return SortIndex;
        } // End of Function


        /// <summary>
        /// Returns the interpolated value from a set of Z's given at an XY grid
        /// </summary>
        /// <param name="XAxis">X values at the grid</param>
        /// <param name="YAxis">Y values at the grid</param>
        /// <param name="ZValues">Zvalues for each grid point</param>
        /// <param name="X">X of result point</param>
        /// <param name="Y">Y of result point</param>
        /// <param name="OutofRangeFlag">greater than 0: return last value, =0: return 0, less than 0: extrapolate</param>
        /// <returns>Z value</returns>
        static public double ZofSurface(double[] XAxis, double[] YAxis, double[,] ZValues, double X, double Y, int OutofRangeFlag)
        {
            int nx, ny, i, j;
            double r;
            nx = XAxis.Length;
            ny = YAxis.Length;
            double[,] Ycurve = new double[ny, 2];
            if (X < XAxis[0])
            {
                i = 0;
                if (OutofRangeFlag < 0) r = 0;
                else if (OutofRangeFlag == 0) return 0;
                else r = (X - XAxis[i]) / (XAxis[i + 1] - XAxis[i]);
            }
            else if (X > XAxis[nx - 1])
            {
                i = nx - 2;
                if (OutofRangeFlag < 0) r = 1;
                else if (OutofRangeFlag == 0) return 0;
                else r = (X - XAxis[i]) / (XAxis[i + 1] - XAxis[i]);
            }
            else
            {
                for (i = 0; i < nx - 1; i++)
                {
                    if (X < XAxis[i + 1]) break;
                }
                r = (X - XAxis[i]) / (XAxis[i + 1] - XAxis[i]);
            }

            for (j = 0; j < ny; j++)
            {
                Ycurve[j, 0] = YAxis[j];
                int ii = i;
                if (i == 0) ii = 0;
                else if (i == nx - 1) ii = nx - 2;
                Ycurve[j, 1] = r * ZValues[ii + 1, j] + (1 - r) * ZValues[ii, j];
            }
            double[] ZA = YofCurve(Ycurve, Y, OutofRangeFlag);
            double Z = ZA[0];
            return Z;
        } // End of Function

        /// <summary>
        /// Finds peaks of a curve
        /// </summary>
        /// <param name="Curve">[npt,2], 0:X values, 1:Y values</param>
        /// <param name="Code">greater than 0=find positive peaks, less than 0, find negative peaks</param>
        /// <returns>list of indeces of peak values</returns>
        static public int[] CurvePeaks(double[,] Curve, int Code)
        {
            int npt = Curve.GetLength(0);
            double[,] ACurve = new double[npt, 2];
            for (int i = 0; i < npt; i++) ACurve[i, 0] = Curve[i, 0];
            if (Code >= 0) { for (int i = 0; i < npt; i++) ACurve[i, 1] = Curve[i, 1]; }
            if (Code < 0) { for (int i = 0; i < npt; i++) ACurve[i, 1] = -Curve[i, 1]; }
            int[] TmpPeaks = new int[npt];
            int npk = 0;
            for (int i = 0; i < npt; i++)
            {
                if (i == 0 && ACurve[i, 1] >= ACurve[i + 1, 1])
                {
                    npk += 1;
                    TmpPeaks[npk - 1] = i;
                }
                else if (i == npt - 1 && ACurve[i, 1] >= ACurve[i - 1, 1])
                {
                    if (ACurve[i, 0] != ACurve[i - 1, 0]) // Ignore points at same location
                    {
                        npk += 1;
                        TmpPeaks[npk - 1] = i;
                    }
                }
                else
                {
                    if (ACurve[i, 1] >= ACurve[i - 1, 1] && ACurve[i, 1] >= ACurve[i + 1, 1])
                    {
                        if (ACurve[i, 0] != ACurve[i - 1, 0]) // Ignore points at same location
                        {
                            npk += 1;
                            TmpPeaks[npk - 1] = i;
                        }
                    }
                }
            }
            int[] Peaks = new int[npk];
            for (int i = 0; i < npk; i++) Peaks[i] = TmpPeaks[i];
            return Peaks;
        } // End of Function

        static public double[, ,] Extend3D(double[, ,] A, int indx, int nAdd)
        {
            // extend index #indx by nAdd rows
            int n0 = A.GetLength(0);
            int n1 = A.GetLength(1);
            int n2 = A.GetLength(2);
            int m0, m1, m2;
            if (indx == 0) m0 = n0 + nAdd; else m0 = n0;
            if (indx == 1) m1 = n1 + nAdd; else m1 = n1;
            if (indx == 2) m2 = n2 + nAdd; else m2 = n2;
            double[, ,] B = new double[m0, m1, m2];
            for (int i0 = 0; i0 < n0; i0++)
            {
                for (int i1 = 0; i1 < n1; i1++)
                {
                    for (int i2 = 0; i2 < n2; i2++)
                    {
                        B[i0, i1, i2] = A[i0, i1, i2];
                    }
                }
            }
            return B;
        }
        static public double[, , , , ,] Extend6D(double[, , , , ,] A, int indx, int nAdd)
        {
            // extend index #indx by nAdd rows
            int n0 = A.GetLength(0);
            int n1 = A.GetLength(1);
            int n2 = A.GetLength(2);
            int n3 = A.GetLength(3);
            int n4 = A.GetLength(4);
            int n5 = A.GetLength(5);
            int m0, m1, m2, m3, m4, m5;
            if (indx == 0) m0 = n0 + nAdd; else m0 = n0;
            if (indx == 1) m1 = n1 + nAdd; else m1 = n1;
            if (indx == 2) m2 = n2 + nAdd; else m2 = n2;
            if (indx == 3) m3 = n3 + nAdd; else m3 = n3;
            if (indx == 4) m4 = n4 + nAdd; else m4 = n4;
            if (indx == 5) m5 = n5 + nAdd; else m5 = n5;
            double[, , , , ,] B = new double[m0, m1, m2, m3, m4, m5];
            for (int i0 = 0; i0 < n0; i0++)
            {
                for (int i1 = 0; i1 < n1; i1++)
                {
                    for (int i2 = 0; i2 < n2; i2++)
                    {
                        for (int i3 = 0; i3 < n3; i3++)
                        {
                            for (int i4 = 0; i4 < n4; i4++)
                            {
                                for (int i5 = 0; i5 < n5; i5++)
                                {
                                    B[i0, i1, i2, i3, i4, i5] = A[i0, i1, i2, i3, i4, i5];
                                }
                            }
                        }
                    }
                }
            }
            return B;
        }
    }//end class
}//end namespace
