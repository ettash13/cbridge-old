﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBridge
{
    /// <summary>
    ///  This class defines a base Material 
    ///  
    ///  
    ///  Depends on: 
    /// </summary>
    public abstract class Material
    {
        string m_Name; // Material Name
        bool m_InUse;
        double m_E;
        double m_PoissonRatio;
        double m_UnitWeight;

        /// <summary>
        /// get/Set for parameters
        /// </summary>
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public bool InUse
        {    get { return m_InUse; }
             set { m_InUse = value; }
        }

    public double E
        {
            get { return m_E; }
            set { m_E = value; }
        }

        public double PoissonRatio
        {
            get { return m_PoissonRatio; }
            set { m_PoissonRatio = value; }
        }

        public double UnitWeight
        {
            get { return m_UnitWeight; }
            set { m_UnitWeight = value; }
        }


        public double[] Prop
        {
            get { double[] Prop1 = new double[3];
                  Prop1[0]        = E;
                  Prop1[1]        = PoissonRatio;
                  Prop1[2]        = UnitWeight;    return Prop1; }

            set { E             = value[0];
                  PoissonRatio  = value[1];
                  UnitWeight    = value[2]; }
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        public Material()
        {
            Name          = "";
            InUse         = false;
            E             = 0.0;
            PoissonRatio  = 0.0;
            UnitWeight    = 0.0;
        }
        
        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        public Material(string iName, double[] iProp) 
        {
            Name = iName;
            InUse = false;
            SetProp(iProp);
        }
                
        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="iProp">E, nu(poisson ratio), gamma (unit weight)</param>
        public Material(string iName, double e, double pr, double gama)
        {
            Name          = iName;
            InUse         = false;
            E             = e;
            PoissonRatio  = pr;
            UnitWeight    = gama;
        }
        
        /// <summary>
        /// sets the material properties
        /// </summary>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        /// <returns>false=failed, true=successful</returns>
        public void SetProp(double[] iProp)
        { 
            E             = iProp[0];
            PoissonRatio  = iProp[1];
            UnitWeight    = iProp[2];      
        }        
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    ///  This class defines an Elastic Material 
    ///  
    ///  Depends on: 
    /// </summary>
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public class MaterialElastic : Material
    { 
        /// <summary>
        /// Constructor
        /// </summary>
        public MaterialElastic() : base()
        { 
        }

        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        public MaterialElastic(string iName, double[] iProp) : base(iName, iProp)
        {
        }

        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="E">Elastic Modulus</param>
        /// <param name="pr">Poissson Ratio</param>
        /// <param name="Alfa">Coef. of Thermal Expansion</param>
        /// <param name="gama">Unit Weight</param>
        public MaterialElastic (string iName, double E, double pr, double gama) : base(iName, E, pr, gama)
        { 
        }

        /// <summary>
        /// Returns a complete duplicate (deep-copy) of the object
        /// </summary>
        /// <returns>A duplicate of this object</returns>
        public MaterialElastic Duplicate()
        {
            MaterialElastic newMat = new MaterialElastic(Name, E, PoissonRatio, UnitWeight);
            return newMat;
        }
    }
       
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    ///  This class defines a Concrete Material 
    ///  
    ///  
    ///  Depends on: 
    /// </summary>
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public class MaterialConcrete : Material
    {
        double m_Fc;
        double m_Fci;  

        public double Fc
        {
            get { return m_Fc; }
            set { m_Fc = value; }
        }

        public double Fci
        {
            get { return m_Fci; }
            set { m_Fci = value; }
        }
        
        /// <summary>
        /// get/Set for parameters
        /// </summary>
        public double[] Prop
        {
            get { double[]    Prop1 = new double[5];
                  Prop1[0]    = E;
                  Prop1[1]    = PoissonRatio;
                  Prop1[2]    = UnitWeight;
                  Prop1[3]    = Fc;
                  Prop1[4]    = Fci;  return Prop1; }
            set
            {     E           = value[0];
                  PoissonRatio= value[1];
                  UnitWeight  = value[2];
                  Fc          = value[3];
                  Fci         = value[4];  }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public MaterialConcrete() :base()
        {
            Fc   = 0.0;
            Fci  = 0.0;
        }

        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        public MaterialConcrete(string iName, double[] iProp) :base( iName, iProp)
        {
            SetProp(iProp);
        }
        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="E">Elastic Modulus</param>
        /// <param name="pr">Poissson Ratio</param>
        /// <param name="Alfa">Coef. of Thermal Expansion</param>
        /// <param name="gama">Unit Weight</param>
        public MaterialConcrete(string iName, double E, double pr, double gama, double alfa, double beta) :base(iName, E, pr, gama)
        {
            Fc    = alfa;
            Fci   = beta;
        }

        /// <summary>
        /// Returns a complete duplicate (deep-copy) of the object
        /// </summary>
        /// <returns>A duplicate of this object</returns>
        public MaterialConcrete Duplicate()
        {
            MaterialConcrete newMat = new MaterialConcrete(Name, E, PoissonRatio, UnitWeight, Fc, Fci);
            return newMat;
        }

        /// <summary>
        /// sets the material properties
        /// </summary>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        /// <returns>false=failed, true=successful</returns>
        public void SetProp(double[] iProp) 
        {
            Fc    = iProp[3];
            Fci   = iProp[4];    
        }
    }
        
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    ///  This class defines Mild Steel Material
    ///  
    ///  
    ///  Depends on: 
    /// </summary>
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public class MaterialMildSteel : Material
    {
        double m_Fy;
        double m_Fu;
        double m_Grade;

        public double Fy
        {
            get { return m_Fy; }
            set { m_Fy = value; }
        }

        public double Fu
        {
            get { return m_Fu; }
            set { m_Fu = value; }
        }

        public double Grade
        {
            get { return m_Grade; }
            set { m_Grade = value; }
        }
        
        /// <summary>
        /// get/Set for parameters
        /// </summary>
        public double[] Prop
        {
            get {  double[] Prop1 = new double[6];

                    Prop1[0] = E;
                    Prop1[1] = PoissonRatio;
                    Prop1[2] = UnitWeight;

                    Prop1[3] = Fy;
                    Prop1[4] = Fu;
                    Prop1[5] = Grade;  return Prop1; }

            set  {  E             = value[0];
                    PoissonRatio  = value[1];
                    UnitWeight    = value[2];
                    Fy            = value[3];
                    Fu            = value[4];
                    Grade         = value[5]; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public MaterialMildSteel() : base()
        {
            Fy    = 0.0;
            Fu    = 0.0;
            Grade = 0.0;
        }

        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        public MaterialMildSteel(string iName, double[] iProp) : base(iName, iProp)
        {
            SetProp(iProp);
        }
        
        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="E">Elastic Modulus</param>
        /// <param name="pr">Poissson Ratio</param>
        /// <param name="Alfa">Coef. of Thermal Expansion</param>
        /// <param name="gama">Unit Weight</param>
        public MaterialMildSteel(string iName, double E, double pr, double gama, double alfa, double beta, double grade) : base(iName, E, pr, gama)
        {
            Fy    = alfa;
            Fu    = beta;
            Grade = grade; 
        }

        /// <summary>
        /// Returns a complete duplicate (deep-copy) of the object
        /// </summary>
        /// <returns>A duplicate of this object</returns>
        public MaterialMildSteel Duplicate()
        {
            MaterialMildSteel newMat = new MaterialMildSteel(Name, E, PoissonRatio, UnitWeight, Fy, Fu, Grade);
            return newMat;
        }

        /// <summary>
        /// sets the material properties
        /// </summary>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        /// <returns>false=failed, true=successful</returns>
        public void SetProp(double[] iProp)
        {
            Fy    = iProp[3];
            Fu    = iProp[4];
            Grade = iProp[5];
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    ///  This class defines Strand Steel Material
    ///  
    ///  
    ///  Depends on: 
    /// </summary>
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public class MaterialStrand : MaterialMildSteel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MaterialStrand() : base()
        {

        }

        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        public MaterialStrand(string iName, double[] iProp) : base(iName, iProp)
        {

        }

        /// <summary>
        /// Cosntructor with material rpoperties
        /// </summary>
        /// <param name="iName">name of material</param>
        /// <param name="E">Elastic Modulus</param>
        /// <param name="pr">Poissson Ratio</param>
        /// <param name="Alfa">Coef. of Thermal Expansion</param>
        /// <param name="gama">Unit Weight</param>
        public MaterialStrand(string iName, double E, double pr, double gama, double alfa, double beta, double grade) : base(iName, E, pr, gama, alfa, beta, grade)
        {

        }

        /// <summary>
        /// Returns a complete duplicate (deep-copy) of the object
        /// </summary>
        /// <returns>A duplicate of this object</returns>
        public MaterialStrand Duplicate()
        {
            MaterialStrand newMat = new MaterialStrand(Name, E, PoissonRatio, UnitWeight, Fy, Fu, Grade);
            return newMat;
        }

        /// <summary>
        /// sets the material properties
        /// </summary>
        /// <param name="iProp">E, nu(poisson ratio), alpha(thrmal Coeff), unit weight</param>
        /// <returns>false=failed, true=successful</returns>
        public void SetProp(double[] iProp)
        {
       
        }
    }
}

