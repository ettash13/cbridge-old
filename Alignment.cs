using System;
using System.Collections.Generic;
using System.Text;

namespace CBridge
{
    ///  Copyright 2013 Zocon Consulting
    ///  Creator:  Luke Miner
    ///  Date:  11/13
    /// <summary>
    ///  This class creates an alignment along which a bridge can be defined.  This definition includes a name, starting point,
    ///  and curve definition by radius and length.
    ///  
    ///  Depends on: 
    /// </summary>
    public class Alignment
    {
        public string m_Name;
        public double[] XYZ0 = new double[3];//starting point of bridge
        public int NCurve;//number of defined curves
        public double[] CrvAzStart; // Azimuth of start of Curve
        public double[] CrvLength; //length along the bridge
        public double[] CrvRadius; // Positive is to the left, 0 means straight
        public double[] CrvDelta;  // Turn/Cenral angle of curve
        public double[,] CrvXYend;  // [2,NCurve}: X&Y coordinate at end of curve
        public double LTotal;      // Total length of alignment (All curves)
        // Vertical Curves
        public int m_NVCurve; // # of Vertical Curves
        public double m_VSlopeStart; // Starting slope of each parabola
        public double[] m_VCrvRun; // Run of each curve
        public double[] m_VCrvRise; // Rise of vertical curves

        /// <summary>
        /// default constructor
        /// </summary>
        public Alignment()
        {
            m_Name = "";
            XYZ0 = null;
            NCurve=0;
            CrvAzStart = null;
            CrvLength = null;
            CrvRadius = null;
            CrvDelta = null;
            CrvXYend = null;
            LTotal = 0;
        }

        /// <summary>
        /// copy constructor
        /// </summary>
        public Alignment(Alignment S)
        {
            m_Name = S.m_Name;
            XYZ0[0] = S.XYZ0[0];
            XYZ0[1] = S.XYZ0[1];
            XYZ0[2] = S.XYZ0[2];
            NCurve = S.NCurve;
            LTotal = S.LTotal;

            CrvAzStart = MatrixOperations.Duplicate(S.CrvAzStart);
            CrvLength = MatrixOperations.Duplicate(S.CrvLength);
            CrvRadius = MatrixOperations.Duplicate(S.CrvRadius);
            CrvDelta = MatrixOperations.Duplicate(S.CrvDelta);
            CrvXYend = MatrixOperations.Duplicate(S.CrvXYend);

        }

        /// <summary>
        /// Copy Constructor with an offset
        /// </summary>
        /// <param name="Name">Name of new Alihgnment</param>
        /// <param name="S">Source Alignment</param>
        /// <param name="Offset"></param>
        /// <param name="Offset"> Offset distance, +ve means to the left while looking ahead station</param>
        /// 
        public Alignment(string Name, Alignment S, double Offset)
        {
            double tol = 1.0e-10;
            m_Name = Name;
            double a = Math.PI/2.0 - S.CrvAzStart[0];  // Curve angle with respect to X axis
            XYZ0[0] = S.XYZ0[0] - Offset * Math.Sin(a);
            XYZ0[1] = S.XYZ0[1] + Offset * Math.Cos(a);
            XYZ0[2] = S.XYZ0[2];
            NCurve = S.NCurve;
            CrvAzStart = new double[NCurve];
            CrvLength = new double[NCurve];
            CrvRadius = new double[NCurve];
            CrvDelta = new double[NCurve];
            CrvXYend = new double[2, NCurve];

            LTotal = 0;
            for (int i = 0; i < NCurve; i++)
            {
                CrvAzStart[i] = S.CrvAzStart[i];
                CrvDelta[i] = S.CrvDelta[i];
                double Xo = XYZ0[0];
                double Yo = XYZ0[1];
                if (i > 0) { Xo = CrvXYend[0, i - 1]; Yo = CrvXYend[1, i - 1]; } 
                if (Math.Abs(S.CrvRadius[i]) < tol)
                {
                    CrvRadius[i] = 0;
                    CrvLength[i] = S.CrvLength[i];
                    CrvXYend[0, i] = Xo + CrvLength[i] * Math.Cos(a);
                    CrvXYend[1, i] = Yo + CrvLength[i] * Math.Sin(a);
                }
                else
                {
                    CrvRadius[i] = S.CrvRadius[i] - Offset;
                    CrvLength[i] = CrvRadius[i] * CrvDelta[i];
                    double delta0 = Math.PI / 2 - CrvAzStart[i];
                    CrvXYend[0, i] = Xo + CrvRadius[i] * (Math.Sin(delta0+ CrvDelta[i]) - Math.Sin(delta0));
                    CrvXYend[1, i] = Yo - CrvRadius[i] * (Math.Cos(delta0+ CrvDelta[i]) - Math.Cos(delta0));
                }
                LTotal += CrvLength[i];
            }
            //CrvAzStart = MatrixOperations.Duplicate(S.CrvAzStart);
            //CrvLength = MatrixOperations.Duplicate(S.CrvLength);
            //CrvRadius = MatrixOperations.Duplicate(S.CrvRadius);
            //CrvDelta = MatrixOperations.Duplicate(S.CrvDelta);

        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="XYZStart">starting point of bridge</param>
        /// <param name="AzStart">Azimuth of start of Curve</param>
        /// <param name="CrvRad">Radii: Positive is to the left, 0 means straight</param>
        /// <param name="CrvLen">Curve length along the Alignment</param>
        public Alignment(string name, double[] XYZStart, double AzStart, double[] CrvRad, double[] CrvLen )
        {
            double pi = 4*Math.Atan(1);
            m_Name = name;
            for (int i = 0; i < 3; i++) XYZ0[i] = XYZStart[i];
            NCurve = CrvRad.Length;
            CrvAzStart = new double[NCurve];
            CrvLength = new double[NCurve];
            CrvRadius = new double[NCurve];
            CrvDelta = new double[NCurve];
            CrvXYend = new double[2,NCurve];
            CrvAzStart[0] = AzStart;
            double X0 = XYZ0[0];
            double Y0 = XYZ0[1];
            double Xp, Yp;
            LTotal = 0.0;
            for (int i = 0; i < NCurve; i++)
            {
                if (i > 0) CrvAzStart[i] = CrvAzStart[i - 1] - CrvDelta[i-1];
                CrvRadius[i] = CrvRad[i];
                CrvLength[i] = CrvLen[i];
                LTotal += CrvLen[i];
                if (CrvRadius[i] == 0)
                {
                    CrvDelta[i] = 0;
                    Xp = CrvLength[i];
                    Yp = 0.0;
                }
                else
                {
                    CrvDelta[i] = CrvLength[i] / CrvRadius[i];
                    Xp = CrvRadius[i] * Math.Sin(CrvDelta[i]);
                    Yp = CrvRadius[i] * (1.0 - Math.Cos(CrvDelta[i]));
                }
                double Beta = 0.5 * pi - CrvAzStart[i];
                CrvXYend[0, i] = X0 + Xp * Math.Cos(Beta) - Yp * Math.Sin(Beta);
                CrvXYend[1, i] = Y0 + Xp * Math.Sin(Beta) + Yp * Math.Cos(Beta);
                X0 = CrvXYend[0, i];
                Y0 = CrvXYend[1, i];
            }
        }//end constructor
        //*******************************************************************************************
        /// <summary>
        /// This function takes an x-distance along the alignment and returns the x and y coordinates as well as the tangent angle of the alignment at that x distance
        /// </summary>
        /// <param name="Xa">x-distance along the alignment</param>
        /// <returns>a double[3] where [0] is the global x coord, [1] is the global y coord, [2] is tangent angle of alignment</returns>
        public double[] GetXY(double Xa)
        {
            double pi = 4 * Math.Atan(1.0);
            double[] XY = new double[3];
            if (NCurve == 0)  // No curves given, assume one straight line
            {
                XY[0] = Xa;
                XY[1] = 0.0;
                XY[2] = 0.0;
            }
            else
            {
                if (Xa <= 0)
                {  // before the start, extrapolate on straight line
                    double Beta = 0.5 * pi - CrvAzStart[0];
                    XY[0] = XYZ0[0] + Xa * Math.Cos(Beta);
                    XY[1] = XYZ0[1] + Xa * Math.Sin(Beta);
                    XY[2] = 0.5 * pi - CrvAzStart[0];
                }
                else if (Xa >= LTotal)
                { // After the end, extrapolate on straight line
                    double Beta = (0.5 * pi - CrvAzStart[NCurve - 1]) + CrvDelta[NCurve - 1];
                    XY[0] = CrvXYend[0, NCurve - 1] + (Xa - LTotal) * Math.Cos(Beta);
                    XY[1] = CrvXYend[1, NCurve - 1] + (Xa - LTotal) * Math.Sin(Beta);
                    XY[2] = Beta;
                }
                else
                {  // Find the curve and calculate
                    double X0 = XYZ0[0];
                    double Y0 = XYZ0[1];
                    double Lsum = 0;  // Sum of previous curve lengths
                    for (int i = 0; i < NCurve; i++)
                    {
                        Lsum += CrvLength[i];
                        if (Xa < Lsum) // in this curve
                        {
                            double DeltaXa, Xp, Yp;
                            if (CrvRadius[i] == 0)
                            {
                                DeltaXa = 0;
                                Xp = Xa - (Lsum - CrvLength[i]);
                                Yp = 0.0;
                            }
                            else
                            {
                                DeltaXa = (Xa - (Lsum - CrvLength[i])) / CrvRadius[i];
                                Xp = CrvRadius[i] * Math.Sin(DeltaXa);
                                Yp = CrvRadius[i] * (1.0 - Math.Cos(DeltaXa));
                            }
                            double Beta = 0.5 * pi - CrvAzStart[i];
                            XY[0] = X0 + Xp * Math.Cos(Beta) - Yp * Math.Sin(Beta);
                            XY[1] = Y0 + Xp * Math.Sin(Beta) + Yp * Math.Cos(Beta);
                            XY[2] = Beta +DeltaXa;
                            break;
                        }
                        X0 = CrvXYend[0,i];
                        Y0 = CrvXYend[1,i];
                    }
                }
            }
            return XY;
        }//end function
        /*************************************************************************************/
        /// <summary>
        /// This indicates whether or not this alignment is curved by checking if curve radii are zero for entire alignment
        /// </summary>
        /// <returns>true if curved, false if not curved</returns>
        public bool isCurved()
        {
            if (CrvRadius != null)
            {
                for (int i = 0; i < CrvRadius.Length; i++)
                {
                    if (CrvRadius[i] != 0) return true;
                }
            }
            return false;
        }//end function

        /*************************************************************************************/
        /// <summary>
        /// Find crossing point of a  line and the alignment, if exists
        /// </summary>
        /// <param name="XL">X coord of a point on line</param>
        /// <param name="YL">Y coord of the point on line</param>
        /// <param name="SlopeAngle">Angle of line with X axis</param>
        /// <returns>X and Y of the crossing point</returns>
        public double[] LineCrossXY(double XL, double YL, double SlopeAngle)
        {
            int NXpt = 0; // Number of alignment segments crossed
            double[,] Xpts = new double[2, 10]; // All crossing points
            double[] Xpt = null; // final crossing point (shortest distance)
            double tol = 0.001;
            // Loop over all sgments until you find a valid crossing
            double A0 = Math.PI / 2.0 - CrvAzStart[0];  // Slope angle at start
            double X0 = XYZ0[0];
            double Y0 = XYZ0[1];
            double X1 = CrvXYend[0, 0];
            double Y1 = CrvXYend[0, 1];
            // Check if crossed before start of alignment
            Xpt = LineXLine(X0, Y0, A0, XL, YL, SlopeAngle);
            if (Xpt != null)
            {
                double AZ0 = AZLine(Xpt[0], Xpt[1], X0, Y0);
                if (Math.Abs(CrvAzStart[0] - AZ0) < tol) return Xpt; // crossing in opposite direction of azimuth
            }
            // Check if crossed after end of alignment
            double endAngle = Math.PI / 2.0 - (CrvAzStart[NCurve - 1] - CrvDelta[NCurve - 1]);
            Xpt = LineXLine(CrvXYend[0, (NCurve - 1)], CrvXYend[1, NCurve - 1], endAngle, XL, YL, SlopeAngle);
            if (Xpt != null)
            {
                double AZ0 = AZLine(Xpt[0], Xpt[1], X0, Y0);
                if (Math.Abs(CrvAzStart[0] - AZ0) < tol) return Xpt; // crossing in opposite direction of azimuth
            }
            // Possible crossing in within defined alignment, check all curves
            for (int icrv = 0; icrv < NCurve; icrv++)
            {
                Xpt = null;
                double R0 = CrvRadius[icrv];
                double L0 = CrvLength[icrv];
                double Xend = CrvXYend[0, icrv];
                double Yend = CrvXYend[1, icrv];
                double Aend = A0 + CrvDelta[icrv];
                if (R0 == 0) // This segment is linear
                {
                    double[] Xpt1 = LineXLine(X0, Y0, A0, XL, YL, SlopeAngle);
                    if (Xpt1 != null)
                    {
                        if ((Math.Abs(Xpt1[0] - X0) <= Math.Abs(Xend - X0)) && (Math.Abs(Xpt1[0] - X1) <= Math.Abs(Xend - X0))) Xpt = Xpt1; // crossing in within the segment
                    }
                }
                else
                {
                    double Xo = X0 - R0 * Math.Sin(A0);  // X& Y of center of citrcle
                    double Yo = Y0 + R0 * Math.Cos(A0);
                    double XX0 = X0 - Xo;  // X&Y coordinates transformed to CS at center of circle
                    double YY0 = Y0 - Yo;
                    double XXL = XL - Xo;
                    double YYL = YL - Yo;
                    double[] Xpt1 = new double[2]; double[] Xpt2 = new double[2]; // crossing points relative to center of circle
                    // find crossing of line and circle
                    if (Math.Abs(SlopeAngle) == Math.PI / 2) // Vertical, crosses at top & bottom of circle
                    {
                        if (XXL < -R0 || XXL > R0) // no crossing
                        {
                            Xpt1 = null; Xpt2 = null;
                        }
                        else
                        {
                            double Slope = XXL / R0;
                            Xpt1[0] = XXL; Xpt1[1] = Math.Sqrt(R0 * R0 - XXL * XXL);
                            Xpt2[0] = XXL; Xpt2[1] = -Xpt1[1];
                        }
                    }
                    else
                    {
                        double SL = Math.Tan(SlopeAngle);
                        double AQ = 1 + SL * SL;
                        double BQ = 2 * SL * (YYL - SL * XXL);
                        double CQ = (YYL - (SL * XXL)) * (YYL - SL * XXL) - R0 * R0;
                        double B2_4AC = BQ * BQ - 4 * AQ * CQ;
                        if (B2_4AC < 0)  // no solution, does not cross circle
                        {
                            Xpt1 = null;
                            Xpt2 = null;
                        }
                        else
                        {
                            Xpt1[0] = (-BQ - Math.Sqrt(B2_4AC)) / (2 * AQ);
                            Xpt1[1] = YYL + SL * (Xpt1[0] - XXL);
                            Xpt2[0] = (-BQ + Math.Sqrt(B2_4AC)) / (2 * AQ);
                            Xpt2[1] = YYL + SL * (Xpt2[0] - XXL);
                        }
                    }
                    // If there was a crossing, see if either point is within limits of the curve
                    if (Xpt1 != null)
                    {
                        double Az1 = AZLine(0, 0, Xpt1[0], Xpt1[1]); // Azimuth from center of citrcle to first crossing
                        double Az2 = AZLine(0, 0, Xpt2[0], Xpt2[1]); // Azimuth from center of citrcle to second crossing
                        double AzStart = AZLine(0, 0, XX0, YY0);     // Azimuth from center of citrcle to start of curve
                        double AzEnd = AZLine(Xo, Yo, Xend, Yend); // Azimuth from center of citrcle to end of curve
                        if (R0 > 0) // Curve to left (CCW)
                        {
                            if (AzEnd > AzStart) // crossing over top, adjust AZEnd
                            {
                                if (Az1 <= AzStart || Az1 >= AzEnd) Xpt = Xpt1; // Crosses outside start and end, save it
                            }
                            else
                            {
                                if (Az1 <= AzStart && Az1 >= AzEnd) Xpt = Xpt1; // Crosses between start and end, save it
                            }
                            double[] Xpt2a = null;
                            if (AzEnd > AzStart) // crossing over top, adjust AZEnd
                            {
                                if (Az2 <= AzStart || Az2 >= AzEnd) Xpt2a = Xpt2; // Crosses between start and end, save it
                            }
                            else
                            {
                                if (Az2 <= AzStart && Az2 >= AzEnd) Xpt2a = Xpt2; // Crosses between start and end, save it
                            }
                            if (Xpt2a != null)
                            {
                                if (Xpt == null)
                                {
                                    Xpt = Xpt2a;
                                }
                                else
                                {
                                    double L1 = Math.Sqrt((Xpt1[0] - XXL) * (Xpt1[0] - XXL) + (Xpt1[1] - YYL) * (Xpt1[1] - YYL));
                                    double L2 = Math.Sqrt((Xpt2[0] - XXL) * (Xpt2[0] - XXL) + (Xpt2[1] - YYL) * (Xpt2[1] - YYL));
                                    if (L2 < L1) Xpt = Xpt2;
                                }
                            }
                        }
                        else  // curve to the right (CW)
                        {
                            if (AzStart > AzEnd) // crossing over top, adjust AZEnd
                            {
                                if (Az1 >= AzStart || Az1 <= AzEnd) Xpt = Xpt1; // Crosses between start and end, save it
                            }
                            else
                            {
                                if (Az1 >= AzStart && Az1 <= AzEnd) Xpt = Xpt1; // Crosses between start and end, save it
                            }
                            double[] Xpt2a = null;
                            if (AzStart > AzEnd) // crossing over top, adjust AZEnd
                            {
                                if (Az2 >= AzStart || Az2 <= AzEnd) Xpt2a = Xpt2; // Crosses between start and end, save it
                            }
                            else
                            {
                                if (Az2 >= AzStart && Az2 <= AzEnd) Xpt2a = Xpt2; // Crosses between start and end, save it
                            }
                            if (Xpt2a != null) // Check shortest distance
                            {
                                if (Xpt == null)
                                {
                                    Xpt = Xpt2a;
                                }
                                else
                                {
                                    double L1 = Math.Sqrt((Xpt1[0] - XXL) * (Xpt1[0] - XXL) + (Xpt1[1] - YYL) * (Xpt1[1] - YYL));
                                    double L2 = Math.Sqrt((Xpt2[0] - XXL) * (Xpt2[0] - XXL) + (Xpt2[1] - YYL) * (Xpt2[1] - YYL));
                                    if (L2 < L1) Xpt = Xpt2;
                                }
                            }
                        }
                    }
                    if (Xpt != null)
                    {
                        // return the point to original XY coord
                        Xpt[0] += Xo;
                        Xpt[1] += Yo;
                        // Check if Azimuth is in same direction or opposite
                        double AZoLine = Math.PI / 2 - SlopeAngle;
                        if (AZoLine < 0) AZoLine += 2 * Math.PI;
                        double AZXLine = AZLine(XL, YL, Xpt[0], Xpt[1]);
                        if (Math.Abs(AZoLine - AZXLine) > tol) Xpt = null;
                    }
                } // end of checking circular segment
                if (Xpt != null)
                {
                    NXpt += 1;
                    double Arrayelength = Xpts.GetLength(1);
                    if (Arrayelength < NXpt)  // Extend the array length
                    {
                        double[,] NewArray = new double[2, NXpt + 10];
                        for (int i = 0; i < Arrayelength; i++) { NewArray[0, i] = Xpts[0, i]; NewArray[1, i] = Xpts[1, i]; }
                        Xpts = NewArray;
                    }
                    Xpts[0, NXpt - 1] = Xpt[0]; Xpts[1, NXpt - 1] = Xpt[1];
                }
                A0 = Aend;  // Slope angle at start
                X0 = Xend;
                Y0 = Yend;
            }
            // Checked all curves, find shortest distance
            double Lmin = 1.0e+10;
            for (int i = 0; i < NXpt; i++)
            {
                double L = Math.Sqrt((Xpts[0, i] - XL) * (Xpts[0, i] - XL) + (Xpts[1, i] - YL) * (Xpts[1, i] - YL));
                if (L < Lmin) { Lmin = L; Xpt = new double[2]; Xpt[0] = Xpts[0, i]; Xpt[1] = Xpts[1, i]; }
            }
            return Xpt;
        }

        /*************************************************************************************/
        /// <summary>
        /// Find crossing point of a  line and the alignment, if exists
        /// </summary>
        /// <param name="XL">X coord of a point on line</param>
        /// <param name="YL">Y coord of the point on line</param>
        /// <param name="SlopeAngle">Angle of line with X axis</param>
        /// <returns>X, Y, & Location along alignment of the closest crossing point</returns>
        public double[] LineCross(double XL, double YL, double SlopeAngle)
        {
            double[] PointLoc = new double[3]; // Return value - X, Y & location along alignment = Shortest distance crossing
            double PointLocb = 0; // location along alignment at crossing before start
            double PointLoce = 0; // location along alignment at crossing after end
            double[] XptA = null; // A crossing points (X, Y, X along Curve)
            int XptCur = 0; // Crossing point Curve # 
            int NXpt = 0; // Number of alignment segments crossed
            double[,] Xpts = new double[3, 10]; // All crossing points (X, Y, X along Curve)
            int[] XptsCurv = new int[10];       // All crossing points (Curve #)
            double[] Xpt = null; // A crossing point
            double[] Xptb = null; // Crossing point before start of alignment, if exists
            double[] Xpte = null; // Crossing point after end of alignment, if exists
            double tol = 0.001;
            // Loop over all sgments until you find a valid crossing
            double A0 = Math.PI / 2.0 - CrvAzStart[0];  // Slope angle at start
            double X0 = XYZ0[0];
            double Y0 = XYZ0[1];
            double X1 = CrvXYend[0, 0];
            double Y1 = CrvXYend[0, 1];
            // Check if crossed before start of alignment
            Xptb = LineXLine(X0, Y0, A0, XL, YL, SlopeAngle);
            if (Xptb != null)
            {
                double AZ0 = AZLine(Xptb[0], Xptb[1], X0, Y0);
                if (Math.Abs(CrvAzStart[0] - AZ0) < tol)
                {   // crossing in opposite direction of azimuth
                    // Get the location along alignment
                    PointLocb = -Math.Sqrt((Xptb[0] - X0) * (Xptb[0] - X0) + (Xptb[1] - Y0) * (Xptb[1] - Y0));
                }
                else
                {
                    Xptb = null;
                }
            }
            // Check if crossed after end of alignment
            double endAngle = Math.PI / 2.0 - (CrvAzStart[NCurve - 1] - CrvDelta[NCurve - 1]);
            Xpte = LineXLine(CrvXYend[0, (NCurve - 1)], CrvXYend[1, NCurve - 1], endAngle, XL, YL, SlopeAngle);
            if (Xpte != null)
            {
                double AZ0 = AZLine(CrvXYend[0, (NCurve - 1)], CrvXYend[1, NCurve - 1], Xpte[0], Xpte[1]);
                if (Math.Abs(CrvAzStart[(NCurve - 1)] - AZ0) < tol)
                {   // crossing in opposite direction of azimuth
                    // Get the location along alignment
                    PointLoce = LTotal + Math.Sqrt((Xpte[0] - X0) * (Xpte[0] - X0) + (Xpte[1] - Y0) * (Xpte[1] - Y0));
                }
                else
                {
                    Xpte = null;
                }
            }
            // Possible crossing is within defined alignment, check all curves
            for (int icrv = 0; icrv < NCurve; icrv++)
            {
                Xpt = null;
                double R0 = CrvRadius[icrv];
                double L0 = CrvLength[icrv];
                double Xend = CrvXYend[0, icrv];
                double Yend = CrvXYend[1, icrv];
                double Aend = A0 + CrvDelta[icrv];
                if (R0 == 0) // This segment is linear
                {
                    double[] Xpt1 = LineXLine(X0, Y0, A0, XL, YL, SlopeAngle);
                    if (Xpt1 != null)
                    {
                        if ((Math.Abs(Xpt1[0] - X0) <= Math.Abs(Xend - X0)) && (Math.Abs(Xpt1[1] - Y0) <= Math.Abs(Yend - Y0)))
                        {// crossing is within the segment
                            XptA = new double[3];
                            XptA[0] = Xpt1[0]; XptA[1] = Xpt1[1];
                            XptA[2] = Math.Sqrt((Xpt1[0] - X0) * (Xpt1[0] - X0) + (Xpt1[1] - Y0) * (Xpt1[1] - Y0));
                            XptCur = icrv;
                        }
                    }
                }
                else
                {
                    XptA = LineXArc(XL, YL, SlopeAngle, X0, Y0, A0, R0, L0);
                    if (XptA != null) XptA[2] *= Math.Abs(R0);
                    XptCur = icrv;
                } // end of checking circular segment
                if (XptA != null)
                {
                    NXpt += 1;
                    double Arrayelength = Xpts.GetLength(1);
                    if (Arrayelength < NXpt)  // Extend the array length
                    {
                        double[,] NewArray = new double[3, NXpt + 10];
                        int[] NewArrayC = new int[NXpt + 10];
                        for (int i = 0; i < Arrayelength; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                NewArray[j, i] = Xpts[j, i];
                            }
                            NewArrayC[i] = XptsCurv[i];
                        }
                        Xpts = NewArray;
                        XptsCurv = NewArrayC;
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        Xpts[j, NXpt - 1] = XptA[j];
                    }
                    XptsCurv[NXpt - 1] = XptCur;
                }
                A0 = Aend;  // Slope angle at start
                X0 = Xend;
                Y0 = Yend;
            }
            //if no crossings are found
            if (NXpt == 0 && Xptb == null && Xpte == null) return null;
            // Checked all curves, find shortest distance
            int PointIndex = 0;
            double Lmin = 1.0e+10;
            for (int i = 0; i < NXpt; i++)
            {
                double L = Math.Sqrt((Xpts[0, i] - XL) * (Xpts[0, i] - XL) + (Xpts[1, i] - YL) * (Xpts[1, i] - YL));
                if (L < Lmin) { Lmin = L; PointIndex = i; }
            }
            // Calculate the distance into alignment
            int jCurve = XptsCurv[PointIndex] - 1;

            for (int i = 0; i <= jCurve; i++)
            {
                PointLoc[2] += CrvLength[i];
            }
            PointLoc[0] = Xpts[0, PointIndex];
            PointLoc[1] = Xpts[1, PointIndex];
            PointLoc[2] += Xpts[2, PointIndex];
            // Check against crossing before start of alignment
            if (Xptb != null)
            {
                double L = Math.Sqrt((Xptb[0] - XL) * (Xptb[0] - XL) + (Xptb[1] - YL) * (Xptb[1] - YL));
                if (L < Lmin) { Lmin = L; PointLoc[0] = Xptb[0]; PointLoc[1] = Xptb[1]; PointLoc[2] = PointLocb; }
            }
            // Check against crossing after end of alignment
            if (Xpte != null)
            {
                double L = Math.Sqrt((Xpte[0] - XL) * (Xpte[0] - XL) + (Xpte[1] - YL) * (Xpte[1] - YL));
                if (L < Lmin) { Lmin = L; PointLoc[0] = Xpte[0]; PointLoc[1] = Xpte[1]; PointLoc[2] = PointLoce; }
            }
            return PointLoc;
        }
        /*************************************************************************************/
        /// <summary>
        /// Find crossing point of a  line and the alignment, if exists
        /// </summary>
        /// <param name="XL">X coord of a point on line</param>
        /// <param name="YL">Y coord of the point on line</param>
        /// <param name="SlopeAngle">Angle of line with X axis</param>
        /// <returns>Location along alignment of the crossing point</returns>
        public double LineCrossXa(double XL, double YL, double SlopeAngle)
        {
            double PointLoc = 0; // Return value - location along alignment = Shortest distance crossing
            double PointLocb = 0; // location along alignment at crossing before start
            double PointLoce = 0; // location along alignment at crossing after end
            double[] XptA = null; // A crossing points (X, Y, X along Curve)
            int XptCur = 0; // Crossing point Curve # 
            int NXpt = 0; // Number of alignment segments crossed
            double[,] Xpts = new double[3, 10]; // All crossing points (X, Y, X along Curve)
            int[] XptsCurv = new int[10];       // All crossing points (Curve #)
            double[] Xpt = null; // A crossing point
            double[] Xptb = null; // Crossing point before start of alignment, if exists
            double[] Xpte = null; // Crossing point after end of alignment, if exists
            double tol = 0.001;
            // Loop over all sgments until you find a valid crossing
            double A0 = Math.PI / 2.0 - CrvAzStart[0];  // Slope angle at start
            double X0 = XYZ0[0];
            double Y0 = XYZ0[1];
            double X1 = CrvXYend[0, 0];
            double Y1 = CrvXYend[0, 1];
            // Check if crossed before start of alignment
            Xptb = LineXLine(X0, Y0, A0, XL, YL, SlopeAngle);
            if (Xptb != null)
            {
                double AZ0 = AZLine(Xptb[0], Xptb[1], X0, Y0);
                if (Math.Abs(CrvAzStart[0] - AZ0) < tol)
                {   // crossing in opposite direction of azimuth
                    // Get the location along alignment
                    PointLocb = -Math.Sqrt((Xptb[0] - X0) * (Xptb[0] - X0) + (Xptb[1] - Y0) * (Xptb[1] - Y0));
                }
                else
                {
                    Xptb = null;
                }
            }
            // Check if crossed after end of alignment
            double endAngle = Math.PI / 2.0 - (CrvAzStart[NCurve - 1] - CrvDelta[NCurve - 1]);
            Xpte = LineXLine(CrvXYend[0, (NCurve - 1)], CrvXYend[1, NCurve - 1], endAngle, XL, YL, SlopeAngle);
            if (Xpte != null)
            {
                double AZ0 = AZLine(Xpte[0], Xpte[1], X0, Y0);
                if (Math.Abs(CrvAzStart[0] - AZ0) < tol)
                {   // crossing in opposite direction of azimuth
                    // Get the location along alignment
                    PointLoce = LTotal + Math.Sqrt((Xpte[0] - X0) * (Xpte[0] - X0) + (Xpte[1] - Y0) * (Xpte[1] - Y0));
                }
                else
                {
                    Xpte = null;
                }
            }
            // Possible crossing is within defined alignment, check all curves
            for (int icrv = 0; icrv < NCurve; icrv++)
            {
                Xpt = null;
                double R0 = CrvRadius[icrv];
                double L0 = CrvLength[icrv];
                double Xend = CrvXYend[0, icrv];
                double Yend = CrvXYend[1, icrv];
                double Aend = A0 + CrvDelta[icrv];
                if (R0 == 0) // This segment is linear
                {
                    double[] Xpt1 = LineXLine(X0, Y0, A0, XL, YL, SlopeAngle);
                    if (Xpt1 != null)
                    {
                        if ((Math.Abs(Xpt1[0] - X0) <= Math.Abs(Xend - X0)) && (Math.Abs(Xpt1[0] - Y0) <= Math.Abs(Xend - X0)))
                        {// crossing is within the segment
                            XptA = new double[3];
                            XptA[0] = Xpt1[0]; XptA[1] = Xpt1[1];
                            XptA[2] = Math.Sqrt((Xpt1[0] - X0) * (Xpt1[0] - X0) + (Xpt1[1] - Y0) * (Xpt1[1] - Y0));
                            XptCur = icrv;
                        }
                    }
                }
                else
                {
                    double[] XptCross = LineXArc(XL, YL, SlopeAngle, X0, Y0, A0, R0, L0);
                    double Xo = X0 - R0 * Math.Sin(A0);  // X& Y of center of citrcle
                    double Yo = Y0 + R0 * Math.Cos(A0);
                    double XX0 = X0 - Xo;  // X&Y coordinates transformed to CS at center of circle
                    double YY0 = Y0 - Yo;
                    double XXL = XL - Xo;
                    double YYL = YL - Yo;
                    double[] Xpt1 = new double[2]; double[] Xpt2 = new double[2]; // crossing points relative to center of circle
                    // find crossing of line and circle
                    if (Math.Abs(SlopeAngle) == Math.PI / 2) // Vertical, crosses at top & bottom of circle
                    {
                        if (XXL < -R0 || XXL > R0) // no crossing
                        {
                            Xpt1 = null; Xpt2 = null;
                        }
                        else
                        {
                            double Slope = XXL / R0;
                            Xpt1[0] = XXL; Xpt1[1] = Math.Sqrt(R0 * R0 - XXL * XXL);
                            Xpt2[0] = XXL; Xpt2[1] = -Xpt1[1];
                        }
                    }
                    else
                    {
                        double SL = Math.Tan(SlopeAngle);
                        double AQ = 1 + SL * SL;
                        double BQ = 2 * SL * (YYL - SL * XXL);
                        double CQ = (YYL - (SL * XXL)) * (YYL - SL * XXL) - R0 * R0;
                        double B2_4AC = BQ * BQ - 4 * AQ * CQ;
                        if (B2_4AC < 0)  // no solution, does not cross circle
                        {
                            Xpt1 = null;
                            Xpt2 = null;
                        }
                        else
                        {
                            Xpt1[0] = (-BQ - Math.Sqrt(B2_4AC)) / (2 * AQ);
                            Xpt1[1] = YYL + SL * (Xpt1[0] - XXL);
                            Xpt2[0] = (-BQ + Math.Sqrt(B2_4AC)) / (2 * AQ);
                            Xpt2[1] = YYL + SL * (Xpt2[0] - XXL);
                        }
                    }
                    // If there was a crossing, see if either point is within limits of the curve
                    if (Xpt1 != null)
                    {
                        double Az1 = AZLine(0, 0, Xpt1[0], Xpt1[1]); // Azimuth from center of citrcle to first crossing
                        double Az2 = AZLine(0, 0, Xpt2[0], Xpt2[1]); // Azimuth from center of citrcle to second crossing
                        double AzStart = AZLine(0, 0, XX0, YY0);     // Azimuth from center of citrcle to start of curve
                        double AzEnd = AZLine(Xo, Yo, Xend, Yend); // Azimuth from center of citrcle to end of curve
                        if (R0 > 0) // Curve to left (CCW)
                        {
                            if (AzEnd > AzStart) // crossing over top, adjust AZEnd
                            {
                                if (Az1 <= AzStart || Az1 >= AzEnd) Xpt = Xpt1; // Crosses outside start and end, save it
                            }
                            else
                            {
                                if (Az1 <= AzStart && Az1 >= AzEnd) Xpt = Xpt1; // Crosses between start and end, save it
                            }
                            double[] Xpt2a = null;
                            if (AzEnd > AzStart) // crossing over top, adjust AZEnd
                            {
                                if (Az2 <= AzStart || Az2 >= AzEnd) Xpt2a = Xpt2; // Crosses between start and end, save it
                            }
                            else
                            {
                                if (Az2 <= AzStart && Az2 >= AzEnd) Xpt2a = Xpt2; // Crosses between start and end, save it
                            }
                            if (Xpt2a != null)
                            {
                                if (Xpt == null)
                                {
                                    Xpt = Xpt2a;
                                }
                                else
                                {
                                    double L1 = Math.Sqrt((Xpt1[0] - XXL) * (Xpt1[0] - XXL) + (Xpt1[1] - YYL) * (Xpt1[1] - YYL));
                                    double L2 = Math.Sqrt((Xpt2[0] - XXL) * (Xpt2[0] - XXL) + (Xpt2[1] - YYL) * (Xpt2[1] - YYL));
                                    if (L2 < L1) Xpt = Xpt2a;
                                }
                            }
                            if (Xpt != null)
                            {
                                // Get the distance in curve for Xpt
                                double AzCL = AZLine(0.0, 0.0, Xpt[0], Xpt[1]); // Azimuth from center to point
                                double RotAngle = AzStart - AzCL;  // Angle from start of curve to point
                                if (RotAngle < 0.0) RotAngle += Math.PI * 2;
                                double LCurve = R0 * RotAngle;
                                XptA = new double[3];
                                XptA[0] = Xpt[0]; XptA[1] = Xpt[1];
                                XptA[2] = LCurve;
                                XptCur = icrv;
                            }
                        }
                        else  // curve to the right (CW)
                        {
                            if (AzStart > AzEnd) // crossing over top, adjust AZEnd
                            {
                                if (Az1 >= AzStart || Az1 <= AzEnd) Xpt = Xpt1; // Crosses between start and end, save it
                            }
                            else
                            {
                                if (Az1 >= AzStart && Az1 <= AzEnd) Xpt = Xpt1; // Crosses between start and end, save it
                            }
                            double[] Xpt2a = null;
                            if (AzStart > AzEnd) // crossing over top, adjust AZEnd
                            {
                                if (Az2 >= AzStart || Az2 <= AzEnd) Xpt2a = Xpt2; // Crosses between start and end, save it
                            }
                            else
                            {
                                if (Az2 >= AzStart && Az2 <= AzEnd) Xpt2a = Xpt2; // Crosses between start and end, save it
                            }
                            if (Xpt2a != null) // Check shortest distance
                            {
                                if (Xpt == null)
                                {
                                    Xpt = Xpt2a;
                                }
                                else
                                {
                                    double L1 = Math.Sqrt((Xpt1[0] - XXL) * (Xpt1[0] - XXL) + (Xpt1[1] - YYL) * (Xpt1[1] - YYL));
                                    double L2 = Math.Sqrt((Xpt2[0] - XXL) * (Xpt2[0] - XXL) + (Xpt2[1] - YYL) * (Xpt2[1] - YYL));
                                    if (L2 < L1) Xpt = Xpt2;
                                }
                            }
                            if (Xpt != null)
                            {
                                // Get the distance in curve for Xpt
                                double AzCL = AZLine(0.0, 0.0, Xpt[0], Xpt[1]); // Azimuth from center to point
                                double RotAngle = AzCL - AzStart;  // Angle from start of curve to point
                                if (RotAngle < 0.0) RotAngle += Math.PI * 2;
                                double LCurve = R0 * RotAngle;
                                XptA = new double[3];
                                XptA[0] = Xpt[0]; XptA[1] = Xpt[1];
                                XptA[2] = LCurve;
                                XptCur = icrv;
                            }
                        }
                    }
                    if (XptA != null)
                    {
                        // return the point to original XY coord
                        XptA[0] += Xo;
                        XptA[1] += Yo;
                        // Check if Azimuth is in same direction or opposite
                        double AZoLine = Math.PI / 2 - SlopeAngle;
                        while (AZoLine < 0) AZoLine += 2 * Math.PI;
                        double AZXLine = AZLine(XL, YL, XptA[0], XptA[1]);
                        if (Math.Abs(AZoLine - AZXLine) > tol) XptA = null;
                    }
                } // end of checking circular segment
                if (XptA != null)
                {
                    NXpt += 1;
                    double Arrayelength = Xpts.GetLength(1);
                    if (Arrayelength < NXpt)  // Extend the array length
                    {
                        double[,] NewArray = new double[3, NXpt + 10];
                        int[] NewArrayC = new int[NXpt + 10];
                        for (int i = 0; i < Arrayelength; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                NewArray[j, i] = Xpts[j, i];
                            }
                            NewArrayC[i] = XptsCurv[i];
                        }
                        Xpts = NewArray;
                        XptsCurv = NewArrayC;
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        Xpts[j, NXpt - 1] = XptA[j];
                    }
                    XptsCurv[NXpt - 1] = XptCur;
                }
                A0 = Aend;  // Slope angle at start
                X0 = Xend;
                Y0 = Yend;
            }
            // Checked all curves, find shortest distance
            int PointIndex = 0;
            double Lmin = 1.0e+10;
            for (int i = 0; i < NXpt; i++)
            {
                double L = Math.Sqrt((Xpts[0, i] - XL) * (Xpts[0, i] - XL) + (Xpts[1, i] - YL) * (Xpts[1, i] - YL));
                if (L < Lmin) { Lmin = L; PointIndex = i; }
            }
            // Calculatebthe distance into alignment
            int jCurve = XptsCurv[PointIndex] - 1;

            for (int i = 0; i <= jCurve; i++)
            {
                PointLoc += CrvLength[i];
            }
            PointLoc += Xpts[2, PointIndex];
            // Check against crossing before start of alignment
            if (Xptb != null)
            {
                double L = Math.Sqrt((Xptb[0] - XL) * (Xptb[0] - XL) + (Xptb[1] - YL) * (Xptb[1] - YL));
                if (L < Lmin) { Lmin = L; PointLoc = PointLocb; }
            }
            // Check against crossing after end of alignment
            if (Xpte != null)
            {
                double L = Math.Sqrt((Xpte[0] - XL) * (Xpte[0] - XL) + (Xpte[1] - YL) * (Xpte[1] - YL));
                if (L < Lmin) { Lmin = L; PointLoc = PointLoce; }
            }
            return PointLoc;
        }
        /// <summary>
        /// Finds the intersection of a line and an arc of a circle
        /// </summary>
        /// <param name="X1">X of the start of the line</param>
        /// <param name="Y1">Y of the start of the line</param>
        /// <param name="A1">Angle of the line with X-axis</param>
        /// <param name="Xs0">X of the start of the arc</param>
        /// <param name="Ys0">Y of the strat of the arc</param>
        /// <param name="As0">Tangent angle of start of arc with X axis</param>
        /// <param name="R">Radius of arc, +ve=CCW</param>
        /// <param name="L">Length of arc</param>
        /// <returns>double[3]=X,Y,central angle into arc</returns>
        public double[] LineXArc(double X1, double Y1, double A1, double Xs0, double Ys0, double As0, double R, double L)
        {
            double[] Xpt = new double[3]; // Return point X, Y, Central Angle into arc
            double PIx2 = 2 * Math.PI;
            if (R < 0)
            {
                // Reverse the start and end of curve
                // Center of Arc circle
                double Ac = L / R;
                double Xc = Xs0 - R * Math.Sin(As0);
                double Yc = Ys0 + R * Math.Cos(As0);
                // end of Arc
                double Ae = As0 + Ac;
                double Xe = Xc + R * Math.Sin(Ae);
                double Ye = Yc - R * Math.Cos(Ae);
                Xpt = LineXArc(X1, Y1, A1, Xe, Ye, (Ae + Math.PI), -R, L);
                // Reverse the points back to original direction
                if(Xpt!=null) Xpt[2] = -L / R - Xpt[2];
            }
            else
            {
                double SA1 = Math.Sin(A1);
                double CA1 = Math.Cos(A1);
                //Start of Arc in coord system with origin at start of line an X axis along line
                double Xs = (Xs0 - X1) * CA1 + (Ys0 - Y1) * SA1;
                double Ys = -(Xs0 - X1) * SA1 + (Ys0 - Y1) * CA1;
                double As = As0 - A1;
                while (As < 0) As += PIx2;
                while (As >= PIx2) As -= PIx2;
                // Center of Arc circle
                double Ac = L / R;
                double Xc = Xs - R * Math.Sin(As);
                double Yc = Ys + R * Math.Cos(As);
                // end of Arc
                double Ae = As + Ac;
                double Xe = Xc + R * Math.Sin(Ae);
                double Ye = Yc - R * Math.Cos(Ae);
                // Check the X-axis crossings
                double b2 = R * R - Yc * Yc;
                if (b2 >= 0)
                {
                    bool[] IfPt = new bool[2]; IfPt[0] = false; IfPt[1] = false;
                    double b = Math.Sqrt(b2);
                    double Xx1 = Xc - b;
                    double Xx2 = Xc + b;
                    double Ax1 = Math.Atan2(-b, Yc);
                    if (Ax1 < 0) Ax1 += PIx2;
                    double Ax2 = Math.Atan2(b, Yc);
                    if (Ax2 < 0) Ax2 += PIx2;
                    if (Xx1 >= 0 && ((Ax1 >= As && Ax1 <= Ae) || (Ax1 + PIx2 >= As && Ax1 + PIx2 <= Ae))) IfPt[0] = true;
                    if (Xx2 >= 0 && ((Ax2 >= As && Ax2 <= Ae) || (Ax2 + PIx2 >= As && Ax2 + PIx2 <= Ae))) IfPt[1] = true;
                    if (IfPt[0]) // If point 1 is valid, it is the lower X distance
                    {
                        Xpt[0] = Xx1;
                        Xpt[2] = Ax1;
                    }
                    else if (IfPt[1])
                    {
                        Xpt[0] = Xx2;
                        Xpt[2] = Ax2;
                    }
                    else
                    {
                        Xpt = null;
                    }
                }
                else
                {
                    Xpt = null;
                }
                if (Xpt != null)  // If a valid point was selected, convert back to oriinal coord.
                {
                    double Xo = Xpt[0];
                    Xpt[0] = Xo * CA1 + X1;
                    Xpt[1] = Xo * SA1 + Y1;
                    Xpt[2] -= As;
                    while (Xpt[2] < 0) Xpt[2] += 2 * Math.PI;
                }
            }
            return Xpt;

        }
        /*************************************************************************************/
        /// <summary>
        /// 
        /// </summary>
        /// This function returns the crossing of two lines, null if parallel
        /// <param name="X1">X of a point on first line</param>
        /// <param name="Y1">Y of a point on first line</param>
        /// <param name="A1">Angle on first line with X axis</param>
        /// <param name="X2">X of a point on second line</param>
        /// <param name="Y2">Y of a point on second line</param>
        /// <param name="A2">Angle on second line with X axis</param>
        /// <returns></returns>
        public double[] LineXLine(double X1, double Y1, double A1, double X2, double Y2, double A2)
        {
            double tol = 1.0e-6;
            // Make the angle between 0, pi
            int npi = (int)(A1 / Math.PI);
            double AA1 = A1 - npi * Math.PI;
            npi = (int)(A2 / Math.PI);
            double AA2 = A2 - npi * Math.PI;
            //
            if (AA1 == AA2) return null;    // parallel lines
            double[] xyX = new double[2]; // x and y of the intersection
            if (AA1 == Math.PI / 2) // Line 1 is vertical
            {
                xyX[0] = X1;
                xyX[1] = Y2 + Math.Tan(AA2) * (X1 - X2);
            }
            else if (AA2 == Math.PI / 2) // Line 2 is vertical
            {
                xyX[0] = X2;
                xyX[1] = Y1 + Math.Tan(AA1) * (X2 - X1);
            }
            else
            {
                double S1 = Math.Tan(AA1); double S2 = Math.Tan(AA2);
                xyX[0] = ((Y2 - Y1) - (X2*S2 - X1*S1)) / (S1 - S2);
                xyX[1] = Y1 + Math.Tan(AA1) * (xyX[0] - X1);
            }
            double AZ = AZLine(X2, Y2, xyX[0], xyX[1]);
            double AZ1 = Math.PI/2 - A2;
            if (AZ1 < 0.0) AZ1 = 2 * Math.PI + AZ1;
            if (Math.Abs(AZ1 - AZ) > tol) xyX = null;
            return xyX;
        }

        /*************************************************************************************/
        /// <summary>
        /// Returns the Azimuth of a line going from X1,Y1 to X2,Y2
        /// </summary>
        /// <param name="X1">X of first point</param>
        /// <param name="Y1">Y of first point</param>
        /// <param name="X2">X of second point</param>
        /// <param name="Y2">Y of second point</param>
        /// <returns></returns>
        public double AZLine(double X1, double Y1, double X2, double Y2)
        {
            double pi = Math.PI;
            if (X1 == X2 && Y1 == Y2) return 0;
            double AZ=0;
            if (X1 == X2) // vertical
            {
                if (Y2 > Y1) AZ = 0;
                else AZ = pi;
            }
            else if (X2 > X1) // in first or fourth qudarant
            {
                if (Y2 == Y1) AZ = pi / 2;
                else AZ = pi / 2 - Math.Atan((Y2 - Y1) / (X2 - X1));
            }
            else  // in second or third quadrant
            {
                if (Y2 == Y1) AZ = 3 * pi / 2;
                else AZ = 3 * pi / 2 - Math.Atan((Y2 - Y1) / (X2 - X1));
            }
            return AZ;
        }

        /// <summary>
        /// Information for vertical curve to augment the hoirizontal alignment
        /// </summary>
        /// <param name="VSlopeStart">Slope of eth starting point</param>
        /// <param name="VCrvRun">Run along alignment</param>
        /// <param name="VCrvRise">rise of alignment (Z)</param>
        public void SetVerticalCurve(double VSlopeStart, double[] VCrvRun, double[] VCrvRise)
        {
            m_VSlopeStart = VSlopeStart;
            m_VCrvRun = MatrixOperations.Duplicate(VCrvRun);
            m_VCrvRise = MatrixOperations.Duplicate(VCrvRise);
        }

        /// <summary>
        /// Calculate the X, Y, Z of a point along an alignment
        /// </summary>
        /// <param name="XAlong">Distance along alignment</param>
        /// <returns>[3] XYZ</returns>
        public double[] GetXYZ(double XAlong)
        {
            double[] XYZ = new double[3];
            double[] XY = GetXY(XAlong); XYZ[0] = XY[0]; XYZ[1] = XY[1];
            double[,] abc;  //coeffeicients for y=a*(x-x0)^2+b(x-x0)+c for all vertical curves
            int nVCrv = 0;
            if (m_VCrvRun != null)
            {
                nVCrv = m_VCrvRun.Length;
                abc = new double[nVCrv, 3];
            }
            double Z0 = XYZ0[2]; //Z of start
            double b = m_VSlopeStart; //staring slope (Z/X)
            double x0 = XYZ0[0];
            if (XAlong > x0) // For points before start of curve use the start elevation
            {
                for (int i = 0; i < nVCrv; i++)
                {
                    if (m_VCrvRun[i] > 0) // skip a curve wwith 0 or negative length
                    {
                        double a = (m_VCrvRise[i] - b * m_VCrvRun[i]) / (m_VCrvRun[i] * m_VCrvRun[i]);
                        double c = Z0;
                        if (XAlong <= x0 + m_VCrvRun[i]) // on the right curve, calculate Z
                        {
                            Z0 = a * (XAlong - x0) * (XAlong - x0) + b * (XAlong - x0) + c;
                            break;
                        }
                        else
                        {
                            b += 2 * a * (m_VCrvRun[i]);  // End slope = start slope of next curve
                            Z0 += m_VCrvRise[i]; // End Z = start Z of next curve
                            x0 += m_VCrvRun[i]; // End X = start X of next curve
                        }
                    }
                }
            }
            XYZ[2] = Z0;
            return XYZ;
        }


    }//end class
}//end namespace