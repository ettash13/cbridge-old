﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CBridge
{
    public partial class CBridge : Form
    {
        StructureModel m_Structure;
                
        // Graphic
        Bitmap m_DrawArea;
        Graphics m_gModel;

        Kanvas3D m_MainView;
        
        public CBridge()
        {
            InitializeComponent();

            m_Structure = new StructureModel();

            SetDefaultStructure();

            m_DrawArea = new Bitmap(pbModel.Size.Width, pbModel.Size.Height);
            m_gModel = Graphics.FromImage(m_DrawArea);
            Graphics g = m_gModel;
            m_MainView = new Kanvas3D();
            Pen Redpen = new Pen(Color.Red);
            Pen Bluepen = new Pen(Color.Blue);
            m_MainView.SetWinSize(pbModel.Size.Width, pbModel.Size.Height);
            m_MainView.AddPen(Redpen); //1
            m_MainView.AddPen(Bluepen); //2
            DrawLine();
        }

        private void CBridge_Load(object sender, EventArgs e)
        {

        }
        
        void SetDefaultStructure()
        {

            ///////////////////////////////////////////////////////
            //
            // Set default Material
            //
            ///////////////////////////////////////////////////////
            double[] MdataC1 = { 2.0, 1, 2.2, 2.3, 2.4 };
            m_Structure.MaterialAddRevise("Concrete", "C1", MdataC1);
            m_Structure.MaterialAddRevise("Concrete", "C2", MdataC1);
            m_Structure.MaterialAddRevise("Concrete", "C3", MdataC1);
            m_Structure.MaterialAddRevise("Concrete", "C4", MdataC1);
            m_Structure.MaterialAddRevise("Concrete", "C5", MdataC1);
            m_Structure.MaterialAddRevise("Concrete", "C6", MdataC1);


            double[] MdataS1 = { 3.0, 1, 3.2, 3.3, 3.4, 3.5, 3.6 };
            m_Structure.MaterialAddRevise("MildSteel", "ST1", MdataS1);
            m_Structure.MaterialAddRevise("MildSteel", "ST2", MdataS1);
            m_Structure.MaterialAddRevise("MildSteel", "ST3", MdataS1);
            m_Structure.MaterialAddRevise("MildSteel", "ST4", MdataS1);
            m_Structure.MaterialAddRevise("MildSteel", "ST5", MdataS1);
            m_Structure.MaterialAddRevise("MildSteel", "ST6", MdataS1);
            

            double[] MdataS2 = { 4.0, 1, 4.2, 4.3, 4.4, 4.5, 4.6 };
            m_Structure.MaterialAddRevise("Strand", "STR1", MdataS2);
            m_Structure.MaterialAddRevise("Strand", "STR2", MdataS2);
            m_Structure.MaterialAddRevise("Strand", "STR3", MdataS2);
            m_Structure.MaterialAddRevise("Strand", "STR4", MdataS2);
            m_Structure.MaterialAddRevise("Strand", "STR5", MdataS2);
            m_Structure.MaterialAddRevise("Strand", "STR6", MdataS2);
            

            double[] MdataE1 = { 1.0, 1, 1.2 };
            m_Structure.MaterialAddRevise("Elastic", "E1", MdataE1);
            m_Structure.MaterialAddRevise("Elastic", "E2", MdataE1);
            m_Structure.MaterialAddRevise("Elastic", "E3", MdataE1);
            m_Structure.MaterialAddRevise("Elastic", "E4", MdataE1);
            m_Structure.MaterialAddRevise("Elastic", "E5", MdataE1);
            m_Structure.MaterialAddRevise("Elastic", "E6", MdataE1);

            ///////////////////////////////////////////////////////
            //
            // Set default Alignment
            //
            ///////////////////////////////////////////////////////
            double[] XYZ0 = { 10, 20, 30 };
            double AzimuthStart = Math.PI/2.0;  // Radian
            double SlopeStart = 10;  // 1 pi

            double[] CRVRad = { 0, -300, 0, 90, 0 };
            double[] CRVLen = { 150, 600, 800, 180, 700 };
            
            m_Structure.AlignmentAdd("Alignment1", XYZ0, AzimuthStart, CRVRad, CRVLen);

            double[] CrvRun = { 200, 200, 400, 400, 500 };
            double[] CrvRise = { 20, -30, 40, 50, 60 };
            
            m_Structure.AlignmentSetVerticalCurve(SlopeStart, CrvRun, CrvRise);

            ///////////////////////////////////////////////////////
            //
            // Set default Sections
            //
            ///////////////////////////////////////////////////////

            double[] SdataC1 = { 2.0, 1, 2.2, 2.3, 2.4, 2.4, 2.5 };
            m_Structure.SectionAddRevise("SlabOnGrider", "SLOG1", SdataC1);
            m_Structure.SectionAddRevise("SlabOnGrider", "SLOG1", SdataC1);

            m_Structure.SectionAddRevise("BoxGrider, ", "BOXG1", SdataC1);
            m_Structure.SectionAddRevise("BoxGrider, ", "BOXG2", SdataC1);

            m_Structure.SectionAddRevise("SolidSlab", "SOLSL1", SdataC1);
            m_Structure.SectionAddRevise("SolidSlab", "SOLSL2", SdataC1);

        }


        private bool[] MaterialInUse(List<Material> MaterialList)
        {
            bool [] InUse = new bool[MaterialList.Count];

            int i = 0;

            foreach (Material item in MaterialList)
            {
                InUse[i] = item.InUse;
                i++;
            }

            return InUse;
        }

        // we are saving the Data grid view to the Structure model
        private bool MaterialConcreteUpdate(List<MaterialConcrete> NewMatList, string[,] RenameList)
        {
            bool RetCode = true;
            try
            {
                if (NewMatList != null)
                {
                    // we are saving new list, remove everything first
                    foreach (MaterialConcrete item in m_Structure.MaterialConcreteList)
                    {
                        m_Structure.MaterialRemove("Concrete", item.Name);
                    }
                    
                    foreach (MaterialConcrete item in NewMatList)
                    {
                        m_Structure.MaterialAddRevise("Concrete", item.Name, item.Prop);
                    }
                }
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }

        // we are saving the Data grid view to the Structure model
        private bool MaterialMildSteelUpdate(List<MaterialMildSteel> NewMatList, string[,] RenameList)
        {
            bool RetCode = true;
            try
            {
                if (NewMatList != null)
                {
                    // we are saving new list, remove everything first
                    foreach (MaterialMildSteel item in m_Structure.MaterialMildSteelList)
                    {
                        m_Structure.MaterialRemove("MildSteel", item.Name);
                    }

                    foreach (MaterialMildSteel item in NewMatList)
                    {
                        m_Structure.MaterialAddRevise("MildSteel", item.Name, item.Prop);
                    }
                }
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }

        // we are saving the Data grid view to the Structure model
        private bool MaterialStrandUpdate(List<MaterialStrand> NewMatList, string[,] RenameList)
        {
            bool RetCode = true;
            try
            {
                if (NewMatList != null)
                {
                    // we are saving new list, remove everything first
                    foreach (MaterialStrand item in m_Structure.MaterialStrandList)
                    {
                        m_Structure.MaterialRemove("Strand", item.Name);
                    }
                    
                    foreach (MaterialStrand item in NewMatList)
                    {
                        m_Structure.MaterialAddRevise("Strand", item.Name, item.Prop);
                    }
                }
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }

        // we are saving the Data grid view to the Structure model
        private bool MaterialElasticUpdate(List<MaterialElastic> NewMatList, string[,] RenameList)
        {
            bool RetCode = true;
            try
            {
                if (NewMatList != null)
                {
                    // we are saving new list, remove everything first
                    foreach (MaterialElastic item in m_Structure.MaterialElasticList)
                    {
                        m_Structure.MaterialRemove("Elastic", item.Name);
                    }

                    foreach (MaterialElastic item in NewMatList)
                    {
                        m_Structure.MaterialAddRevise("Elastic", item.Name, item.Prop);
                    }
                }
            }
            catch
            {
                RetCode = false;
            }
            return RetCode;
        }

        // --------------------------------------------------------------------
        // Graphic Functions
        // --------------------------------------------------------------------

        private void UpdateModelView()
        {

        }

        private void DrawLine()
        {
            Bitmap DrawArea;
            Graphics g;
            
            DrawArea = new Bitmap(pbModel.Size.Width, pbModel.Size.Height);
            g = Graphics.FromImage(DrawArea);

            Pen mypen = new Pen(Color.Red);
            g.DrawLine(mypen, 0, 0, 200, 150);
            Pen mypenG = new Pen(Color.Green, 4);
            
            g.DrawLine(mypenG, 200, 150, 200, 250);
            g.DrawString("This is a diagonal line drawn on the control",  new Font("Courier", 18), System.Drawing.Brushes.Black, new Point(30, 30));
            
            Brush Mybrush = Brushes.Red;
            
            g.DrawString("Second text", new Font("Courier", 8), Mybrush, new Point(30, 50));
            g.DrawLine(mypenG, 30, 50, 200, 50);

            pbModel.Image = DrawArea;

            g.Dispose();
        }

        // --------------------------------------------------------------------
        // Event Functions
        // --------------------------------------------------------------------
        private void openModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog theDialog = new OpenFileDialog();

            theDialog.Filter = "Text files(*.txt)|*.txt|csv files(*.csv)|*.csv|All files(*.*)|*.*";
            theDialog.FileName = "";

            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                string FName = theDialog.FileName;
                try
                {
                    string FData;
                    using (StreamReader sr = new StreamReader(FName))
                    {
                        FData = sr.ReadToEnd();
                    }

                    InputText Data = new InputText(FData);

                    m_Structure = Data.GetStructure();

                    UpdateModelView();
                }
                catch
                {
                    MessageBox.Show("File Could not be Openned");
                }
            }
        }

        private void materialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMaterials fMaterial = new frmMaterials();
                        
            List<MaterialConcrete> thisMaterialConcrete   = m_Structure.MaterialConcreteList;
            List<MaterialMildSteel> thisMaterialMildSteel = m_Structure.MaterialMildSteelList;
            List<MaterialStrand> thisMaterialStrand       = m_Structure.MaterialStrandList;
            List<MaterialElastic> thisMaterialElastic     = m_Structure.MaterialElasticList;

            bool[] InUseConcrete = { false, false };
         
            fMaterial.SetValues(thisMaterialConcrete, thisMaterialMildSteel, thisMaterialStrand, thisMaterialElastic, InUseConcrete);
            
            
            // ShowDialog()  Shows the Dialog and will not go to next line until Dialog is closed with this.close              
            fMaterial.ShowDialog();

            if (fMaterial.m_OK)
            {
                MaterialConcreteUpdate(fMaterial.m_MaterialConcreteListNew, fMaterial.m_RenameListConcrete);
                MaterialMildSteelUpdate(fMaterial.m_MaterialMildSteelListNew, fMaterial.m_RenameListMildSteel);
                MaterialStrandUpdate(fMaterial.m_MaterialStrandListNew, fMaterial.m_RenameListStrand);
                MaterialElasticUpdate(fMaterial.m_MaterialElasticListNew, fMaterial.m_RenameListElastic);
            }
        }
        

        private void bAlignment_Click(object sender, EventArgs e)
        {
            
            frmAlignment fAlignment = new frmAlignment();

            Alignment thisAlignment = new Alignment(m_Structure.Alignment);
                      
            fAlignment.SetValues(thisAlignment);

            fAlignment.ShowDialog();

            if (fAlignment.m_OK) m_Structure.Alignment = fAlignment.m_Alignment;      
        }

        private void button1_Click(object sender, EventArgs e)
        {
            materialToolStripMenuItem_Click(sender, e);
        }

        /////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  New Section Clicked : this is just a dialog for entering Section 
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// //////////////////////////////////////////////////////////////////////////////
            
        private void bSection_Click(object sender, EventArgs e)
        {

            frmSectionNew fSectionNew = new frmSectionNew();

            fSectionNew.ShowDialog();
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Exit without Saving? ", "Some Title", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                
                this.Close();

            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
            
        }
    }
}

