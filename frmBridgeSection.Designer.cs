﻿namespace CBridge
{
    partial class frmBridgeSection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lType = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dgvGriders = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxLeftCurb = new System.Windows.Forms.TextBox();
            this.textBoxSlabThickness = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSlabWidth = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxRightCurb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnReplicate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.cmbSectionName = new System.Windows.Forms.ComboBox();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBoxSectionType = new System.Windows.Forms.TextBox();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.GName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGriders)).BeginInit();
            this.SuspendLayout();
            // 
            // lType
            // 
            this.lType.AutoSize = true;
            this.lType.Location = new System.Drawing.Point(17, 49);
            this.lType.Name = "lType";
            this.lType.Size = new System.Drawing.Size(122, 13);
            this.lType.TabIndex = 1;
            this.lType.Text = "Bridge Section Type";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.pictureBox1.Location = new System.Drawing.Point(332, 45);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(722, 486);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // dgvGriders
            // 
            this.dgvGriders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGriders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGriders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GName,
            this.Distance});
            this.dgvGriders.Location = new System.Drawing.Point(20, 252);
            this.dgvGriders.Name = "dgvGriders";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGriders.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvGriders.Size = new System.Drawing.Size(280, 279);
            this.dgvGriders.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Left Curb:";
            // 
            // textBoxLeftCurb
            // 
            this.textBoxLeftCurb.Location = new System.Drawing.Point(160, 80);
            this.textBoxLeftCurb.Name = "textBoxLeftCurb";
            this.textBoxLeftCurb.Size = new System.Drawing.Size(140, 20);
            this.textBoxLeftCurb.TabIndex = 6;
            // 
            // textBoxSlabThickness
            // 
            this.textBoxSlabThickness.Location = new System.Drawing.Point(160, 150);
            this.textBoxSlabThickness.Name = "textBoxSlabThickness";
            this.textBoxSlabThickness.Size = new System.Drawing.Size(140, 20);
            this.textBoxSlabThickness.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 188);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Slab Width";
            // 
            // textBoxSlabWidth
            // 
            this.textBoxSlabWidth.Location = new System.Drawing.Point(160, 185);
            this.textBoxSlabWidth.Name = "textBoxSlabWidth";
            this.textBoxSlabWidth.Size = new System.Drawing.Size(140, 20);
            this.textBoxSlabWidth.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Slab Thickness";
            // 
            // textBoxRightCurb
            // 
            this.textBoxRightCurb.Location = new System.Drawing.Point(160, 115);
            this.textBoxRightCurb.Name = "textBoxRightCurb";
            this.textBoxRightCurb.Size = new System.Drawing.Size(140, 20);
            this.textBoxRightCurb.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Right Curb:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Bridge Section Name";
            // 
            // btnReplicate
            // 
            this.btnReplicate.Location = new System.Drawing.Point(43, 552);
            this.btnReplicate.Name = "btnReplicate";
            this.btnReplicate.Size = new System.Drawing.Size(87, 23);
            this.btnReplicate.TabIndex = 14;
            this.btnReplicate.Text = "Replicate";
            this.btnReplicate.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(171, 552);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(87, 23);
            this.btnDelete.TabIndex = 15;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // cmbSectionName
            // 
            this.cmbSectionName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSectionName.ForeColor = System.Drawing.Color.DarkRed;
            this.cmbSectionName.FormattingEnabled = true;
            this.cmbSectionName.Location = new System.Drawing.Point(160, 9);
            this.cmbSectionName.Name = "cmbSectionName";
            this.cmbSectionName.Size = new System.Drawing.Size(140, 21);
            this.cmbSectionName.TabIndex = 16;
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(376, 552);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(176, 23);
            this.btnNew.TabIndex = 17;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(831, 552);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(87, 23);
            this.btnOK.TabIndex = 18;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(946, 552);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 23);
            this.btnCancel.TabIndex = 19;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Girder Layout";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(332, 16);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(63, 17);
            this.checkBox1.TabIndex = 21;
            this.checkBox1.Text = "In Use";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // textBoxSectionType
            // 
            this.textBoxSectionType.Enabled = false;
            this.textBoxSectionType.Location = new System.Drawing.Point(160, 45);
            this.textBoxSectionType.Name = "textBoxSectionType";
            this.textBoxSectionType.Size = new System.Drawing.Size(140, 20);
            this.textBoxSectionType.TabIndex = 22;
            // 
            // cmbUnit
            // 
            this.cmbUnit.ForeColor = System.Drawing.Color.DarkRed;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Items.AddRange(new object[] {
            "in",
            "ft"});
            this.cmbUnit.Location = new System.Drawing.Point(716, 552);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(91, 21);
            this.cmbUnit.TabIndex = 23;
            this.cmbUnit.Text = "in";
            // 
            // GName
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GName.DefaultCellStyle = dataGridViewCellStyle1;
            this.GName.HeaderText = "Grider Name";
            this.GName.Name = "GName";
            this.GName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Distance
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Distance.DefaultCellStyle = dataGridViewCellStyle2;
            this.Distance.HeaderText = "Distance";
            this.Distance.Name = "Distance";
            // 
            // frmBridgeSection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 595);
            this.Controls.Add(this.cmbUnit);
            this.Controls.Add(this.textBoxSectionType);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.cmbSectionName);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnReplicate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxRightCurb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxSlabWidth);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxSlabThickness);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxLeftCurb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvGriders);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lType);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkRed;
            this.Name = "frmBridgeSection";
            this.Text = "frmBridgeSection";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGriders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lType;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dgvGriders;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLeftCurb;
        private System.Windows.Forms.TextBox textBoxSlabThickness;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSlabWidth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxRightCurb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnReplicate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cmbSectionName;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textBoxSectionType;
        private System.Windows.Forms.ComboBox cmbUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn GName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Distance;
    }
}