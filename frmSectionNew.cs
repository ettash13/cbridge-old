﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CBridge
{
    public partial class frmSectionNew : Form
    {
        bool DoEvents = false; // a flag for turning event handling off and on selectively

        public string m_SectionName = "";
        public string m_SectionType = "";
        public string m_SectionShape = "";

        public string m_GriderName = "";
        public int m_NumGriders = 0;
        public int m_GriderSpacing = 0;

        public int m_Unit = 0;         // default 0=ft, 1=inch
        
        public bool m_OK = false; // false=return from cancel, true from OK


        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Constructor of New Section Form 
        /// 
        /// </summary>
        /// ///////////////////////////////////////////////////////////////////////////////////////////
        public frmSectionNew()
        {
            DoEvents = false;
            InitializeComponent();

            SetValues();
            
            DoEvents = true; // a flag for turning event handling off and on selectively
                
        }

        // --------------------------------------------------------------------
        // User Functions
        // --------------------------------------------------------------------

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves parameter valuess into the view
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void SetValues()
        {
            InitializeDialog();
            
        }
        
        /// <summary>
        /// Moves parameter valuess into dialog items
        /// </summary>
        public void InitializeDialog()
        {
            DoEvents = false;

            cmbSectionType.SelectedIndex = 0; 
            
            cmbSectionShape.SelectedIndex = 0;

            cmbUnit.SelectedIndex = 0;

            DoEvents = true;
        }


        private void CheckValues()
        {
            if ((m_SectionName != null) && (m_GriderName != null) && (m_NumGriders > 0) && (m_GriderSpacing > 0))
            {
                btn_OK.Enabled = true;
                m_OK = true;
            }
            else
            {
                btn_OK.Enabled = false;
                m_OK = false;
            }
        }

        private void textBoxSectionName_TextChanged(object sender, EventArgs e)
        {
            m_SectionName = textBoxSectionName.Text;
            CheckValues();
        }

        private void cmbSectionType_TextChanged(object sender, EventArgs e)
        {
         
            m_SectionType = cmbSectionType.Text;
                        
            CheckValues();
        }

        private void cmbSectionShape_TextChanged(object sender, EventArgs e)
        {
            m_SectionShape = cmbSectionShape.Text;
            CheckValues();
        }

        private void textBoxGriderName_TextChanged(object sender, EventArgs e)
        {
            m_GriderName = textBoxGriderName.Text;
            CheckValues();
        }
        
        private void textBoxNumGriders_TextChanged(object sender, EventArgs e)
        {
            m_NumGriders = Convert.ToInt32(textBoxNumGriders.Text);
            CheckValues();
        }

        private void textBoxGriderSpacing_TextChanged(object sender, EventArgs e)
        {
            m_GriderSpacing = Convert.ToInt32(textBoxGriderSpacing.Text);
            CheckValues();
        }
        
        private void cmbUnit_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
                        
        
        /// <summary>
        /// 
        ///  Finally the Press OK
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btn_OK_Click(object sender, EventArgs e)
        {
            if (m_OK)
            {
                frmBridgeSection fSection = new frmBridgeSection();

                fSection.SetValues(m_SectionName, m_SectionType, m_GriderName, m_NumGriders, m_GriderSpacing, m_Unit);
                fSection.ShowDialog();

            }
        }

        //
        //   Cancel ? no problem
        //
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }



    }
}
