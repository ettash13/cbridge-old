﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBridge
{
    public class StructureModel
    {
        private string m_Name;  // Name of Structure

        private double[] m_GravityFactor = { 0, 0, -1 };        // Gravity multiplier factors, default is -Z
        
        // The Materials for the Bridge
        private List<MaterialConcrete> m_MaterialConcreteList = new List<MaterialConcrete>(); 
        private List<MaterialMildSteel> m_MaterialMildSteelList = new List<MaterialMildSteel>();
        private List<MaterialStrand> m_MaterialStrandList = new List<MaterialStrand>();
        private List<MaterialElastic> m_MaterialElasticList = new List<MaterialElastic>();
        
        // The Alignment for the Bridge
        private Alignment m_Alignment = new Alignment();

        // The Sections for the Bridge
        private List<SectionSlabOnGirder> m_SectionSlabOnGirderList = new List<SectionSlabOnGirder>();
        private List<SectionBoxGirder> m_SectionBoxGirderList = new List<SectionBoxGirder>();
        private List<SectionSolidSlab> m_SectionSolidSlabList = new List<SectionSolidSlab>();

        /// <summary>
        /// get/Set for parameters
        /// </summary>
        public string Name
        {
            get
            {
                return m_Name;
            }
            set
            {
                m_Name = value;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        //
        // StructureModel public functions
        //
        ///////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Default Constructor
        /// </summary>
        public StructureModel(string Name = "Structure1")
        {
            m_Name = Name;
        }

        /// <summary>
        /// Returns a complete duplicate (deep-copy) of the object
        /// </summary>
        /// <returns>A duplicate of this object</returns>
        public StructureModel Duplicate()
        {
            StructureModel newModel = null;
            try
            {
                newModel = new StructureModel();

                newModel.m_Name = m_Name;

                if (m_MaterialConcreteList != null)
                {
                    newModel.m_MaterialConcreteList = new List<MaterialConcrete>(m_MaterialConcreteList.Count);

                    foreach (MaterialConcrete item in m_MaterialConcreteList) newModel.m_MaterialConcreteList.Add(item);
                }

                if (m_MaterialMildSteelList != null)
                {
                    newModel.m_MaterialMildSteelList = new List<MaterialMildSteel>(m_MaterialMildSteelList.Count);

                    foreach (MaterialMildSteel item in m_MaterialMildSteelList) newModel.m_MaterialMildSteelList.Add(item);
                }

                if (m_MaterialStrandList != null)
                {
                    newModel.m_MaterialStrandList = new List<MaterialStrand>(m_MaterialStrandList.Count);

                    foreach (MaterialStrand item in m_MaterialStrandList) newModel.m_MaterialStrandList.Add(item);
                }

                if (m_MaterialElasticList != null)
                {
                    newModel.m_MaterialElasticList = new List<MaterialElastic>(m_MaterialElasticList.Count);

                    foreach (MaterialElastic item in m_MaterialElasticList) newModel.m_MaterialElasticList.Add(item);
                }

            }
            catch
            {

            }

            return newModel;
        }

        
        ///////////////////////////////////////////////////////////////////////////////////////////
        //
        // Material public functions  
        //
        ///////////////////////////////////////////////////////////////////////////////////////////
        public string[] MaterialConcreteNameList
        {
            get
            {
                if (m_MaterialConcreteList == null) return null;

                string[] Names = new string[m_MaterialConcreteList.Count];
                
                int i = 0;

                foreach (MaterialConcrete item in m_MaterialConcreteList)
                {
                    Names[i++] = item.Name;
                }

                return Names;
            }
        }

        public List<MaterialConcrete> MaterialConcreteList
        {
            get
            {
                if (m_MaterialConcreteList == null) return null;

                List<MaterialConcrete> Materials = new List<MaterialConcrete>();

                foreach (MaterialConcrete item in m_MaterialConcreteList)
                {
                    Materials.Add(item);
                }

                return Materials;
            }
        }

        public string[] MaterialMildSteelNameList
        {
            get
            {
                if (m_MaterialMildSteelList == null) return null;

                string[] Names = new string[m_MaterialMildSteelList.Count];

                int i = 0;

                foreach (MaterialMildSteel item in m_MaterialMildSteelList)
                {
                    Names[i++] = item.Name;
                }

                return Names;
            }
        }

        public List<MaterialMildSteel> MaterialMildSteelList
        {
            get
            {
                if (m_MaterialMildSteelList == null) return null;

                List<MaterialMildSteel> Materials = new List<MaterialMildSteel>();

                foreach (MaterialMildSteel item in m_MaterialMildSteelList)
                {
                    Materials.Add(item);
                }

                return Materials;
            }
        }
        
        public string[] MaterialStrandNameList
        {
            get
            {
                if (m_MaterialStrandList == null) return null;

                string[] Names = new string[m_MaterialStrandList.Count];

                int i = 0;

                foreach (MaterialStrand item in m_MaterialStrandList)
                {
                    Names[i++] = item.Name;
                }

                return Names;
            }
        }

        public List<MaterialStrand> MaterialStrandList
        {
            get
            {
                if (m_MaterialStrandList == null) return null;

                List<MaterialStrand> Materials = new List<MaterialStrand>();

                foreach (MaterialStrand item in m_MaterialStrandList)
                {
                    Materials.Add(item);
                }

                return Materials;
            }
        }

        public string[] MaterialElasticNameList
        {
            get
            {
                if (m_MaterialElasticList == null) return null;

                string[] Names = new string[m_MaterialElasticList.Count];

                int i = 0;

                foreach (MaterialElastic item in m_MaterialElasticList)
                {
                    Names[i++] = item.Name;
                }

                return Names;
            }
        }

        public List<MaterialElastic> MaterialElasticList
        {
            get
            {
                if (m_MaterialElasticList == null) return null;

                List<MaterialElastic> Materials = new List<MaterialElastic>();

                foreach (MaterialElastic item in m_MaterialElasticList)
                {
                    Materials.Add(item);
                }

                return Materials;
            }
        }


        /// <summary>
        /// Adds a material to the list of nodes or replaces an exsiting one
        /// </summary>
        /// <param name="Name">material to added or replaced</param>
        /// <param name="Sprop">E, nu, Alpha</param>
        /// <returns>true=Ok, false=failed</returns>

        public bool MaterialAddRevise(string MaterialType, string Name, double[] Sprop)
        {
            bool RetCode = false; // Not Succerssful
            
            switch (MaterialType)
            {
                case "Concrete":

                    // Check if we have this name in the list 
                    if (m_MaterialConcreteList.Exists(x => x.Name == Name))
                    {
                        // Relace the item, we need to find the index of the item first
                        int index = m_MaterialConcreteList.IndexOf(m_MaterialConcreteList.Single(x => x.Name == Name));

                        m_MaterialConcreteList.RemoveAt(index);
                        m_MaterialConcreteList.Insert(index, (new MaterialConcrete(Name, Sprop)));

                        RetCode = true;

                    }
                    else   // no, Add the item
                    {
                        m_MaterialConcreteList.Add(new MaterialConcrete(Name, Sprop));

                        RetCode = true;
                    }

                    break;

                case "MildSteel":

                    // Check if we have this name in the list 
                    if (m_MaterialMildSteelList.Exists(x => x.Name == Name))
                    {
                        // Relace the item, we need to find the index of the item first
                        int index = m_MaterialMildSteelList.IndexOf(m_MaterialMildSteelList.Single(x => x.Name == Name));

                        m_MaterialMildSteelList.RemoveAt(index);
                        m_MaterialMildSteelList.Insert(index, (new MaterialMildSteel(Name, Sprop)));

                        RetCode = true;

                    }
                    else   // no, Add the item
                    {
                        m_MaterialMildSteelList.Add(new MaterialMildSteel(Name, Sprop));

                        RetCode = true;
                    }

                    break;

                case "Strand":
                                        
                    // Check if we have this name in the list 
                    if (m_MaterialStrandList.Exists(x => x.Name == Name))
                    {
                        // Relace the item, we need to find the index of the item first
                        int index = m_MaterialStrandList.IndexOf(m_MaterialStrandList.Single(x => x.Name == Name));

                        m_MaterialStrandList.RemoveAt(index);
                        m_MaterialStrandList.Insert(index, (new MaterialStrand(Name, Sprop)));

                        RetCode = true;

                    }
                    else   // no, Add the item
                    {
                        m_MaterialStrandList.Add(new MaterialStrand(Name, Sprop));

                        RetCode = true;
                    }

                    break;

                    
                case "Elastic":
                                        
                    // Check if we have this name in the list 
                    if (m_MaterialElasticList.Exists(x => x.Name == Name))
                    {
                        // Relace the item, we need to find the index of the item first
                        int index = m_MaterialElasticList.IndexOf(m_MaterialElasticList.Single(x => x.Name == Name));

                        m_MaterialElasticList.RemoveAt(index);
                        m_MaterialElasticList.Insert(index, (new MaterialElastic(Name, Sprop)));

                        RetCode = true;

                    }
                    else   // no, Add the item
                    {
                        m_MaterialElasticList.Add(new MaterialElastic(Name, Sprop));

                        RetCode = true;
                    }

                    break;

            }

            return RetCode;
        }

        /// <summary>
        /// Remove a material from the list
        /// </summary>
        /// <param name="Name">name of material to be removed</param>
        /// <returns>false=not found, true=Removed</returns>

        public bool MaterialRemove(string MaterialType, string Name)
        {
            bool RetCode = false; // Not Succerssful

            switch (MaterialType)
            {
                case "Concrete":
                                        
                    if (m_MaterialConcreteList.Exists(x => x.Name == Name))
                    {
                        int index = m_MaterialConcreteList.IndexOf(m_MaterialConcreteList.Single(x => x.Name == Name));
                        m_MaterialConcreteList.RemoveAt(index);

                        RetCode = true;
                    }
                    break;

                case "MildSteel":
                    
                    // Check if we have this name in the list 
                    if (m_MaterialMildSteelList.Exists(x => x.Name == Name))
                    {
                        int index = m_MaterialMildSteelList.IndexOf(m_MaterialMildSteelList.Single(x => x.Name == Name));
                        m_MaterialMildSteelList.RemoveAt(index);

                        RetCode = true;
                    }
                    break;

                case "Strand":

                    // Check if we have this name in the list 
                    if (m_MaterialStrandList.Exists(x => x.Name == Name))
                    {
                        int index = m_MaterialStrandList.IndexOf(m_MaterialStrandList.Single(x => x.Name == Name));
                        m_MaterialStrandList.RemoveAt(index);

                        RetCode = true;
                    }
                    break;

                case "Elastic":

                    // Check if we have this name in the list
                    if (m_MaterialElasticList.Exists(x => x.Name == Name))
                    {
                        int index = m_MaterialElasticList.IndexOf(m_MaterialElasticList.Single(x => x.Name == Name));
                        m_MaterialElasticList.RemoveAt(index);

                        RetCode = true;
                    }
                    break;                    
            }

            return RetCode;
        }
   
        ///////////////////////////////////////////////////////////////////////////////////////////
        //
        // Alignment public functions  
        //
        ///////////////////////////////////////////////////////////////////////////////////////////
        public Alignment Alignment
        {
            get
            {
                    return m_Alignment;
            }
            set
            {
                m_Alignment = value;
            }
        }


        /// <summary>
        /// Adds Alignment to structure model
        /// </summary>
        /// <param name="Name">Alignment name </param>
        /// <param name="Sprop">E, nu, Alpha</param>
        /// <returns>true=Ok, false=failed</returns>

        public bool AlignmentAdd(string name, double[] XYZStart, double AzStart, double[] CrvRadius, double[] CrvLenght)
        {
            bool RetCode = false; // Not Succerssful

            m_Alignment = new Alignment(name, XYZStart, AzStart, CrvRadius, CrvLenght);

            if (m_Alignment != null)
                return true;
            else
                return false;

        }


        /// <summary>
        /// Information for vertical curve to augment the hoirizontal alignment
        /// </summary>
        /// <param name="VSlopeStart">Slope of eth starting point</param>
        /// <param name="VCrvRun">Run along alignment</param>
        /// <param name="VCrvRise">rise of alignment (Z)</param>

        public void AlignmentSetVerticalCurve(double VSlopeStart, double[] VCrvRun, double[] VCrvRise)
        {
            m_Alignment.SetVerticalCurve(VSlopeStart, VCrvRun, VCrvRise);

        }


        ///////////////////////////////////////////////////////////////////////////////////////////
        //
        // Section public functions  
        //
        ///////////////////////////////////////////////////////////////////////////////////////////
        public string[] SectionSlabOnGirderNameList
        {
            get
            {
                if (m_SectionSlabOnGirderList == null) return null;

                string[] Names = new string[m_SectionSlabOnGirderList.Count];

                int i = 0;

                foreach (SectionSlabOnGirder item in m_SectionSlabOnGirderList)
                {
                    Names[i++] = item.Name;
                }

                return Names;
            }
        }

        public List<SectionSlabOnGirder> SectionSlabOnGirderList
        {
            get
            {
                if (m_SectionSlabOnGirderList == null) return null;

                List<SectionSlabOnGirder> Materials = new List<SectionSlabOnGirder>();

                foreach (SectionSlabOnGirder item in m_SectionSlabOnGirderList)
                {
                    Materials.Add(item);
                }

                return Materials;
            }
        }

        public string[] SectionBoxGirderNameList
        {
            get
            {
                if (m_SectionBoxGirderList == null) return null;

                string[] Names = new string[m_SectionBoxGirderList.Count];

                int i = 0;

                foreach (SectionBoxGirder item in m_SectionBoxGirderList)
                {
                    Names[i++] = item.Name;
                }

                return Names;
            }
        }

        public List<SectionBoxGirder> SectionBoxGirderList
        {
            get
            {
                if (m_SectionBoxGirderList == null) return null;

                List<SectionBoxGirder> Materials = new List<SectionBoxGirder>();

                foreach (SectionBoxGirder item in m_SectionBoxGirderList)
                {
                    Materials.Add(item);
                }

                return Materials;
            }
        }

        public string[] SectionSolidSlabNameList
        {
            get
            {
                if (m_SectionSolidSlabList == null) return null;

                string[] Names = new string[m_SectionSolidSlabList.Count];

                int i = 0;

                foreach (SectionSolidSlab item in m_SectionSolidSlabList)
                {
                    Names[i++] = item.Name;
                }

                return Names;
            }
        }

        public List<SectionSolidSlab> SectionSolidSlabList
        {
            get
            {
                if (m_SectionSolidSlabList == null) return null;

                List<SectionSolidSlab> Materials = new List<SectionSolidSlab>();

                foreach (SectionSolidSlab item in m_SectionSolidSlabList)
                {
                    Materials.Add(item);
                }

                return Materials;
            }
        }


        /// <summary>
        /// Adds a material to the list of nodes or replaces an exsiting one
        /// </summary>
        /// <param name="Name">material to added or replaced</param>
        /// <param name="Sprop">E, nu, Alpha</param>
        /// <returns>true=Ok, false=failed</returns>

        public bool SectionAddRevise(string SectionType, string Name, double[] Sprop)
        {
            bool RetCode = false; // Not Succerssful

            switch (SectionType)
            {
                case "SlabOnGirder":

                    // Check if we have this name in the list 
                    if (m_SectionSlabOnGirderList.Exists(x => x.Name == Name))
                    {
                        // Relace the item, we need to find the index of the item first
                        int index = m_SectionSlabOnGirderList.IndexOf(m_SectionSlabOnGirderList.Single(x => x.Name == Name));

                        m_SectionSlabOnGirderList.RemoveAt(index);
                        m_SectionSlabOnGirderList.Insert(index, (new SectionSlabOnGirder(Name)));

                        RetCode = true;

                    }
                    else   // no, Add the item
                    {
                        m_SectionSlabOnGirderList.Add(new SectionSlabOnGirder(Name));

                        RetCode = true;
                    }

                    break;

                case "BoxGirder":

                    // Check if we have this name in the list 
                    if (m_SectionBoxGirderList.Exists(x => x.Name == Name))
                    {
                        // Relace the item, we need to find the index of the item first
                        int index = m_SectionBoxGirderList.IndexOf(m_SectionBoxGirderList.Single(x => x.Name == Name));

                        m_SectionBoxGirderList.RemoveAt(index);
                        m_SectionBoxGirderList.Insert(index, (new SectionBoxGirder(Name)));

                        RetCode = true;

                    }
                    else   // no, Add the item
                    {
                        m_SectionBoxGirderList.Add(new SectionBoxGirder(Name));

                        RetCode = true;
                    }

                    break;

                case "SolidSlab":

                    // Check if we have this name in the list 
                    if (m_SectionSolidSlabList.Exists(x => x.Name == Name))
                    {
                        // Relace the item, we need to find the index of the item first
                        int index = m_SectionSolidSlabList.IndexOf(m_SectionSolidSlabList.Single(x => x.Name == Name));

                        m_SectionSolidSlabList.RemoveAt(index);
                        m_SectionSolidSlabList.Insert(index, (new SectionSolidSlab(Name)));

                        RetCode = true;

                    }
                    else   // no, Add the item
                    {
                        m_SectionSolidSlabList.Add(new SectionSolidSlab(Name));

                        RetCode = true;
                    }

                    break;

            }

            return RetCode;
        }
                
        ///////////////////////////////////////////////////////////////////////////////////////////
        //
        // Gravity public functions  
        //
        ///////////////////////////////////////////////////////////////////////////////////////////
        public bool SetGravity(double[] GravityFactor)
        {
            bool RetCode = false; // Not Succerssful

            if (GravityFactor != null)
            {
                if (GravityFactor.Length == 3)
                {
                    m_GravityFactor = new double[3];
                    for (int i = 0; i < 3; i++)
                    {
                        m_GravityFactor[i] = GravityFactor[i];
                    }
                    RetCode = true;
                }
            }

            return RetCode;
        }
    }
}