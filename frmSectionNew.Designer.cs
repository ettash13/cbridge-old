﻿namespace CBridge
{
    partial class frmSectionNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSectionType = new System.Windows.Forms.ComboBox();
            this.cmbSectionShape = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSectionName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxGriderSpacing = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxGriderName = new System.Windows.Forms.TextBox();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNumGriders = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SectionType";
            // 
            // cmbSectionType
            // 
            this.cmbSectionType.ForeColor = System.Drawing.Color.DarkRed;
            this.cmbSectionType.FormattingEnabled = true;
            this.cmbSectionType.Items.AddRange(new object[] {
            "Slab On Girder",
            "Box Girder",
            "Solid Slab"});
            this.cmbSectionType.Location = new System.Drawing.Point(112, 54);
            this.cmbSectionType.Name = "cmbSectionType";
            this.cmbSectionType.Size = new System.Drawing.Size(140, 21);
            this.cmbSectionType.TabIndex = 1;
            this.cmbSectionType.TextChanged += new System.EventHandler(this.cmbSectionType_TextChanged);
            // 
            // cmbSectionShape
            // 
            this.cmbSectionShape.ForeColor = System.Drawing.Color.DarkRed;
            this.cmbSectionShape.FormattingEnabled = true;
            this.cmbSectionShape.Items.AddRange(new object[] {
            "Slab",
            "Box"});
            this.cmbSectionShape.Location = new System.Drawing.Point(112, 89);
            this.cmbSectionShape.Name = "cmbSectionShape";
            this.cmbSectionShape.Size = new System.Drawing.Size(140, 21);
            this.cmbSectionShape.TabIndex = 2;
            this.cmbSectionShape.TextChanged += new System.EventHandler(this.cmbSectionShape_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Section Shape";
            // 
            // textBoxSectionName
            // 
            this.textBoxSectionName.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxSectionName.Location = new System.Drawing.Point(112, 20);
            this.textBoxSectionName.Name = "textBoxSectionName";
            this.textBoxSectionName.Size = new System.Drawing.Size(140, 20);
            this.textBoxSectionName.TabIndex = 5;
            this.textBoxSectionName.TextChanged += new System.EventHandler(this.textBoxSectionName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(586, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(271, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "If Slab on girder, Give a name, must be unique";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Section Name";
            // 
            // btn_OK
            // 
            this.btn_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_OK.Enabled = false;
            this.btn_OK.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_OK.Location = new System.Drawing.Point(792, 535);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(65, 22);
            this.btn_OK.TabIndex = 8;
            this.btn_OK.Text = "OK";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancel.Location = new System.Drawing.Point(879, 535);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 22);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.pictureBox1.Location = new System.Drawing.Point(18, 140);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(929, 374);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(219, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(274, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Show shape for slb or box, show  default girder";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(286, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Grider Spacing";
            // 
            // textBoxGriderSpacing
            // 
            this.textBoxGriderSpacing.BackColor = System.Drawing.Color.White;
            this.textBoxGriderSpacing.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxGriderSpacing.Location = new System.Drawing.Point(399, 92);
            this.textBoxGriderSpacing.Name = "textBoxGriderSpacing";
            this.textBoxGriderSpacing.Size = new System.Drawing.Size(124, 20);
            this.textBoxGriderSpacing.TabIndex = 12;
            this.textBoxGriderSpacing.TextChanged += new System.EventHandler(this.textBoxGriderSpacing_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(286, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Girder Name";
            // 
            // textBoxGriderName
            // 
            this.textBoxGriderName.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxGriderName.Location = new System.Drawing.Point(396, 19);
            this.textBoxGriderName.Name = "textBoxGriderName";
            this.textBoxGriderName.Size = new System.Drawing.Size(127, 20);
            this.textBoxGriderName.TabIndex = 14;
            this.textBoxGriderName.TextChanged += new System.EventHandler(this.textBoxGriderName_TextChanged);
            // 
            // cmbUnit
            // 
            this.cmbUnit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnit.ForeColor = System.Drawing.Color.DarkRed;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Items.AddRange(new object[] {
            "in",
            "ft"});
            this.cmbUnit.Location = new System.Drawing.Point(686, 537);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(70, 21);
            this.cmbUnit.TabIndex = 16;
            this.cmbUnit.Text = "in";
            this.cmbUnit.SelectedIndexChanged += new System.EventHandler(this.cmbUnit_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(286, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Number of Girders";
            // 
            // textBoxNumGriders
            // 
            this.textBoxNumGriders.ForeColor = System.Drawing.Color.DarkRed;
            this.textBoxNumGriders.Location = new System.Drawing.Point(399, 54);
            this.textBoxNumGriders.Name = "textBoxNumGriders";
            this.textBoxNumGriders.Size = new System.Drawing.Size(124, 20);
            this.textBoxNumGriders.TabIndex = 17;
            this.textBoxNumGriders.TextChanged += new System.EventHandler(this.textBoxNumGriders_TextChanged);
            // 
            // frmSectionNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 566);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxNumGriders);
            this.Controls.Add(this.cmbUnit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxGriderName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxGriderSpacing);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxSectionName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbSectionShape);
            this.Controls.Add(this.cmbSectionType);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkRed;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmSectionNew";
            this.Text = "frmSectionNew";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbSectionType;
        private System.Windows.Forms.ComboBox cmbSectionShape;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSectionName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxGriderSpacing;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxGriderName;
        private System.Windows.Forms.ComboBox cmbUnit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNumGriders;
    }
}