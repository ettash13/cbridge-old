﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CBridge
{
    public partial class frmMaterials : Form
    {
        bool DoEvents = true; // a flag for turning event handling off and on selectively

        int m_Unit = 2;         // default for cmbUnit index for kip, inch

        string[,] m_HeaderTextConcrete  = new string[4, 5];  // one for each unit name like "ksi, inch"
        string[,] m_HeaderTextMildSteel = new string[4, 6];    
        string[,] m_HeaderTextStrand    = new string[4, 6];    
        string[,] m_HeaderTextElastic   = new string[4, 3];    

        string[] m_ColumnNamesConcrete  = new string[8];    // [all the column names]
        string[] m_ColumnNamesMildSteel = new string[9];    
        string[] m_ColumnNamesStrand    = new string[9];    
        string[] m_ColumnNamesElastic   = new string[6];    
            
        double[,] m_UnitFactor = null;                      // unit conversions from ksi to selected unit
        double[,] m_Limits = null;                          //  Min, max, default for each column

        public List<MaterialConcrete>  m_MaterialConcreteListIn;
        public List<MaterialMildSteel> m_MaterialMildSteelListIn;
        public List<MaterialStrand>    m_MaterialStrandListIn;
        public List<MaterialElastic>   m_MaterialElasticListIn;

        public List<MaterialConcrete>  m_MaterialConcreteListNew;  // The Outgoing data from dialog; in kip & inch
        public List<MaterialMildSteel> m_MaterialMildSteelListNew;
        public List<MaterialStrand>    m_MaterialStrandListNew;
        public List<MaterialElastic>   m_MaterialElasticListNew;

        bool[] m_InUse_Concrete  =     new bool[10];             // Flags for Material being Used , So we cant delete        
        bool[] m_InUse_MildSteel =     new bool[10];
        bool[] m_InUse_Strand    =     new bool[10];
        bool[] m_InUse_Elastic   =     new bool[10];                          
     
        public string[,] m_RenameListConcrete  = null;       // List of material new names returned with new list
        public string[,] m_RenameListMildSteel = null;       // List of material new names returned with new list
        public string[,] m_RenameListStrand    = null;       // List of material new names returned with new list
        public string[,] m_RenameListElastic   = null;       // List of material new names returned with new list
                
        public bool m_OK = false;                            // false=return from cancel, true from OK
        
        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Constructor of Material forms
        /// 
        /// </summary>
        /// ///////////////////////////////////////////////////////////////////////////////////////////
        public frmMaterials()
        {
            InitializeComponent();
            m_Limits = new double[4, 9];                    //  Min, max, default, unit conversion for each column 
                                                            // (units of ksi) {InName, NewName, In-Use, E, nu, alfa, gama)

            // Defaults are set for steel in ksi
            //     min                        max                        default                      conversion 
            m_Limits[0, 3] = 1.0e-10;  m_Limits[1, 3] = 1.0e+15;  m_Limits[2, 3] = 30000;       m_Limits[3, 3] = 1;    // E ksi
            m_Limits[0, 4] = 1.0e-10;  m_Limits[1, 4] = 1.0;      m_Limits[2, 4] = 0.3;         m_Limits[3, 4] = 1;    // Poisson Ratio unitless
            m_Limits[0, 5] = 0;        m_Limits[1, 5] = 1.0e+15;  m_Limits[2, 5] = 0.490/1728;  m_Limits[3, 5] = 1;    // Unit Weight pcf (kci to pcf)
            m_Limits[0, 6] = 1.0e-10;  m_Limits[1, 6] = 100;      m_Limits[2, 6] = 60;          m_Limits[3, 6] = 1;    // Fc or Fy
            m_Limits[0, 7] = 1.0e-10;  m_Limits[1, 7] = 100;      m_Limits[2, 7] = 75;          m_Limits[3, 7] = 1;    // Fci or Fu
            m_Limits[0, 8] = 1.0e-10;  m_Limits[1, 8] = 100;      m_Limits[2, 8] = 1;           m_Limits[3, 8] = 1;     // grade
            
            // Header Texts for Concrete
            string[,] HeaderTextConcrete = 
                                               {{"E (psi)", "Piosson R", "U.W(pci)", "f'c(psi)", "f'ci(psi)"},
                                                {"E (psf)", "Piosson R", "U.W(pcf)", "f'c(psf)", "f'ci(psf)"},
                                                {"E (ksi)", "Piosson R", "U.W(kci)", "f'c(ksi)", "f'ci(ksi)"},
                                                {"E (ksf)", "Piosson R", "U.W(kcf)", "f'c(ksf)", "f'ci(ksf)"}};

            // Header Texts for Mild Steel
            string[,] HeaderTextMildSteel = 
                                                {{"E (psi)", "Piosson R", "U.W(pci)", "fy(psi)", "fu(psi)", "Grade"},
                                                 {"E (psf)", "Piosson R", "U.W(pcf)", "fy(psf)", "fu(psf)", "Grade"},
                                                 {"E (ksi)", "Piosson R", "U.W(kci)", "fy(ksi)", "fu(ksi)", "Grade"},
                                                 {"E (ksf)", "Piosson R", "U.W(kcf)", "fy(ksf)", "f'ci(ksf)", "Grade"}};

            // Header Texts for Strand
            string[,] HeaderTextStrand = 
                                                {{"E (psi)", "Piosson R", "U.W(pci)", "f'c(psi)", "fu(psi)", "Grade"},
                                                 {"E (psf)", "Piosson R", "U.W(pcf)", "f'c(psf)", "fu(psf)", "Grade"},
                                                 {"E (ksi)", "Piosson R", "U.W(kci)", "f'c(ksi)", "fu(ksi)", "Grade"},
                                                 {"E (ksf)", "Piosson R", "U.W(kcf)", "f'c(ksf)", "fu(ksf)", "Grade"}};

            // Header Texts for Elastic 
            string[,] HeaderTextElastic  = 
                                               {{"E (psi)", "Piosson R", "U.W(pci)"},
                                                {"E (psf)", "Piosson R", "U.W(pcf)"},
                                                {"E (ksi)", "Piosson R", "U.W(kci)"},
                                                {"E (ksf)", "Piosson R", "U.W(kcf)"}};

            m_HeaderTextConcrete  = HeaderTextConcrete;
            m_HeaderTextMildSteel = HeaderTextMildSteel;
            m_HeaderTextStrand    = HeaderTextStrand;
            m_HeaderTextElastic   = HeaderTextElastic;
            
            // Column Names for all materials 
            string[] ColumnNamesConcrete  = { "dgvMC_InName",  "dgvMC_Name",  "dgvMC_InUse",  "dgvMC_E",  "dgvMC_PR",  "dgvMC_UW",  "dgvMC_FC",  "dgvMC_FCI"};
            string[] ColumnNamesMildSteel = { "dgvMMS_InName", "dgvMMS_Name", "dgvMMS_InUse", "dgvMMS_E", "dgvMMS_PR", "dgvMMS_UW", "dgvMMS_FY", "dgvMMS_FU", "dgvMMS_Grade" };
            string[] ColumnNamesStrand    = { "dgvMS_InName",  "dgvMS_Name",  "dgvMS_InUse",  "dgvMS_E",  "dgvMS_PR",  "dgvMS_UW",  "dgvMS_FY",  "dgvMS_FU",  "dgvMS_Grade" };
            string[] ColumnNamesElastic   = { "dgvME_InName",  "dgvME_Name",  "dgvME_InUse",  "dgvME_E",  "dgvME_PR",  "dgvME_UW"};

            m_ColumnNamesConcrete  = ColumnNamesConcrete;
            m_ColumnNamesMildSteel = ColumnNamesMildSteel;
            m_ColumnNamesStrand = ColumnNamesStrand;
            m_ColumnNamesElastic   = ColumnNamesElastic;

            
            // unit conversion array
            //                          E            , PR    , U W           , f'c           , f'ci         ,Grade
            double[,] Unitfactor =   {{1000.0        ,1.0    ,1000.0         , 1000.0        , 1000.0        ,1},                  
                                      {(1000*144)    ,1.0    ,(1000*1728)    , (1000*144)    , (1000*144)    ,1},
                                      {1.0           ,1.0    ,1.0            ,1.0            , 1.0           ,1},
                                      {144.0         ,1.0    ,1728.0         ,144.0          , 144.0         ,1}};

            m_UnitFactor = Unitfactor; 

            cmbUnit.Text = cmbUnit.Items[2].ToString(); // Initialize the dialog to kip, in

            cmbUnit.SelectedIndex = 2;
            m_Unit = 2;
                    
            DoEvents = true; // a flag for turning event handling off and on selectively
        }

        // --------------------------------------------------------------------
        // User Functions
        // --------------------------------------------------------------------

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves parameter valuess into Grid view
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void SetValues(List<MaterialConcrete>  MaterialConcrete,
                              List<MaterialMildSteel> MaterialMildSteel,
                              List<MaterialStrand> MaterialStrand,
                              List<MaterialElastic>   MaterialElastic,
                              bool[] InUse)
        {
            m_MaterialConcreteListIn  = MaterialConcrete;
            m_MaterialMildSteelListIn = MaterialMildSteel;
            m_MaterialStrandListIn    = MaterialStrand;
            m_MaterialElasticListIn   = MaterialElastic;

            m_InUse_Concrete = InUse;

            UpdateGridViews();
            bool OK = CheckAllLimits();
            if (!OK) bOK.Enabled = false;
        }

        private bool CheckAllLimits()
        {
            DataGridView dgv = null;
            bool AllOK = true;

            // for checkeing names we need a list of all materila names
            int totalNames = dgvMaterialConcrete.RowCount + dgvMaterialMildSteel.RowCount + dgvMaterialStrand.RowCount + dgvMaterialElastic.RowCount - 4 ;
            string[] MaterialNames = new string[totalNames];

            int count = 0;

            for (int i = 0; i < 4; i++)
            {
                if (i == 0) dgv = dgvMaterialConcrete;
                if (i == 1) dgv = dgvMaterialMildSteel;
                if (i == 2) dgv = dgvMaterialElastic;
                if (i == 3) dgv = dgvMaterialStrand;

                for (int iR = 0; iR < dgv.Rows.Count - 1; iR++)
                {
                    MaterialNames[count] = Convert.ToString(dgv.Rows[iR].Cells[1].Value);
                    count++;
                }
            }
            

            for (int i = 0; i < 4; i++)
            {
                if (i == 0) dgv = dgvMaterialConcrete;
                if (i == 1) dgv = dgvMaterialMildSteel;
                if (i == 2) dgv = dgvMaterialElastic;
                if (i == 3) dgv = dgvMaterialStrand;

                string Message = "";
                for (int iR = 0; iR < dgv.Rows.Count - 1; iR++)
                {
                    for (int iC = 0; iC < dgv.ColumnCount - 1; iC++)
                    {
                        string Cellvalue = Convert.ToString(dgv.Rows[iR].Cells[iC].Value);

                        if (iC == 1)
                        {
                            // this is Name column, check this name against all names in 4 grids








                        }
                        else if (iC != 2) // do not check the In-Use Column
                        {

                            if (!CheckLimits(dgv, iR, iC, Cellvalue, ref Message))
                            {
                                dgv.Rows[iR].Cells[iC].Style.ForeColor = Color.Red;
                                dgv.Rows[iR].Cells[iC].Style.BackColor = Color.Yellow;

                                AllOK = false;
                            }
                            else
                            {
                                dgv.Rows[iR].Cells[iC].Style.ForeColor = Color.Black;
                                dgv.Rows[iR].Cells[iC].Style.BackColor = Color.White;
                            }
                        }
                    }
                }
            }
            return AllOK;
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves parameter valuess into Material Grid Views
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void UpdateGridViews()
        {
            DoEvents = false;

            // Show Concrete materials
            if (m_MaterialConcreteListIn != null)
            {                                           
                UpdateGidViewConcrete();
            }

            // Show MildSteel materials
            if (m_MaterialMildSteelListIn != null)
            {
                UpdateGidViewMildSteel();
            }

            // Show Strand materials
            if (m_MaterialStrandListIn != null)
            {
                UpdateGidViewStrand();
            }
            
            if (m_MaterialElasticListIn != null)
            {
                UpdateGidViewElastic();
            }
                        
            DoEvents = true;
        }


        void UpdateGidViewConcrete()
        {
            // Show Concrete materials        
            int i = 0;

            foreach (MaterialConcrete item in m_MaterialConcreteListIn)
            {
                // Add a Row
                i = dgvMaterialConcrete.Rows.Add();

                dgvMaterialConcrete[m_ColumnNamesConcrete[0], i].Value = item.Name;
                dgvMaterialConcrete[m_ColumnNamesConcrete[1], i].Value = item.Name;

                //if (m_InUse_Concrete[i])
                if (false)
                    dgvMaterialConcrete[m_ColumnNamesConcrete[2], i].Value = "Yes";
                else
                    dgvMaterialConcrete[m_ColumnNamesConcrete[2], i].Value = "No";
                       
                double[] Prop = item.Prop;

                // Set column values
                dgvMaterialConcrete[m_ColumnNamesConcrete[3], i].Value = Math.Truncate(Prop[0] * m_Limits[3, 3] * 100) / 100;
                dgvMaterialConcrete[m_ColumnNamesConcrete[4], i].Value = Math.Truncate(Prop[1] * m_Limits[3, 4] * 100) / 100;
                dgvMaterialConcrete[m_ColumnNamesConcrete[5], i].Value = Math.Truncate(Prop[2] * m_Limits[3, 5] * 100) / 100;
                dgvMaterialConcrete[m_ColumnNamesConcrete[6], i].Value = Math.Truncate(Prop[3] * m_Limits[3, 6] * 100) / 100;
                dgvMaterialConcrete[m_ColumnNamesConcrete[7], i].Value = Math.Truncate(Prop[4] * m_Limits[3, 7] * 100) / 100;
            }  
        }

        void UpdateGidViewMildSteel()
        {
            int i = 0;

            foreach (MaterialMildSteel item in m_MaterialMildSteelListIn)
            {
                // Add a Row
                i = dgvMaterialMildSteel.Rows.Add();

                dgvMaterialMildSteel[m_ColumnNamesMildSteel[0], i].Value = item.Name;
                dgvMaterialMildSteel[m_ColumnNamesMildSteel[1], i].Value = item.Name;

                if (m_InUse_MildSteel[i])
                    dgvMaterialMildSteel[m_ColumnNamesMildSteel[2], i].Value = "Yes";
                else
                    dgvMaterialMildSteel[m_ColumnNamesMildSteel[2], i].Value = "No";

                double[] Prop = item.Prop;

                // Set column values
                dgvMaterialMildSteel[m_ColumnNamesMildSteel[3], i].Value = Math.Truncate(Prop[0] * m_Limits[3, 3] * 100) / 100;
                dgvMaterialMildSteel[m_ColumnNamesMildSteel[4], i].Value = Math.Truncate(Prop[1] * m_Limits[3, 4] * 100) / 100;
                dgvMaterialMildSteel[m_ColumnNamesMildSteel[5], i].Value = Math.Truncate(Prop[2] * m_Limits[3, 5] * 100) / 100;
                dgvMaterialMildSteel[m_ColumnNamesMildSteel[6], i].Value = Math.Truncate(Prop[3] * m_Limits[3, 6] * 100) / 100;
                dgvMaterialMildSteel[m_ColumnNamesMildSteel[7], i].Value = Math.Truncate(Prop[4] * m_Limits[3, 7] * 100) / 100;
                dgvMaterialMildSteel[m_ColumnNamesMildSteel[8], i].Value = Math.Truncate(Prop[5] * m_Limits[3, 8] * 100) / 100;
            }
        }
        
        void UpdateGidViewStrand()
        {
            int i = 0;

            foreach (MaterialStrand item in m_MaterialStrandListIn)
            {
                // Add a Row
                i = dgvMaterialStrand.Rows.Add();

                dgvMaterialStrand[m_ColumnNamesStrand[0], i].Value = item.Name;
                dgvMaterialStrand[m_ColumnNamesStrand[1], i].Value = item.Name;

                if (m_InUse_Strand[i])
                    dgvMaterialStrand[m_ColumnNamesStrand[2], i].Value = "Yes";
                else
                    dgvMaterialStrand[m_ColumnNamesStrand[2], i].Value = "No";

                double[] Prop = item.Prop;

                // Set column values
                dgvMaterialStrand[m_ColumnNamesStrand[3], i].Value = Math.Truncate(Prop[0] * m_Limits[3, 3] * 100) / 100;
                dgvMaterialStrand[m_ColumnNamesStrand[4], i].Value = Math.Truncate(Prop[1] * m_Limits[3, 4] * 100) / 100;
                dgvMaterialStrand[m_ColumnNamesStrand[5], i].Value = Math.Truncate(Prop[2] * m_Limits[3, 5] * 100) / 100;
                dgvMaterialStrand[m_ColumnNamesStrand[6], i].Value = Math.Truncate(Prop[3] * m_Limits[3, 6] * 100) / 100;
                dgvMaterialStrand[m_ColumnNamesStrand[7], i].Value = Math.Truncate(Prop[4] * m_Limits[3, 7] * 100) / 100;
                dgvMaterialStrand[m_ColumnNamesStrand[8], i].Value = Math.Truncate(Prop[5] * m_Limits[3, 7] * 100) / 100;
            }
        }

        void UpdateGidViewElastic()
        {
            int i = 0;

            foreach (MaterialElastic item in m_MaterialElasticListIn)
            {
                // Add a Row
                i = dgvMaterialElastic.Rows.Add();

                dgvMaterialElastic[m_ColumnNamesElastic[0], i].Value = item.Name;
                dgvMaterialElastic[m_ColumnNamesElastic[1], i].Value = item.Name;

                if (m_InUse_Elastic[i])
                    dgvMaterialElastic[m_ColumnNamesElastic[2], i].Value = "Yes";
                else
                    dgvMaterialElastic[m_ColumnNamesElastic[2], i].Value = "No";

                double[] Prop = item.Prop;

                // Set column values
                dgvMaterialElastic[m_ColumnNamesElastic[3], i].Value = Math.Truncate(Prop[0] * m_Limits[3, 3] * 100) / 100;
                dgvMaterialElastic[m_ColumnNamesElastic[4], i].Value = Math.Truncate(Prop[1] * m_Limits[3, 4] * 100) / 100;
                dgvMaterialElastic[m_ColumnNamesElastic[5], i].Value = Math.Truncate(Prop[2] * m_Limits[3, 5] * 100) / 100;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves Grid view data into structure parameters
        ///  
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void UpdateMaterialDataStructures()
        {
            DoEvents = false;

            if (dgvMaterialConcrete.Rows.Count > 0)
            {
                UpdateConcreteDataStructure();
            }
                        
            if (dgvMaterialMildSteel.Rows.Count > 0)
            {
                UpdateMildSteelDataStructure();
            }

            if (dgvMaterialStrand.Rows.Count > 0)
            {
                UpdateStrandDataStructure();
            }

            if (dgvMaterialElastic.Rows.Count > 0)
            {
                UpdateElasticDataStructure();
            }
            
            DoEvents = true;
           
        }

       void UpdateConcreteDataStructure()
       {
            // Make Generate names  
            DataGridView dgv               = dgvMaterialConcrete;
            string[]     columnNames       = m_ColumnNamesConcrete;

            int nMaterial                  = dgv.Rows.Count - 1;
            m_MaterialConcreteListNew       = new List<MaterialConcrete>(nMaterial);
            
            List<MaterialConcrete> newList = m_MaterialConcreteListNew;
                 
            int NumRename = 0;
            double Factor = 1;

            for (int i = 0; i < nMaterial; i++)
            {
                string MatName = dgv[columnNames[1], i].Value.ToString();

                double[] Prop = new double[5];

                for (int j = 3; j < 8; j++)
                {
                    int jj = j - 3;
                    Factor = m_UnitFactor[m_Unit, jj];
                    Prop[j - 3] = Convert.ToDouble(dgv[j, i].Value) / Factor; 
                }

                newList.Add(new MaterialConcrete(MatName, Prop));

                // [0] = InName ; [1] = Name [2] = InUse
                if (dgv[columnNames[0], i].Value != null)
                {
                    if (dgv[columnNames[0], i].Value.ToString() != dgv[columnNames[1], i].Value.ToString())
                        NumRename++;
                }

                // telmporary until we use material
                dgv[columnNames[2], i].Value = "NO" ;
            }

            // prepare list of renamed items
            m_RenameListConcrete = new string[NumRename, 2];
            int iRename = 0;

            for (int i = 0; i < nMaterial; i++)
            {
                if (dgv[columnNames[0], i].Value != null)
                {
                    // [0] = InName ; [1] = Name [2] = InUse
                    if (dgv[columnNames[0], i].Value.ToString() != dgv[columnNames[1], i].Value.ToString())
                    {
                        // Name and InName = new name
                        m_RenameListConcrete[iRename, 0] = dgv[columnNames[0], i].Value.ToString();
                        m_RenameListConcrete[iRename, 1] = dgv[columnNames[1], i].Value.ToString();

                        iRename++;
                    }
                }
            }
        }
        
        void UpdateMildSteelDataStructure()
        {
            // Make Generate names  
            DataGridView dgv = dgvMaterialMildSteel;
            string[] columnNames = m_ColumnNamesMildSteel;

            int nMaterial = dgv.Rows.Count - 1;
            m_MaterialMildSteelListNew = new List<MaterialMildSteel>(nMaterial);

            List<MaterialMildSteel> newList = m_MaterialMildSteelListNew;

            int NumRename = 0;
            double Factor = 1;
            
            for (int i = 0; i < nMaterial; i++)
            {
                string MatName = dgv[columnNames[1], i].Value.ToString();

                double[] Prop = new double[6];

                for (int j = 3; j < 9; j++)
                {
                    int jj = j - 3;
                    Factor = m_UnitFactor[m_Unit, jj];
                    Prop[j - 3] = Convert.ToDouble(dgv[j, i].Value) / Factor;
                }

                newList.Add(new MaterialMildSteel(MatName, Prop));

                // [0] = InName ; [1] = Name [2] = InUse
                if (dgv[columnNames[0], i].Value != null)
                {
                    if (dgv[columnNames[0], i].Value.ToString() != dgv[columnNames[1], i].Value.ToString())
                        NumRename++;
                }

                // telmporary until we use material
                dgv[columnNames[2], i].Value = "NO";
            }

            // prepare list of renamed items
            m_RenameListMildSteel = new string[NumRename, 2];
            int iRename = 0;

            for (int i = 0; i < nMaterial; i++)
            {
                if (dgv[columnNames[0], i].Value != null)
                {
                    // [0] = InName ; [1] = Name [2] = InUse
                    if (dgv[columnNames[0], i].Value.ToString() != dgv[columnNames[1], i].Value.ToString())
                    {
                        // Name and InName = new name
                        m_RenameListMildSteel[iRename, 0] = dgv[columnNames[0], i].Value.ToString();
                        m_RenameListMildSteel[iRename, 1] = dgv[columnNames[1], i].Value.ToString();

                        iRename++;
                    }
                }
            }
        }


        void UpdateStrandDataStructure()
        {
            // Make Generate names  
            DataGridView dgv = dgvMaterialStrand;
            string[] columnNames = m_ColumnNamesStrand;

            int nMaterial = dgv.Rows.Count - 1;
            m_MaterialStrandListNew = new List<MaterialStrand>(nMaterial);

            List<MaterialStrand> newList = m_MaterialStrandListNew;

            int NumRename = 0;
            double Factor = 1;

            for (int i = 0; i < nMaterial; i++)
            {
                string MatName = dgv[columnNames[1], i].Value.ToString();

                double[] Prop = new double[6];

                for (int j = 3; j < 9; j++)
                {
                    int jj = j - 3;
                    Factor = m_UnitFactor[m_Unit, jj];
                    Prop[j - 3] = Convert.ToDouble(dgv[j, i].Value) / Factor;
                }

                newList.Add(new MaterialStrand(MatName, Prop));

                // [0] = InName ; [1] = Name [2] = InUse
                if (dgv[columnNames[0], i].Value != null)
                {
                    if (dgv[columnNames[0], i].Value.ToString() != dgv[columnNames[1], i].Value.ToString())
                        NumRename++;
                }

                // telmporary until we use material
                dgv[columnNames[2], i].Value = "NO";
            }

            // prepare list of renamed items
            m_RenameListStrand = new string[NumRename, 2];
            int iRename = 0;

            for (int i = 0; i < nMaterial; i++)
            {
                if (dgv[columnNames[0], i].Value != null)
                {
                    // [0] = InName ; [1] = Name [2] = InUse
                    if (dgv[columnNames[0], i].Value.ToString() != dgv[columnNames[1], i].Value.ToString())
                    {
                        // Name and InName = new name
                        m_RenameListStrand[iRename, 0] = dgv[columnNames[0], i].Value.ToString();
                        m_RenameListStrand[iRename, 1] = dgv[columnNames[1], i].Value.ToString();

                        iRename++;
                    }
                }
            }
        }


        void UpdateElasticDataStructure()
        {
            // Make Generate names  
            DataGridView dgv = dgvMaterialElastic;
            string[] columnNames = m_ColumnNamesElastic;

            int nMaterial = dgv.Rows.Count - 1;
            m_MaterialElasticListNew = new List<MaterialElastic>(nMaterial);

            List<MaterialElastic> newList = m_MaterialElasticListNew;

            int NumRename = 0;
            double Factor = 1;

            for (int i = 0; i < nMaterial; i++)
            {
                string MatName = dgv[columnNames[1], i].Value.ToString();

                double[] Prop = new double[3];

                for (int j = 3; j < 6; j++)
                {

                    int jj = j - 3;
                    Factor = m_UnitFactor[m_Unit, jj];
                    Prop[j - 3] = Convert.ToDouble(dgv[j, i].Value) / Factor;
                }

                newList.Add(new MaterialElastic(MatName, Prop));

                // [0] = InName ; [1] = Name [2] = InUse
                if (dgv[columnNames[0], i].Value != null)
                {
                    if (dgv[columnNames[0], i].Value.ToString() != dgv[columnNames[1], i].Value.ToString())
                        NumRename++;
                }

                // telmporary until we use material
                dgv[columnNames[2], i].Value = "NO";
            }

            // prepare list of renamed items
            m_RenameListElastic = new string[NumRename, 2];
            int iRename = 0;

            for (int i = 0; i < nMaterial; i++)
            {
                if (dgv[columnNames[0], i].Value != null)
                {
                    // [0] = InName ; [1] = Name [2] = InUse
                    if (dgv[columnNames[0], i].Value.ToString() != dgv[columnNames[1], i].Value.ToString())
                    {
                        // Name and InName = new name
                        m_RenameListElastic[iRename, 0] = dgv[columnNames[0], i].Value.ToString();
                        m_RenameListElastic[iRename, 1] = dgv[columnNames[1], i].Value.ToString();

                        iRename++;
                    }
                }
            }
        }
        

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Replicat Rows of Data Grid  
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////

        private void ReplicateRow(DataGridView dgv, int RIndex)
        {
            bool EventStatus = DoEvents;

            DoEvents = false;

            int iRow = dgv.Rows.Add(); // The newly aded row index is the last row

            string baseName = dgv[0, RIndex].Value.ToString();
            string[] NameList= new string[dgv.Rows.Count-2];

            for (int i = 0; i < dgv.Rows.Count - 2; i++) NameList[i] = dgv[1, i].Value.ToString();

            string newName = GetName(baseName, NameList);

            dgv[0, iRow].Value = newName;
            dgv[1, iRow].Value = newName;

            dgv[2, iRow].Value = "No";

            for (int j = 3; j < dgv.Columns.Count; j++)
            {
                dgv[j, iRow].Value = dgv[j, RIndex].Value;
            }

            DoEvents = EventStatus;
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Get Name 
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private string GetName(string BaseName, string[] NameList)
        {

        IncrementName:

            string NewName = BaseName.Substring(0, BaseName.Length - 1);
            string right = BaseName.Substring(BaseName.Length - 1); // rightmost charachter

            try
            {
                int n = Convert.ToInt16(right);

                if (n == 9) NewName = NewName + "10";

                else NewName = NewName + Convert.ToString(n + 1);
            }
            catch

            {
                NewName = BaseName + "0";
            }

            for (int i = 0; i < NameList.Length; i++) // Check for duplicate names
            {
                if (NameList[i] == NewName)
                {
                    BaseName = NewName;

                    goto IncrementName;
                }
            }
            return NewName;
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  Event Functions 
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
 
        
        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  Replicating Rows 
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////

        private void dgvMaterialConcrete_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgv_RowsAdded(dgvMaterialConcrete);
        }

        private void dgvMaterialMildSteel_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgv_RowsAdded(dgvMaterialMildSteel);
        }

        private void dgvMaterialStrand_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgv_RowsAdded(dgvMaterialStrand);
        }

        private void dgvMaterialElastic_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgv_RowsAdded(dgvMaterialElastic);
        }


        private void dgv_RowsAdded(DataGridView dgv)
        {
            if (DoEvents)
            {
                DoEvents = false;
                int i = dgv.CurrentRow.Index;
                string baseName = "New";

                string[] NameList = new string[dgv.Rows.Count - 2];

                for (int j = 0; j < dgv.Rows.Count - 2; j++) NameList[j] = dgv[1, j].Value.ToString();

                string newName = GetName(baseName, NameList);

                dgv[1, i].Value = newName;
                dgv[2, i].Value = "No";

                if (i == 0)
                {
                    for (int j = 3; j < dgv.Columns.Count; j++)
                    {
                        dgv[j, i].Value = m_Limits[2, j];  // Default values
                    }
                }
                else
                {
                    for (int j = 3; j < dgv.Columns.Count; j++)
                    {
                        dgv[j, i].Value = dgv[j, i - 1].Value;
                    }
                }

                DoEvents = true;
            }
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  Button delete and buttomn OKs 
        /// 
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        private void bDelete_Click(object sender, EventArgs e)
        {
            DataGridView dgv = null;
            switch (tabMaterial.SelectedTab.Text)
            {
                case "Elastic":
                    dgv = dgvMaterialElastic;
                    break;
                case "Concrete":
                    dgv = dgvMaterialConcrete;
                    break;
                case "MildSteel":
                    dgv = dgvMaterialMildSteel;
                    break;
                case "Strand":
                    dgv = dgvMaterialStrand;
                    break;
                default:
                    MessageBox.Show("Invalid Tab. Contact Technical Help");
                    break;
            }
            if (dgv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow item in dgv.SelectedRows)
                {
                    if (item.Index != dgv.Rows.Count - 1)
                    {
                        if (item.Cells[2].Value.ToString() == "No") dgv.Rows.Remove(item);
                    }
                }
            }
            CheckAllLimits();
        }

        private void bReplicate_Click(object sender, EventArgs e)
        {
            DataGridView dgv = null;

            switch (tabMaterial.SelectedTab.Text)
            {
                case "Elastic":
                    dgv = dgvMaterialElastic;
                    break;
                case "Concrete":
                    dgv = dgvMaterialConcrete;
                    break;
                case "MildSteel":
                    dgv = dgvMaterialMildSteel;
                    break;
                case "Strand":
                    dgv = dgvMaterialStrand;
                    break;
                default:
                    MessageBox.Show("Invalid Tab. Contact Technical Help");
                    break;
            }
            foreach (DataGridViewRow item in dgv.SelectedRows)
            {
                if (item.Index != dgv.Rows.Count - 1) ReplicateRow(dgv, item.Index);
            }
        }


        private void cmbUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!DoEvents) return;

            DoEvents = false;

            // Convert Concrete values
            dgvMaterialConcrete.Columns[3].DefaultCellStyle.Format = "#.###";

            int SelectedUnit = cmbUnit.SelectedIndex;

            ConvertCellUnits(dgvMaterialConcrete, SelectedUnit);
            ConvertCellUnits(dgvMaterialMildSteel, SelectedUnit);
            ConvertCellUnits(dgvMaterialStrand, SelectedUnit);
            ConvertCellUnits(dgvMaterialElastic, SelectedUnit);
            
            m_Unit = SelectedUnit;

            DoEvents = true;
        }


        void ConvertCellUnits(DataGridView dgv , int SelectedUnit)
        {
            
            string GridName = dgv.Name;
            string[,] HeaderText = null;

            switch  (GridName)
            {
                case "dgvMaterialConcrete":

                    HeaderText = m_HeaderTextConcrete;
                    break;


                case "dgvMaterialMildSteel":

                    HeaderText = m_HeaderTextMildSteel;
                    break;
                    
                case "dgvMaterialStrand":

                    HeaderText = m_HeaderTextStrand;
                    break;

                case "dgvMaterialElastic":

                    HeaderText = m_HeaderTextElastic;
                    break;

                default:
                    break;
            }


            int LastColumn = dgv.ColumnCount;
            
            for (int i = 3; i < LastColumn; i++)
            {
                dgv.Columns[i].HeaderText = HeaderText[SelectedUnit, i - 3];
            }

            for (int j = 3; j < LastColumn; j++)
            {
                int jj = j - 3;

                double Factor = m_UnitFactor[SelectedUnit, jj] / m_UnitFactor[m_Unit, jj];

                m_Limits[3, j] = m_Limits[3, j] * Factor;

                for (int ir = 0; ir < dgv.Rows.Count - 1; ir++)
                {
                    dgv[j, ir].Value = Math.Truncate((Convert.ToDouble(dgv[j, ir].Value) * Factor) * 100) / 100;
                }
            }
        }
                

        private void dgvMaterialElastic_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvMaterialElastic.SelectedRows.Count > 0)
            {
                bDelete.Enabled = true;
                bReplicate.Enabled = true;
            }
            else
            {
                bDelete.Enabled = false;
                bReplicate.Enabled = false;
            }
        }

        private void dgvMaterialConcrete_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvMaterialConcrete.SelectedRows.Count > 0)
            {
                bDelete.Enabled = true;
                bReplicate.Enabled = true;
            }
            else
            {
                bDelete.Enabled = false;
                bReplicate.Enabled = false;
            }
        }


        private void dgvMaterialMildSteel_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvMaterialMildSteel.SelectedRows.Count > 0)
            {
                bDelete.Enabled = true;
                bReplicate.Enabled = true;
            }
            else
            {
                bDelete.Enabled = false;
                bReplicate.Enabled = false;
            }
        }

        private void dgvMaterialStrand_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvMaterialStrand.SelectedRows.Count > 0)
            {
                bDelete.Enabled = true;
                bReplicate.Enabled = true;
            }
            else
            {
                bDelete.Enabled = false;
                bReplicate.Enabled = false;
            }
        }


        private void frmMaterials_Load(object sender, EventArgs e)
        {

        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            m_OK = false;
            this.Close();
            return;
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            // Reset the Material List
            UpdateMaterialDataStructures();

            //Close;
            m_OK = true;
            this.Close();
            return;
        }


        /////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        ///  When any Cell value change, we check all cell to make sure the are withing the set limits
        /// 
        /// 
        /// 
        /// </summary>
        //////////////////////////////////////////////////////////////////////////////////////////////
        private void dgvMaterialConcrete_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                if (dgvMaterialConcrete.CurrentRow == null) return;

                string Message = "";

                int Row = dgvMaterialConcrete.CurrentRow.Index;

                int Col = dgvMaterialConcrete.CurrentCell.ColumnIndex;

                string Cellvalue = Convert.ToString(dgvMaterialConcrete.CurrentCell.Value);

                if (!CheckLimits(dgvMaterialConcrete, Row, Col, Cellvalue, ref Message))
                {
                    dgvMaterialConcrete.Rows[Row].Cells[Col].Style.ForeColor = Color.Red;
                    dgvMaterialConcrete.Rows[Row].Cells[Col].Style.BackColor = Color.Yellow;

                    bOK.Enabled = false;
                }
                else
                {
                    bOK.Enabled = CheckAllLimits(); // Temporary; We do not need the check after this

                }
            }
        }
              

        private void dgvMaterialMildSteel_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                if (dgvMaterialMildSteel.CurrentRow == null) return;
                string Message = "";

                int Row = dgvMaterialMildSteel.CurrentRow.Index;

                int Col = dgvMaterialMildSteel.CurrentCell.ColumnIndex;

                string Cellvalue = Convert.ToString(dgvMaterialMildSteel.CurrentCell.Value);

                if (!CheckLimits(dgvMaterialMildSteel, Row, Col, Cellvalue, ref Message))
                {
                    dgvMaterialMildSteel.Rows[Row].Cells[Col].Style.ForeColor = Color.Red;
                    dgvMaterialMildSteel.Rows[Row].Cells[Col].Style.BackColor = Color.Yellow;

                    bOK.Enabled = false;
                }
                else
                {
                    Message = "";
                    bool AllOK = true;
                    AllOK = CheckAllLimits(); // Temporary

                    for (int iR = 0; iR < dgvMaterialMildSteel.Rows.Count - 1; iR++)
                    {
                        for (int iC = 0; iC < dgvMaterialMildSteel.ColumnCount - 1; iC++)
                        {
                            if (iC != 2) // do not check the In-Use Column
                            {
                                Cellvalue = Convert.ToString(dgvMaterialMildSteel.Rows[iR].Cells[iC].Value);

                                if (!CheckLimits(dgvMaterialMildSteel, iR, iC, Cellvalue, ref Message))
                                {
                                    dgvMaterialMildSteel.Rows[iR].Cells[iC].Style.ForeColor = Color.Red;
                                    dgvMaterialMildSteel.Rows[iR].Cells[iC].Style.BackColor = Color.Yellow;

                                    AllOK = false;
                                }
                                else
                                {
                                    dgvMaterialMildSteel.Rows[iR].Cells[iC].Style.ForeColor = Color.Black;
                                    dgvMaterialMildSteel.Rows[iR].Cells[iC].Style.BackColor = Color.White;
                                }
                            }
                        }
                    }

                    if (AllOK)
                    {
                        bOK.Enabled = true;
                    }
                }
            }
        }

        private void dgvMaterialStrand_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                if (dgvMaterialStrand.CurrentRow == null) return;
                string Message = "";

                int Row = dgvMaterialStrand.CurrentRow.Index;

                int Col = dgvMaterialStrand.CurrentCell.ColumnIndex;

                string Cellvalue = Convert.ToString(dgvMaterialStrand.CurrentCell.Value);

                if (!CheckLimits(dgvMaterialStrand, Row, Col, Cellvalue, ref Message))
                {
                    dgvMaterialStrand.Rows[Row].Cells[Col].Style.ForeColor = Color.Red;
                    dgvMaterialStrand.Rows[Row].Cells[Col].Style.BackColor = Color.Yellow;

                    bOK.Enabled = false;
                }
                else
                {
                    Message = "";
                    bool AllOK = true;
                    AllOK = CheckAllLimits(); // Temporary

                    for (int iR = 0; iR < dgvMaterialStrand.Rows.Count - 1; iR++)
                    {
                        for (int iC = 0; iC < dgvMaterialStrand.ColumnCount - 1; iC++)
                        {
                            if (iC != 2) // do not check the In-Use Column
                            {
                                Cellvalue = Convert.ToString(dgvMaterialStrand.Rows[iR].Cells[iC].Value);

                                if (!CheckLimits(dgvMaterialStrand, iR, iC, Cellvalue, ref Message))
                                {
                                    dgvMaterialStrand.Rows[iR].Cells[iC].Style.ForeColor = Color.Red;
                                    dgvMaterialStrand.Rows[iR].Cells[iC].Style.BackColor = Color.Yellow;

                                    AllOK = false;
                                }
                                else
                                {
                                    dgvMaterialStrand.Rows[iR].Cells[iC].Style.ForeColor = Color.Black;
                                    dgvMaterialStrand.Rows[iR].Cells[iC].Style.BackColor = Color.White;
                                }
                            }
                        }
                    }

                    if (AllOK)
                    {
                        bOK.Enabled = true;
                    }
                }
            }
        }


        private void dgvMaterialElastic_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                if (dgvMaterialElastic.CurrentRow == null) return;
                string Message = "";

                int Row = dgvMaterialElastic.CurrentRow.Index;

                int Col = dgvMaterialElastic.CurrentCell.ColumnIndex;

                string Cellvalue = Convert.ToString(dgvMaterialElastic.CurrentCell.Value);

                if (!CheckLimits(dgvMaterialElastic, Row, Col, Cellvalue, ref Message))
                {
                    dgvMaterialElastic.Rows[Row].Cells[Col].Style.ForeColor = Color.Red;
                    dgvMaterialElastic.Rows[Row].Cells[Col].Style.BackColor = Color.Yellow;

                    bOK.Enabled = false;
                }
                else
                {
                    Message = "";
                    bool AllOK = CheckAllLimits(); // Temporary

                    for (int iR = 0; iR < dgvMaterialElastic.Rows.Count - 1; iR++)
                    {
                        for (int iC = 0; iC < dgvMaterialElastic.ColumnCount - 1; iC++)
                        {
                            if (iC != 2) // do not check the In-Use Column
                            {
                                Cellvalue = Convert.ToString(dgvMaterialElastic.Rows[iR].Cells[iC].Value);

                                if (!CheckLimits(dgvMaterialElastic, iR, iC, Cellvalue, ref Message))
                                {
                                    dgvMaterialElastic.Rows[iR].Cells[iC].Style.ForeColor = Color.Red;
                                    dgvMaterialElastic.Rows[iR].Cells[iC].Style.BackColor = Color.Yellow;

                                    AllOK = false;
                                }
                                else
                                {
                                    dgvMaterialElastic.Rows[iR].Cells[iC].Style.ForeColor = Color.Black;
                                    dgvMaterialElastic.Rows[iR].Cells[iC].Style.BackColor = Color.White;

                                }
                            }
                        }
                    }
                    if (AllOK)
                    {
                        bOK.Enabled = true;
                    }
                }
            }
        }


        private bool CheckLimits(DataGridView dgv, int Row, int Column, string Value, ref string Message)
        {
            bool RetCode = true;
            try
            {
                int caseCode = 0; // We never need to check InName

                if (Column == 1) caseCode = 1; // Name

                if (Column == 2)
                {
                    System.Diagnostics.Debugger.Break();
                }


                if (Column > 2) caseCode = 2; // Limits

                switch (caseCode)
                {
                    case 1:  // Check for duplicate names
                        for (int i = 0; i < dgv.Rows.Count - 1; i++)
                        {
                            string CheckName = dgv[1, i].Value.ToString();

                            if (Value == CheckName && i != Row)
                            {
                                RetCode = false;
                                Message = "Duplicate Name";
                                break;
                            }
                        }
                        break;

                    case 2:  // Check Limits
                        
                        double dValue = Convert.ToDouble(Value);

                        if (dValue < m_Limits[0, Column] || dValue > m_Limits[1, Column])
                        {
                            RetCode = false;
                            Message = "Out of Range";
                            break;
                        }
                        break;

                    default:

                        break;
                }
            }
            catch
            {
                RetCode = false;
                Message = "Limit Check Exception";
            }
            return RetCode;
        }
    }
}