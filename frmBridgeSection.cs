﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CBridge
{
    public partial class frmBridgeSection : Form
    {
        public frmBridgeSection()
        {
            InitializeComponent();
        }


        public void SetValues(string SectionName, string SectionType, string Gridername, int NumGriders, int GriderSpacing, int Unit)
        {
            // Name
            cmbSectionName.Items.Add (SectionName);
            cmbSectionName.SelectedIndex = 0;
            
            // Type
            textBoxSectionType.Text = SectionType;

            int TotalLenght = 0;

            for (int i = 0; i < NumGriders; i++)
            {
                dgvGriders.Rows.Add();

                dgvGriders.Rows[i].Cells["GName"].Value = Gridername;
                    
                dgvGriders.Rows[i].Cells["Distance"].Value = TotalLenght.ToString();

                TotalLenght += GriderSpacing;
            }
        }
        

        private void btnNew_Click(object sender, EventArgs e)
        {

            frmSectionNew frmSectionNew = new frmSectionNew();

            //  if (m_OK == true);
            this.Close();

            frmSectionNew.ShowDialog();
                               

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }








    }
}
